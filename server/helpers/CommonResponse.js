import { validationResult } from "express-validator";
import fs from "fs";

export const successResponse = (res, doc, message = "") => {
  return res.json({
    message: message,
    data: doc,
  });
};

export const errorResponse = (res, message = "", err = {}, statusCode = 400) => {
  return res.status(statusCode).json({
    message: message,
    ...err,
  });
};

export const validationErrorMessage = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({
      message: "Form request validation error!",
      errors: errors.array(),
    });
  next();
};

export const removeImage = (path) => {
  if (fs.existsSync(path)) {
    fs.rmSync(path);
  }
}
