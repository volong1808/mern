
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import fileUpload from "express-fileupload";
import bodyParser from "body-parser";
import path from 'path';
import dotenv from 'dotenv';
dotenv.config();

import mCoverRoutes from './routes/m_cover.js';
import mSizeRoutes from './routes/m_size.js';
import mCoverPriceRoutes from './routes/m_cover_price.js';
import mPagePriceRoutes from './routes/m_page_price.js';
import mFrameRoutes from './routes/m_frame.js';
import mLayoutRoutes from './routes/m_layout.js';
import mPhotobookTypeRoutes from './routes/m_photobook_type.js';
import mPaymentMethodRoutes from './routes/m_payment_method.js';
import promotionRoutes from './routes/promotion.js';
import orderRoutes from './routes/order.js';
import customerRoutes from './routes/customer.js';
import userRoutes from './routes/user.js';
import paymentRoutes from './routes/payment.js';
import photobookSettingRoutes from './routes/photobook_setting.js';
import photobookLayoutRoutes from './routes/photobook_layout.js';
import photobookImageRoutes from './routes/photobook_image.js';
import photobookTextRoutes from './routes/photobook_text.js';
import optionRoutes from './routes/option.js';
import uploadRoutes from './routes/upload.js';
import downloadRoutes from './routes/download.js';
import saveFileDesignRoutes from './routes/save_file_design.js';
import mConfig from './routes/m_config.js';

const app = express();
const __dirname = path.resolve();

app.use(cors({
  origin: "*"
}));

app.use(express.json({ limit: '30mb', extended: true }))
app.use(express.urlencoded({ limit: '30mb', extended: true }))
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload({ createParentPath: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/m-cover', mCoverRoutes);
app.use('/m-size', mSizeRoutes);
app.use('/m-cover-price', mCoverPriceRoutes);
app.use('/m-page-price', mPagePriceRoutes);
app.use('/m-frame', mFrameRoutes);
app.use('/m-layout', mLayoutRoutes);
app.use('/m-photobook-type', mPhotobookTypeRoutes);
app.use('/m-payment-method', mPaymentMethodRoutes);
app.use('/promotion', promotionRoutes);
app.use('/order', orderRoutes);
app.use('/customer', customerRoutes);
app.use('/user', userRoutes);
app.use('/payment', paymentRoutes);
app.use('/photobook-setting', photobookSettingRoutes);
app.use('/photobook-layout', photobookLayoutRoutes);
app.use('/photobook-image', photobookImageRoutes);
app.use('/photobook-text', photobookTextRoutes);
app.use('/option', optionRoutes);
app.use('/upload', uploadRoutes);
app.use('/m-config', mConfig);
app.use('/download', downloadRoutes);

app.use('/save-file-design', saveFileDesignRoutes);

const CONNECTION_URL = process.env.DB_CONNECT_URL;
const PORT = process.env.PORT || 5000;

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, () => console.log(`Server Running on Port: http://localhost:${PORT}`)))
  .catch((error) => console.log(`${error} did not connect`));

mongoose.set('useFindAndModify', false);
