import MCover from "../models/m_cover.js";

const seeder = async () => {
    const items = [
        { name: "Bìa hình", descriptions: "Bìa in ảnh và ghi chữ", image: "upload/images/covers/bia_hinh.jpg", typeFrame: 1 },
        { name: "Bìa da", descriptions: "Bìa da không chữ", image: "upload/images/covers/bia_da.jpg", typeFrame: 0 },
        { name: "Bìa da khắc laser", descriptions: "Bìa da có khắc chữ/họa tiết bằng laser theo ý khách hàng", image: "upload/images/covers/bia_da_khac_laser.jpg", typeFrame: 2 },
        { name: "Bìa da ép nhũ", descriptions: "Bìa da có chữ/họa tiết bằng mực nhũ theo ý khách hàng", image: "upload/images/covers/bia_da_ep_nhu.jpg", typeFrame: 2 },
        { name: "Bìa canvas in hình", descriptions: "Bìa in ảnh và ghi chữ", image: "upload/images/covers/bia_canvas_in_hinh.jpg", typeFrame: 1 },
        { name: "Bìa vải linen", descriptions: "Bìa vải không chữ", image: "upload/images/covers/bia_vai_linen.jpg", typeFrame: 0 },
        { name: "Bìa vải linen khắc laser", descriptions: "Bìa vải có khắc chữ/họa tiết bằng laser theo ý khách hàng", image: "upload/images/covers/bia_vai_linen_khac_laser.jpg", typeFrame: 2 },
        { name: "Bìa pha lê", descriptions: "Bìa in ảnh và ghi chữ", image: "upload/images/covers/bia_pha_le.jpg", typeFrame: 1 },
    ];

    await MCover.deleteMany({});
    await MCover.insertMany(items);

    console.log('Seeder m_cover success!');

}

export default seeder;
