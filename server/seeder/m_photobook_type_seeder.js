import MPhotobookType from "../models/m_photobook_type.js";

const seeder = async () => {
    const items = [
        { name: "Photobook Ruột Mềm Tạp Chí", descriptions: "Photobook Ruột Mềm Tạp Chí",  image: "upload/images/photobook-types/ruot-mem-tap-chi.jpg" },
        { name: "Photobook Ruột Mềm Mở Phẳng", descriptions: "Photobook Ruột Mềm Mở Phẳng", image: "upload/images/photobook-types/ruot-mem-mo-phang.png" },
        { name: "Photobook Ruột Cứng Mở Phẳng", descriptions: "Photobook Ruột Cứng Mở Phẳng", image: "upload/images/photobook-types/ruot-cung-mo-phang.png" },
    ];

    await MPhotobookType.deleteMany({});
    await MPhotobookType.insertMany(items);

    console.log('Seeder m_photobook_type success!');

}

export default seeder;
