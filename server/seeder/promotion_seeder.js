import Promotion from "../models/promotion.js";

const seeder = async () => {

    const time_start = new Date();

    const items = [
        { code: "KM200K", name: "Giảm 200k", value: 200000, type: 2 },
        { code: "KM10", name: "Giảm 10%", value: 10 },
        { code: "KM15", name: "Giảm 15%", value: 15, time_start, time_end: (new Date()).setDate(time_start.getDate() + 30) },
        { code: "KM30", name: "Giảm 30%", value: 30, time_start, time_end: (new Date()).setDate(time_start.getDate() + 5), min_price_order: 1000000 },
    ];

    await Promotion.deleteMany({});
    await Promotion.insertMany(items);

    console.log('Seeder promotion success!');

}

export default seeder;
