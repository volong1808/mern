import MConfig from "../models/m_config.js";

const seeder = async () => {

    const items = [
        {
            key: "company",
            value: {
                company_name: "DU MỤC ART",
                address: "76 Trần Hữu Trang, p. Hòa Cường Bắc, q. Hải Châu, Đà Nẵng",
                email: "dumucart@gmail.com",
                phone: "0934884920"
            }
        },
    ];

    await MConfig.deleteMany({});
    await MConfig.insertMany(items);

    console.log('Seeder config success!');

}

export default seeder;
