import mongoose from "mongoose";
import dotenv from 'dotenv';
dotenv.config();

import mPhotobookTypeSeeder from "./m_photobook_type_seeder.js";
import mCoverSeeder from "./m_cover_seeder.js";
import mSizeSeeder from "./m_size_seeder.js";
import mPagePriceSeeder from "./m_page_price_seeder.js";
import mCoverPriceSeeder from "./m_cover_price_seeder.js";
import mLayoutSeeder from "./m_layout_seeder.js";
import mPaymentMethodSeeder from "./m_payment_method_seeder.js";
import promotion from "./promotion_seeder.js";
import user from "./user_seeder.js";
import mConfig from "./m_config_seeder.js";

const CONNECTION_URL = process.env.DB_CONNECT_URL;

const dbseed = async () => {
    await mPhotobookTypeSeeder();
    await mCoverSeeder();
    await mSizeSeeder();
    await mPagePriceSeeder();
    await mCoverPriceSeeder();
    await mLayoutSeeder();
    await mPaymentMethodSeeder();
    await promotion();
    await user();
    await mConfig();
}

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log(`Mongodb connected !!!`);
        dbseed().then(() => {
            console.log(`Seeder Generate Created !!!`);
        });
    })
    .catch((error) => console.log(`${error} did not connect!`));

mongoose.set('useFindAndModify', false);
