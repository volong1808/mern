import MLayout from "../models/m_layout.js";
import MFrame from "../models/m_frame.js";

const seeder = async () => {
    const items = [
        {
            layout: { type: 1, image: "upload/images/frames/cover/5f02a8c475aad.png", is_cover: true, typeFrame: 1 },
            frames: [
                { type: "image", top: 40, left: 290, width: 170, height: 170 },
                { type: "text", top: 218, left: 290, width: 170, height: 24 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/cover/5f02a994aa9e8.png", is_cover: true, typeFrame: 1 },
            frames: [
                { type: "image", top: 75, left: 265, width: 220, height: 160 },
                { type: "text", top: 25, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/cover/5f33a735cb1cf.png", is_cover: true, typeFrame: 2 },
            frames: [
                { type: "text", top: 15, left: 265, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f5ed7f41f732.png" },
            frames: [
                { type: "image", top: 0, left: 250, width: 250, height: 250 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f5ed73450889.png" },
            frames: [
                { type: "image", top: 50, left: 300, width: 150, height: 150 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
                { type: "text", top: 213, left: 300, width: 150, height: 24 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f014ffe96a0a.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 220 },
                { type: "text", top: 15, left: 265, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f0157eb33ca1.png" },
            frames: [
                { type: "image", top: 15, left: 265, width: 220, height: 165 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
                { type: "text", top: 213, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f0150769cda2.png" },
            frames: [
                { type: "image", top: 0, left: 310, width: 190, height: 250 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 1, image: "upload/images/frames/1/5f01545223844.png" },
            frames: [
                { type: "image", top: 15, left: 285, width: 180, height: 220 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f5ee1bbe8f57.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 220 },
                { type: "image", top: 15, left: 265, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f5ee2bdb0cdf.png" },
            frames: [
                { type: "image", top: 0, left: 0, width: 180, height: 250 },
                { type: "image", top: 43, left: 265, width: 220, height: 165 },
                { type: "text", top: 62, left: 185, width: 60, height: 120 },
                { type: "text", top: 216, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f5ee22aec66c.png" },
            frames: [
                { type: "image", top: 0, left: 0, width: 180, height: 250 },
                { type: "image", top: 0, left: 320, width: 180, height: 250 },
                { type: "text", top: 62, left: 185, width: 60, height: 120 },
                { type: "text", top: 62, left: 255, width: 60, height: 120 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f5ee3378f62f.png" },
            frames: [
                { type: "image", top: 43, left: 123, width: 124, height: 165 },
                { type: "image", top: 43, left: 250, width: 250, height: 165 },
                { type: "text", top: 62, left: 15, width: 95, height: 120 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015a3240c5e.png" },
            frames: [
                { type: "image", top: 0, left: 0, width: 250, height: 250 },
                { type: "image", top: 0, left: 250, width: 250, height: 250 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015af6d7f08.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 165, height: 220 },
                { type: "image", top: 15, left: 320, width: 165, height: 220 },
                { type: "text", top: 62, left: 185, width: 60, height: 120 },
                { type: "text", top: 62, left: 255, width: 60, height: 120 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015b46aab23.png" },
            frames: [
                { type: "image", top: 15, left: 35, width: 180, height: 220 },
                { type: "image", top: 15, left: 285, width: 180, height: 220 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015bc51c59c.png" },
            frames: [
                { type: "image", top: 40, left: 15, width: 110, height: 170 },
                { type: "image", top: 40, left: 126, width: 110, height: 170 },
                { type: "text", top: 15, left: 265, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015c4e8a34e.png" },
            frames: [
                { type: "image", top: 40, left: 15, width: 220, height: 165 },
                { type: "image", top: 15, left: 290, width: 170, height: 220 },
                { type: "text", top: 225, left: 15, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f0158cb4f6da.png" },
            frames: [
                { type: "image", top: 15, left: 265, width: 220, height: 110 },
                { type: "image", top: 126, left: 265, width: 220, height: 110 },
                { type: "text", top: 15, left: 15, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f0159dfdf464.png" },
            frames: [
                { type: "image", top: 43, left: 0, width: 250, height: 165 },
                { type: "image", top: 43, left: 250, width: 250, height: 165 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 2, image: "upload/images/frames/2/5f015942e9eb9.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 180 },
                { type: "image", top: 15, left: 265, width: 220, height: 180 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f5ee7e623f77.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 220 },
                { type: "image", top: 15, left: 265, width: 220, height: 110 },
                { type: "image", top: 126, left: 265, width: 220, height: 110 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f5eeb6269b1c.png" },
            frames: [
                { type: "image", top: 43, left: 15, width: 220, height: 165 },
                { type: "image", top: 43, left: 265, width: 110, height: 165 },
                { type: "image", top: 43, left: 376, width: 110, height: 165 },
                { type: "text", top: 216, left: 15, width: 220, height: 24 },
                { type: "text", top: 216, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f5eedeb1f0de.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 220 },
                { type: "image", top: 15, left: 265, width: 110, height: 110 },
                { type: "image", top: 125, left: 375, width: 110, height: 110 },
                { type: "text", top: 60, left: 380, width: 100, height: 24 },
                { type: "text", top: 170, left: 270, width: 100, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f015ed7ecdd1.png" },
            frames: [
                { type: "image", top: 15, left: 35, width: 180, height: 110 },
                { type: "image", top: 126, left: 35, width: 180, height: 110 },
                { type: "image", top: 45, left: 265, width: 220, height: 160 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f017b52307da.png" },
            frames: [
                { type: "image", top: 45, left: 0, width: 124, height: 160 },
                { type: "image", top: 45, left: 125, width: 124, height: 160 },
                { type: "image", top: 45, left: 250, width: 250, height: 160 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f017e4ea39d5.png" },
            frames: [
                { type: "image", top: 50, left: 15, width: 110, height: 150 },
                { type: "image", top: 50, left: 126, width: 110, height: 150 },
                { type: "image", top: 0, left: 250, width: 250, height: 250 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f017ec60f93c.png" },
            frames: [
                { type: "image", top: 50, left: 15, width: 110, height: 150 },
                { type: "image", top: 50, left: 126, width: 110, height: 150 },
                { type: "image", top: 0, left: 295, width: 160, height: 220 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 3, image: "upload/images/frames/3/5f0160ef673cd.png" },
            frames: [
                { type: "image", top: 15, left: 35, width: 180, height: 110 },
                { type: "image", top: 126, left: 35, width: 180, height: 110 },
                { type: "image", top: 15, left: 291, width: 170, height: 220 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f5efa304b055.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 110 },
                { type: "image", top: 126, left: 15, width: 220, height: 110 },
                { type: "image", top: 15, left: 265, width: 220, height: 110 },
                { type: "image", top: 126, left: 265, width: 220, height: 110 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f5efb27e21e8.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 110, height: 110 },
                { type: "image", top: 126, left: 15, width: 110, height: 110 },
                { type: "image", top: 15, left: 126, width: 110, height: 110 },
                { type: "image", top: 126, left: 126, width: 110, height: 110 },
                { type: "text", top: 15, left: 265, width: 220, height: 220 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f5efc30cb4aa.png" },
            frames: [
                { type: "image", top: 66, left: 0, width: 82, height: 120 },
                { type: "image", top: 66, left: 84, width: 82, height: 120 },
                { type: "image", top: 66, left: 168, width: 82, height: 120 },
                { type: "image", top: 15, left: 265, width: 220, height: 220 },
                { type: "text", top: 200, left: 15, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f5efd55f2a09.png" },
            frames: [
                { type: "image", top: 15, left: 99, width: 150, height: 110 },
                { type: "image", top: 15, left: 251, width: 150, height: 110 },
                { type: "image", top: 126, left: 99, width: 150, height: 110 },
                { type: "image", top: 126, left: 251, width: 150, height: 110 },
                { type: "text", top: 65, left: 15, width: 69, height: 120 },
                { type: "text", top: 65, left: 416, width: 69, height: 120 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f5efdbaa5619.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 220 },
                { type: "image", top: 15, left: 265, width: 220, height: 138 },
                { type: "image", top: 155, left: 265, width: 110, height: 80 },
                { type: "image", top: 155, left: 376, width: 110, height: 80 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f027eaa7f695.png" },
            frames: [
                { type: "image", top: 15, left: 40, width: 170, height: 110 },
                { type: "image", top: 15, left: 290, width: 170, height: 110 },
                { type: "image", top: 126, left: 40, width: 170, height: 110 },
                { type: "image", top: 126, left: 290, width: 170, height: 110 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f027f5bd604b.png" },
            frames: [
                { type: "image", top: 15, left: 49, width: 200, height: 138 },
                { type: "image", top: 15, left: 251, width: 124, height: 80 },
                { type: "image", top: 155, left: 125, width: 124, height: 80 },
                { type: "image", top: 97, left: 251, width: 200, height: 138 },
                { type: "text", top: 40, left: 390, width: 95, height: 24 },
                { type: "text", top: 185, left: 15, width: 95, height: 24 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f0280e384d9f.png" },
            frames: [
                { type: "image", top: 35, left: 0, width: 249, height: 180 },
                { type: "image", top: 35, left: 251, width: 123, height: 180 },
                { type: "image", top: 35, left: 376, width: 124, height: 89 },
                { type: "image", top: 126, left: 376, width: 124, height: 89 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f0281ddc7316.png" },
            frames: [
                { type: "image", top: 15, left: 15, width: 220, height: 139 },
                { type: "image", top: 156, left: 15, width: 110, height: 79 },
                { type: "image", top: 156, left: 126, width: 110, height: 79 },
                { type: "image", top: 15, left: 325, width: 160, height: 220 },
                { type: "text", top: 115, left: 265, width: 60, height: 24 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f0286e9ad479.png" },
            frames: [
                { type: "image", top: 45, left: 15, width: 110, height: 160 },
                { type: "image", top: 45, left: 126, width: 110, height: 160 },
                { type: "image", top: 45, left: 265, width: 110, height: 160 },
                { type: "image", top: 45, left: 376, width: 110, height: 160 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
                { type: "text", top: 220, left: 265, width: 220, height: 24 },
            ]
        },
        {
            layout: { type: 4, image: "upload/images/frames/4/5f0282928bb54.png" },
            frames: [
                { type: "image", top: 42, left: 15, width: 110, height: 166 },
                { type: "image", top: 42, left: 126, width: 110, height: 166 },
                { type: "image", top: 15, left: 285, width: 180, height: 110 },
                { type: "image", top: 126, left: 285, width: 180, height: 110 },
                { type: "text", top: 220, left: 15, width: 220, height: 24 },
            ]
        },
    ];
    
    await MLayout.deleteMany({});
    await MFrame.deleteMany({});

    items.forEach(async item => {
        await MLayout.create(item.layout, async (error, m_layout) => {
            item.frames.forEach(async frame => {
                await MFrame.create({m_layout , ...frame}, async (err, m_frame) => {
                    await MLayout.findByIdAndUpdate(
                        m_layout._id,
                        {$push: {m_frames: m_frame}},
                        {safe: true, upsert: true}
                    );
                });
            })
        });      
    })

    console.log('Seeder m_layout & m_frame success!');

}

export default seeder;
