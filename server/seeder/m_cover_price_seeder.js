import MCoverPrice from "../models/m_cover_price.js";
import MSize from "../models/m_size.js";
import MCover from "../models/m_cover.js";

const seeder = async () => {
    let items = [];
    const [sizes, covers] = await Promise.all([MSize.find({}), MCover.find({})]);

    if (sizes && covers) {
        sizes.forEach(size => {
            covers.forEach((cover, index) => {
                items.push({
                    m_size: size,
                    m_cover: cover,
                    price: size.width * (index + 1) * 1000
                });
            });
        });
    }

    await MCoverPrice.deleteMany({});
    await MCoverPrice.insertMany(items);

    console.log('Seeder m_cover_price success!');

}

export default seeder;
