import MPaymentMethod from "../models/m_payment_method.js";

const seeder = async () => {
    const items = [
        { name: "Thanh toán khi giao hàng (COD)", descriptions: "Chỉ áp dụng cho đơn hàng có giá trị dưới 400.000đ.",  is_default: true },
        { name: "Thanh toán Online", descriptions: "Miễn phí vận chuyển cho đơn hàng trên 350.000đ." },
    ];

    await MPaymentMethod.deleteMany({});
    await MPaymentMethod.insertMany(items);

    console.log('Seeder m_payment_method success!');

}

export default seeder;
