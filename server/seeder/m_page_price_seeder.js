import MPagePrice from "../models/m_page_price.js";
import MSize from "../models/m_size.js";

const seeder = async () => {
    let items = [], pageNumbers = [ 20, 30, 40, 50, 60, 80, 100 ];

    await MSize.find({}, (err, datas) => {
        datas.forEach(size => {
            pageNumbers.forEach(number => {
                items.push({
                    m_size: size,
                    page_number: number,
                    price: size.width * number * 1000
                });
            });
        });
    });

    await MPagePrice.deleteMany({});
    await MPagePrice.insertMany(items);

    console.log('Seeder m_page_price success!');

}

export default seeder;
