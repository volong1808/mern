import MSize from "../models/m_size.js";

const seeder = async () => {
    const items = [
        { width: 15, height: 15 },
        { width: 20, height: 20 },
        { width: 25, height: 25 },
        { width: 30, height: 30 },
    ];

    await MSize.deleteMany({});
    await MSize.insertMany(items);

    console.log('Seeder m_size success!');

}

export default seeder;
