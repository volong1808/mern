import User from "../models/user.js";
import bcrypt from "bcryptjs";

const seeder = async () => {

    const hashedPassword = await bcrypt.hash("admin123", 12);

    const items = [
        { username: "admin", password: hashedPassword, full_name: "Admin" },
        { username: "photobook", password: hashedPassword, full_name: "Photobook" }
    ];

    await User.deleteMany({});
    await User.insertMany(items);

    console.log('Seeder User Admin success!');

}

export default seeder;
