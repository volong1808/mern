import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MPagePrice from "../models/m_page_price.js";

const router = express.Router();

export const index = async (req, res) => {
  MPagePrice.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list MPagePrice failed", err);
    }
    successResponse(res, datas, "Load list MPagePrice successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let pagePrice = new MPagePrice(formData);
  pagePrice.save(function (err) {
    if (err) {
      return errorResponse(res, "Create MPagePrice failed", err);
    }
    successResponse(res, pagePrice, "Create MPagePrice successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MPagePrice.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "MPagePrice not found", err);
    }
    successResponse(res, data, "Load detail MPagePrice successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MPagePrice.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update MPagePrice failed!", err);
      }
      successResponse(res, data, "Update MPagePrice successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MPagePrice.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete MPagePrice failed", err);
    }
    successResponse(res, data, "Delete MPagePrice successfully");
  });
};

export default router;
