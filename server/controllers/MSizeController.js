import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MSize from "../models/m_size.js";

const router = express.Router();

export const index = async (req, res) => {
  MSize.find({}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list size failed", err);
    }
    successResponse(res, datas, "Load list size successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let size = new MSize(formData);
  size.save(function (err) {
    if (err) {
      return errorResponse(res, "Create size failed", err);
    }
    successResponse(res, size, "Create size successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MSize.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Not found", err);
    }
    successResponse(res, data, "Load detail size successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MSize.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update size failed!", err);
      }
      successResponse(res, data, "Update size successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MSize.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete size failed", err);
    }
    successResponse(res, data, "Delete size successfully");
  });
};

export default router;
