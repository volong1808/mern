import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import PhotobookText from "../models/order_photobook_text.js";

const router = express.Router();

export const index = async (req, res) => {
  PhotobookText.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list PhotobookText failed", err);
    }
    successResponse(res, datas, "Load list PhotobookText successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let text = new PhotobookText(formData);
  text.save(function (err) {
    if (err) {
      return errorResponse(res, "Create PhotobookText failed", err);
    }
    successResponse(res, text, "Create PhotobookText successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  PhotobookText.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "PhotobookText not found", err);
    }
    successResponse(res, data, "Load detail PhotobookText successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  PhotobookText.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update PhotobookText failed!", err);
      }
      successResponse(res, data, "Update PhotobookText successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  PhotobookText.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete PhotobookText failed", err);
    }
    successResponse(res, data, "Delete PhotobookText successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  PhotobookText.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list failed", err);
    }

    PhotobookText.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list failed", err);
        }
        return successResponse(res, doc, "Delete list successfully");
      }
    );
  });  
};

export default router;
