import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import PhotobookImage from "../models/order_photobook_image.js";

const router = express.Router();

export const index = async (req, res) => {
  PhotobookImage.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list PhotobookImage failed", err);
    }
    successResponse(res, datas, "Load list PhotobookImage successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let image = new PhotobookImage(formData);
  image.save(function (err) {
    if (err) {
      return errorResponse(res, "Create PhotobookImage failed", err);
    }
    successResponse(res, image, "Create PhotobookImage successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  PhotobookImage.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "PhotobookImage not found", err);
    }
    successResponse(res, data, "Load detail PhotobookImage successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  PhotobookImage.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update PhotobookImage failed!", err);
      }
      successResponse(res, data, "Update PhotobookImage successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  PhotobookImage.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete PhotobookImage failed", err);
    }
    successResponse(res, data, "Delete PhotobookImage successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  PhotobookImage.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list failed", err);
    }

    PhotobookImage.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list failed", err);
        }
        return successResponse(res, doc, "Delete list successfully");
      }
    );
  });  
};

export default router;
