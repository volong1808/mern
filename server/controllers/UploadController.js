import express from "express";
import { errorResponse, successResponse } from "../helpers/CommonResponse.js";
import crypto from 'crypto';
import fs from 'fs-extra';
import sharp from 'sharp';
import sizeOf  from 'buffer-image-size';
const router = express.Router();


export const saveFile = async (req, res) => {
    try {
        if (!req.files) {
            return errorResponse(res, "Upload image failed", err);
        } else {
            let imageUploaded = [];
            let photoBookId = req.body.current_id;
            const images = Array.isArray(req.files.images) ? req.files.images : [req.files.images];

            const folderThumb = "./public/upload/temp/" + photoBookId + "/thumb";
            if (!fs.existsSync(folderThumb)) {
                fs.mkdirpSync(folderThumb);
            }

            for (let i = 0; i < images.length; i++) {
                let image = images[i];
                const fileName = crypto.randomUUID() + image.name;
                const path = "upload/temp/" + photoBookId + "/origin/" + fileName;
                image.mv('public/' + path, (err) => {
                    if (err) {
                        return errorResponse(res, "Upload image failed", err);
                    }
                });

                const pathThumb = "upload/temp/" + photoBookId + "/thumb/" + fileName;
                const imageSize = sizeOf(Buffer.from(image.data));
                const width = (imageSize.width > 300) ? 300 : imageSize.width;
                const height = Math.round(width*imageSize.height/imageSize.width);
                await sharp(Buffer.from(image.data))
                .resize(width,height, {fit: "contain"})
                .toFile('public/' + pathThumb, (err) => {
                    if (err) {
                        return errorResponse(res, "Upload image failed", err);
                    }
                });
                imageUploaded.push({
                    photobook_id: photoBookId,
                    key: Math.trunc(Date.now() / 1000) + "" + i,
                    path: pathThumb,
                    name: fileName,
                });
            }
            successResponse(res, imageUploaded, "Store images successfully");
        }
    } catch (err) {
        res.status(500).send(err);
    }
};

export const deleteImage = async (req, res) => {
    let pathThumb = req.query.path.substring(req.query.path.lastIndexOf('upload'));
    let pathOrigin = pathThumb.replace("thumb", "origin");
    try {
        fs.unlinkSync('./public/' + pathThumb);
        fs.unlinkSync('./public/' + pathOrigin);
        return res.json({ message: "sucess", data: true });
    } catch (err) {
        return res.json({ message: "fail", data: false });
    }
};

export const deleteImagePhotobook = async (req, res) => {
    let idphotobook = req.query.idphotobook;
    let path = './public/upload/temp/' + idphotobook;
    try {
        if (fs.existsSync(path)) {
            fs.rmSync(path, { recursive: true });
            return res.json({ message: "sucess", data: true });
        } else {
            return res.json({ message: "fail", data: false });
        }
    } catch (err) {
        return res.json({ message: "fail", data: false });
    }
};


export default router;
