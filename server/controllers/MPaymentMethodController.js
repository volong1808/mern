import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MPaymentMethod from "../models/m_payment_method.js";

const router = express.Router();

export const index = async (req, res) => {
  MPaymentMethod.find({}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list MPaymentMethod failed", err);
    }
    successResponse(res, datas, "Load list MPaymentMethod successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let paymentMethod = new MPaymentMethod(formData);
  paymentMethod.save(function (err) {
    if (err) {
      return errorResponse(res, "Create MPaymentMethod failed", err);
    }
    successResponse(res, paymentMethod, "Create MPaymentMethod successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MPaymentMethod.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "MPaymentMethod not found", err);
    }
    successResponse(res, data, "Load detail MPaymentMethod successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MPaymentMethod.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update MPaymentMethod failed!", err);
      }
      successResponse(res, data, "Update MPaymentMethod successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MPaymentMethod.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete MPaymentMethod failed", err);
    }
    successResponse(res, data, "Delete MPaymentMethod successfully");
  });
};

export default router;
