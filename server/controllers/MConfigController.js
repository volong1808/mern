import express from "express";
import {
    errorResponse,
    successResponse,
} from "../helpers/CommonResponse.js";
import MConfig from "../models/m_config.js";
const router = express.Router();

export const getByKey = async (req, res) => {
    let key = req.params.key;
    MConfig.findOne({key: key}, function (err, datas) {
        if (err) {
            return errorResponse(res, 'Load config failed', err);
        }
        successResponse(res, datas, "Load config successfuly");
    });
};

export default router;
