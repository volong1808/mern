import express from "express";
import fs from "fs-extra";

import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import moment from 'moment';
import fsx from 'fs-extra';
import Order from "../models/order.js";
import MCoverPrice from "../models/m_cover_price.js";
import MPagePrice from "../models/m_page_price.js";
import MSize from "../models/m_size.js";
import Customer from "../models/customer.js";
import OrderPhotobookSetting from  "../models/order_photobook_setting.js";
import OrderPhotobookLayout from  "../models/order_photobook_layout.js";
import OrderPhotobookText from  "../models/order_photobook_text.js";
import OrderPhotobookImage from  "../models/order_photobook_image.js";

const router = express.Router();

const PROMOTION_PERCENT_TYPE = 1;
const ADVAND_PAYMENT_PERCENT = 30;
const FRAME_IMAGE_TYPE = 'image';
const FRAME_TEXT_TYPE = 'text';
const IMAGE_TMP_PATH = "./public/upload/temp/";
const IMAGE_PHOTOBOOK_PATH = "./public/upload/photobooks/";

const getTotalPrice = (photobooks) => {
  let total = 0;
  photobooks.forEach((item, index) => {
    if (item.isPrint) {
      total = total + item.price*item.quantity;
    }
  });
  return total;
};

const addCustomer = async ({full_name, phone, address}, res) => {
  let data = {
    full_name: full_name,
    phone: phone,
    address: address
  }
  let customer = new Customer(data);
  await customer.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Customer failed", err);
    };
  });

  return customer
}

const addPhotobookSettingOrder = async (data, res) => {
  let photobookSetting = new OrderPhotobookSetting(data);
  await photobookSetting.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Order failed", err);
    };
  });
  return  photobookSetting;
}

const addPhotobookLayoutOrder = async (data, res) => {
  let orderPhotobookLayout = new OrderPhotobookLayout(data);
  await orderPhotobookLayout.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Order failed", err);
    };
  });
  return  orderPhotobookLayout;
}

const addOrderPhotoobookImage = async (data, res) => {
  let orderPhotobookImage = new OrderPhotobookImage(data);
  await orderPhotobookImage.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Order image failed", err);
    };
  });
  return  orderPhotobookImage;
}

const addOrderPhotoobookText = async (data, res) => {
  let orderPhotobookText = new OrderPhotobookText(data);
  await orderPhotobookText.save(function (err) {
    if (err) {
      return errorResponse(res, "Create order text failed", err);
    };
  });
  return  orderPhotobookText;
}

const getCustomerByPhone = async (phone) => {
  return  await Customer.findOne({ "phone": phone});
}

const renderOrderCode = async () => {
  const orderNumber = await getNewNumberOrder();
  return  'W-' + moment().format('DDMMYYYY') + '-' + orderNumber;
}

const getNewNumberOrder = async () => {
  let orders = await Order.find({'ordered_at': {"$gte": moment().format('dddd, MMMM DD YYYY')}});
  return orders.length + 1;
}

const getPromotionPrice = (totalPrice, promotion) => {
  if (promotion.type === PROMOTION_PERCENT_TYPE) {
    return (promotion.value.$numberDecimal/100) * totalPrice;
  }

  return promotion.value !== undefined ? promotion.value : 0;
}


const processImagePhotobooksOrder = (photobooks, orderCode) => {
  const pathSave = IMAGE_PHOTOBOOK_PATH + orderCode;
  if (fsx.pathExistsSync(pathSave)) {
    fsx.mkdirpSync(pathSave);
  }
  let isSuccess = true;
  Object.keys(photobooks).forEach( (item, index) => {
    if (saveImagesPhotobook(photobooks[item], pathSave) === true) {
      return isSuccess = true;
    }
  });

  if (isSuccess) {
    return pathSave;
  }
  return false;
}

const saveImagesPhotobook = (photobook, pathSave) => {
  const photobookId = photobook.pathImage.tmp;
  const path = IMAGE_TMP_PATH + photobookId;
  try {
    fsx.copySync(path, pathSave + '/' + photobook.pathImage.server);
    return true;
  } catch (err) {
    return false
  }
};

const removeTmpImages = (photobooks) => {
  photobooks.map((item, index) => {
    const path = IMAGE_TMP_PATH + item.pathImage.tmp;
    try {
      fsx.removeSync(path);
      return true;
    } catch (err) {
      return false
    }
  });
}

const processPhotobookOrder = async (photobooks, orderId, res) => {
  let dataPhotobookSettingOrder = await Promise.all(photobooks.map(async (item, index) => {
    let pagesImage = item.design.pagesImage;
    let dataSetting = {
      order: orderId,
      m_photobook_type: item.photobookType._id,
      m_size: item.option.pageSize,
      m_cover: item.option.pageCover,
      total_page: item.option.pageTotal,
      price: item.price,
      quantity: item.quantity,
    };
    let photobookSetting = await addPhotobookSettingOrder(dataSetting, res);
    let layouts = await processLayouts(pagesImage, photobookSetting._id, orderId, res);
    let data = {
      photobookSetting: photobookSetting,
      layouts: layouts,
      pathImage: {
        tmp: item.idphotobook,
        server: photobookSetting._id
      }
    };
    return Promise.resolve(data)
  }));
  return dataPhotobookSettingOrder;
}


const processLayouts = async (pagesImage, photobookSettingId, orderId, res) => {
  let dataOrderLayouts = await Promise.all(pagesImage.map(async (item, index) => {
    let {layoutItem} = item;
    let layoutData = {
      photobook_setting: photobookSettingId,
      m_layout: layoutItem._id,
      position: item.page,
      image: 'upload/photobooks/' + orderId + '/' + photobookSettingId + '/' + item.image.substring(item.image.lastIndexOf('pagesImage'))
    };
    let photobookLayout = await addPhotobookLayoutOrder(layoutData, photobookSettingId, res);
    let frames = await processFrameOrder(layoutItem.m_frames, photobookLayout._id, photobookSettingId, orderId, res);

    return Promise.resolve({photobookLayout: photobookLayout, frames: frames})
  }));
  return dataOrderLayouts;
}

const processFrameOrder = async (frames, layoutId, photobookSettingId, orderId, res) => {
  let dataFrame = Promise.all(frames.map(async (item, index) => {
    let frameOrder = {};
    let type = item.type;
    let dataItem = {
      photobook_layout: layoutId,
      m_frame: item._id,
      width: item.width,
      height: item.height,
      left: item.left,
      top: item.top,
    }

    if (item.type === FRAME_IMAGE_TYPE) {
      dataItem.image =  'upload/photobooks/' + orderId + '/' + photobookSettingId + '/' + item.content.substring(item.content.lastIndexOf('thumb'));
      if (item.contentCrop !== undefined) {
        dataItem.image_crop =  'upload/photobooks/' + orderId + '/' + photobookSettingId + '/' + item.contentCrop.substring(item.contentCrop.lastIndexOf('pagesImage'));
        dataItem.infoCrop = JSON.stringify(item.infoCrop);
      }

      frameOrder = await addOrderPhotoobookImage(dataItem, res);
    } else {
      dataItem.text = item.content;
      dataItem.fonts = item.fonts;
      dataItem.style = item.style;
      dataItem.color = item.color;

      frameOrder = await addOrderPhotoobookText(dataItem, res);
    }
    return Promise.resolve({type: type, frames: frameOrder})
  }));
  return dataFrame;
}

export const index = async (req, res) => {
  Order.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list order failed", err);
    }
    successResponse(res, datas, "Load list order successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let {photobooks, orderInfo, promotion} = formData.params;
  const totalPrice = getTotalPrice(photobooks);
  const promotionPrice = promotion ? getPromotionPrice(totalPrice, promotion) : 0;
  const paymentPrice = totalPrice - promotionPrice;
  const advancePrice = (paymentPrice * ADVAND_PAYMENT_PERCENT)/100;

  let customer = await getCustomerByPhone(orderInfo.phone);
  if (customer === null) {
    customer = await addCustomer({full_name: orderInfo.full_name, phone: orderInfo.phone, address: orderInfo.address}, res);
  }
  const orderCode = await renderOrderCode();
  const orderData = {
    code: orderCode,
    promotion_code: promotion?._id ? promotion?._id :  null,
    total_price: totalPrice,
    discount_price: promotionPrice,
    payment_price: paymentPrice,
    advance_price: parseInt(advancePrice),
    address_ship: orderInfo.address,
    ordered_at: moment().format(),
    note: orderInfo.note,
    customer: customer._id
  }

  Order.create(orderData, async (err, order) => {
    if (err) {
      return errorResponse(res, "Create Order failed", err);
    } else {
      const photobookSettingData = await processPhotobookOrder(photobooks, order._id, res);
      const photobookPath = processImagePhotobooksOrder(photobookSettingData,order._id);
      if (photobookPath === false) {
        return errorResponse(res, "Process Image failed", {});
      }
      removeTmpImages(photobookSettingData);

      let data = {
        order: order,
        photobookSetting: photobookSettingData
      };
      successResponse(res, data, "Create order successfull");
    }
  });  
};

export const detail = async (req, res) => {
  let id = req.params.id;
  Order.findById(id)
    .populate('promotion_code')
    .populate('customer')
    .exec(async function (err, order) {
    if (err) {
      return errorResponse(res, "Order not found", err);
    }
    let photobookSetting = await OrderPhotobookSetting
      .find({order: order._id})
      .populate('m_photobook_type')
      .populate('m_cover')
      .populate('m_size');

    let data = {
      order: order,
      photobook_setting: photobookSetting
    };
    successResponse(res, data, "Load detail order successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  Order.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update order failed!", err);
      }
      successResponse(res, data, "Update order successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  Order.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete order failed", err);
    }
    const path = './public/upload/photobooks/' + id
    fs.rmdirSync(path, { recursive: true });
    successResponse(res, data, "Delete order successfully");
  });
};

export const price = async (req, res) => {
  const {
    m_cover: mCoverId,
    m_size: mSizeId,
    pageTotal: pageTotal
  } = req.query;

  let pageNumber = (pageTotal >= 40) ? 40: Math.floor(pageTotal/10)*10;
  let pageExtra = pageTotal - pageNumber;

  try {
    const coverPrice = await MCoverPrice.findOne({
      m_size: mSizeId,
      m_cover: mCoverId,
    });

    const pagePrice = await MPagePrice.findOne({
      m_size: mSizeId,
      page_number: pageNumber,
    });

    const mSize = await MSize.findById(mSizeId);

    if (!coverPrice || !pagePrice) {
      throw new Error("Price Not found");
    }
    successResponse(
      res,
      coverPrice.price + pagePrice.price + pageExtra*mSize.width*1000,
      "Calculate price success"
    );
  } catch (err) {
    return errorResponse(res, "Calculate price failed", err);
  }
};

export default router;
