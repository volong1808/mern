import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import Promotion from "../models/promotion.js";

const router = express.Router();

export const index = async (req, res) => {
  Promotion.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list Promotion failed", err);
    }
    successResponse(res, datas, "Load list Promotion successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let promotion = new Promotion(formData);
  promotion.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Promotion failed", err);
    }
    successResponse(res, promotion, "Create Promotion successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  Promotion.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Promotion not found", err);
    }
    successResponse(res, data, "Load detail Promotion successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  Promotion.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update Promotion failed!", err);
      }
      successResponse(res, data, "Update Promotion successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  Promotion.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete promotion failed", err);
    }
    successResponse(res, data, "Delete promotion successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  Promotion.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list promotion failed", err);
    }

    Promotion.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list promotion failed", err);
        }
        return successResponse(res, doc, "Delete list promotion successfully");
      }
    );
  });  
};

export default router;
