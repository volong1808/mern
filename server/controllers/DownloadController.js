import express from "express";
import archiver from "archiver";
import streams from "memory-streams";
import fs from "fs-extra";

import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";

import dotenv from 'dotenv';
dotenv.config();

const router = express.Router();

export const photobook = async (req, res) => {
  let { order_id, photobook_setting_id } = req.query;
  
  const pathZip = `./public/upload/temp/${order_id}/${photobook_setting_id}`;

  const pathFolderOrigin = `./public/upload/photobooks/${order_id}/${photobook_setting_id}/origin`;
                                
  // To copy a folder or file  
  fs.copySync(pathFolderOrigin, `${pathZip}/origin`);

  const hwm = 1000 * 256000;
  const zipBuffer = await new Promise((resolve, reject) => {
    try {
      const ws = new streams.WritableStream({ highWaterMark: hwm });
      ws.on("finish", () => resolve(ws.toBuffer()));
      const archive = archiver.create("zip", { highWaterMark: hwm });
      archive.pipe(ws);
      archive
        .on("warning", (err) => console.warn(err))
        .on("error", (err) => { throw err; })
        .directory(`${pathZip}`, false)
        .finalize();
    } catch (err) {
      reject(err);
    }
  });
  fs.rmdirSync(pathZip, { recursive: true });
  successResponse(res, zipBuffer.toString("base64"));
};

export default router;
