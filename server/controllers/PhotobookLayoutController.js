import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import PhotobookLayout from "../models/order_photobook_layout.js";

const router = express.Router();

export const index = async (req, res) => {
  PhotobookLayout.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list PhotobookLayout failed", err);
    }
    successResponse(res, datas, "Load list PhotobookLayout successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let layout = new PhotobookLayout(formData);
  layout.save(function (err) {
    if (err) {
      return errorResponse(res, "Create PhotobookLayout failed", err);
    }
    successResponse(res, layout, "Create PhotobookLayout successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  PhotobookLayout.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "PhotobookLayout not found", err);
    }
    successResponse(res, data, "Load detail PhotobookLayout successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  PhotobookLayout.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update PhotobookLayout failed!", err);
      }
      successResponse(res, data, "Update PhotobookLayout successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  PhotobookLayout.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete PhotobookLayout failed", err);
    }
    successResponse(res, data, "Delete PhotobookLayout successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  PhotobookLayout.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list failed", err);
    }

    PhotobookLayout.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list failed", err);
        }
        return successResponse(res, doc, "Delete list successfully");
      }
    );
  });  
};

export default router;
