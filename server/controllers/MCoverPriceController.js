import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MCoverPrice from "../models/m_cover_price.js";

const router = express.Router();

export const index = async (req, res) => {
  MCoverPrice.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list MCoverPrice failed", err);
    }
    successResponse(res, datas, "Load list MCoverPrice successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let coverPrice = new MCoverPrice(formData);
  coverPrice.save(function (err) {
    if (err) {
      return errorResponse(res, "Create MCoverPrice failed", err);
    }
    successResponse(res, coverPrice, "Create MCoverPrice successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MCoverPrice.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "MCoverPrice not found", err);
    }
    successResponse(res, data, "Load detail MCoverPrice successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MCoverPrice.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update MCoverPrice failed!", err);
      }
      successResponse(res, data, "Update MCoverPrice successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MCoverPrice.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete MCoverPrice failed", err);
    }
    successResponse(res, data, "Delete MCoverPrice successfully");
  });
};

export default router;
