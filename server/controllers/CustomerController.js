import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import Customer from "../models/customer.js";

const router = express.Router();

export const index = async (req, res) => {
  Customer.find({}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list Customer failed", err);
    }
    successResponse(res, datas, "Load list Customer successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let customer = new Customer(formData);
  customer.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Customer failed", err);
    }
    successResponse(res, customer, "Create Customer successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  Customer.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Customer not found", err);
    }
    successResponse(res, data, "Load detail Customer successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  Customer.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update Customer failed!", err);
      }
      successResponse(res, data, "Update Customer successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  Customer.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete Customer failed", err);
    }
    successResponse(res, data, "Delete Customer successfully");
  });
};

export default router;
