import express from "express";
import { errorResponse, successResponse } from "../helpers/CommonResponse.js";
import MCoverPrice from "../models/m_cover_price.js";
import MPagePrice from "../models/m_page_price.js";

const router = express.Router();

export const calculatePrice = async (req, res) => {
  const {
    m_cover: mCoverId,
    m_size: mSizeId,
    page_number: pageNumber,
  } = req.query;

  try {
    const coverPrice = await MCoverPrice.findOne({
      m_size: mSizeId,
      m_cover: mCoverId,
    }).select("price");

    const pagePrice = await MPagePrice.findOne({
      m_size: mSizeId,
      page_number: pageNumber,
    }).select("price");

    if (!coverPrice || !pagePrice) {
      throw new Error("Price Not found");
    }
    successResponse(
      res,
      coverPrice.price + pagePrice.price,
      "Calculate price success"
    );
  } catch (err) {
    return errorResponse(res, "Calculate price failed", err);
  }
};

export default router;
