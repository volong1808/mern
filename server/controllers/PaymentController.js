import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import Payment from "../models/payment.js";

const router = express.Router();

export const index = async (req, res) => {
  Payment.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list Payment failed", err);
    }
    successResponse(res, datas, "Load list Payment successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let payment = new Payment(formData);
  payment.save(function (err) {
    if (err) {
      return errorResponse(res, "Create Payment failed", err);
    }
    successResponse(res, payment, "Create Payment successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  Payment.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Payment not found", err);
    }
    successResponse(res, data, "Load detail Payment successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  Payment.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update Payment failed!", err);
      }
      successResponse(res, data, "Update Payment successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  Payment.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete Payment failed", err);
    }
    successResponse(res, data, "Delete Payment successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  Payment.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list failed", err);
    }

    Payment.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list failed", err);
        }
        return successResponse(res, doc, "Delete list successfully");
      }
    );
  });  
};

export default router;
