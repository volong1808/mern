import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import PhotobookSetting from "../models/order_photobook_setting.js";

const router = express.Router();

export const index = async (req, res) => {
  PhotobookSetting.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list PhotobookSetting failed", err);
    }
    successResponse(res, datas, "Load list PhotobookSetting successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let setting = new PhotobookSetting(formData);
  setting.save(function (err) {
    if (err) {
      return errorResponse(res, "Create PhotobookSetting failed", err);
    }
    successResponse(res, setting, "Create PhotobookSetting successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  PhotobookSetting.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "PhotobookSetting not found", err);
    }
    successResponse(res, data, "Load detail PhotobookSetting successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  PhotobookSetting.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update PhotobookSetting failed!", err);
      }
      successResponse(res, data, "Update PhotobookSetting successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  PhotobookSetting.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete PhotobookSetting failed", err);
    }
    successResponse(res, data, "Delete PhotobookSetting successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  PhotobookSetting.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list failed", err);
    }

    PhotobookSetting.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list failed", err);
        }
        return successResponse(res, doc, "Delete list successfully");
      }
    );
  });  
};

export default router;
