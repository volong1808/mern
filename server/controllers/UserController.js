import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import User from "../models/user.js";

import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import dotenv from 'dotenv';
dotenv.config();

const router = express.Router();

export const index = async (req, res) => {
  User.find({}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list User failed", err);
    }
    successResponse(res, datas, "Load list User successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let user = new User(formData);
  user.save(function (err) {
    if (err) {
      return errorResponse(res, "Create User failed", err);
    }
    successResponse(res, user, "Create User successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  User.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "User not found", err);
    }
    successResponse(res, data, "Load detail User successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  User.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update User failed!", err);
      }
      successResponse(res, data, "Update User successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  User.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete User failed", err);
    }
    successResponse(res, data, "Delete User successfully");
  });
};

const secret = process.env.JWT_SECRET;

export const login = async (req, res) => {
  const { username, password } = req.body;

  try {
    const oldUser = await User.findOne({ username });

    if (!oldUser) return res.status(404).json({ message: "User doesn't exist" });

    const isPasswordCorrect = await bcrypt.compare(password, oldUser.password);

    if (!isPasswordCorrect) return res.status(400).json({ message: "Invalid credentials" });

    const token = jwt.sign({ username: oldUser.username, id: oldUser._id }, secret, { expiresIn: "6h" });

    res.status(200).json({ result: oldUser, token });
  } catch (err) {
    res.status(500).json({ message: "Something went wrong" });
  }
};

export default router;
