import express from "express";
import { errorResponse, removeImage, successResponse } from "../helpers/CommonResponse.js";
import MPhotobookType from "../models/m_photobook_type.js";
import crypto from "crypto";
const router = express.Router();

export const index = async (req, res) => {
  MPhotobookType.find({}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Load list photobook type failed", err);
    }
    successResponse(res, docs, "Load list photobook successfuly");
  });
};

export const store = async (req, res) => {
  let path = null;
  if (!req.files || !req.files.image) {
    return errorResponse(res, "No file uploaded", {}, 422);
  } else {
    let image = req.files.image;
    path = "upload/images/photobook-types/" + crypto.randomUUID() + image.name;

    image.mv("public/" + path, (err) => {
      if (err) {
        return errorResponse(res, "Handle upload failed", err);
      }
    });
  }

  let formData = req.body;
  formData.image = path;
  let photobookType = new MPhotobookType(formData);
  photobookType.save(function (err) {
    if (err) {
      removeImage("./public/" + path);
      return errorResponse(res, "Create photobook type failed", err);
    }
    successResponse(res, photobookType, "Create photobook type successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MPhotobookType.findById(id, function (err, doc) {
    if (err) {
      return errorResponse(res, "Not found", err);
    }
    successResponse(res, doc, "Load detail photobook type successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = {
    name: req.body.name,
    descriptions: req.body.descriptions,
  };
  let path = "";
  if (req.files && req.files.image) {
    let image = req.files.image;
    path = "upload/images/photobook-types/" + crypto.randomUUID() + image.name;

    image.mv("public/" + path, (err) => {
      if (err) {
        return errorResponse(res, "Handle upload failed", err);
      }
    });
  }
  if (path) {
    formData.image = path;
  }

  MPhotobookType.findByIdAndUpdate(id, formData, function (err, doc) {
    if (err) {
      removeImage("./public/" + path);
      return errorResponse(res, "Update photobook type failed!", err);
    }
    if (formData.image) {
      removeImage("./public/" + doc.image);
    }
    successResponse(res, doc, "Update photobook type successfully!");
  });
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MPhotobookType.findByIdAndDelete(id, function (err, doc) {
    if (err) {
      return errorResponse(res, "Delete photobook type failed", err);
    }
    removeImage("./public/" + doc.image);
    successResponse(res, doc, "Delete photobook type successfully");
  });
};

export const destroyMultiple = async (req, res) => {
  let ids = req.body.ids;
  MPhotobookType.find({ _id: { $in: ids}}, function (err, docs) {
    if (err) {
      return errorResponse(res, "Delete list photobook type failed", err);
    }

    MPhotobookType.deleteMany(
      {_id: { $in: ids}},
      function (err, doc) {
        if (err) {
          return errorResponse(res, "Delete list photobook type failed", err);
        }
        docs.map(item => {
          removeImage("./public/" + item.image);
        })
        return successResponse(res, doc, "Delete list photobook type successfully");
      }
    );
  });  
};

export default router;
