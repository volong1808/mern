import express from "express";
import {
    errorResponse,
    successResponse,
} from "../helpers/CommonResponse.js";
import MCover from "../models/m_cover.js";
import fs from 'fs';
import crypto from 'crypto';

const router = express.Router();

export const index = async (req, res) => {
    MCover.find(req.query ? req.query : {}, function (err, datas) {
        if (err) {
            return errorResponse(res, "Load list cover failed", err);
        }
        successResponse(res, datas, "Load list cover successfuly");
    });
};

export const store = async (req, res) => {
    try {
        if (req.files) {
            let image = req.files.image;
            const path = "upload/images/covers/" + crypto.randomUUID() + image.name;
            image.mv('public/' + path, (err) => {
                if (err) {
                    return errorResponse(res, "Load list image failed", err);
                }
            });

            let formData = req.body;
            formData.image = path;

            let cover = new MCover(formData);
            cover.save(function (err) {
                if (err) {
                    return errorResponse(res, "Create cover failed", err);
                }
                successResponse(res, cover, "Create cover successfull");
            });
        } else {
            return errorResponse(res, "Image is not exists", err);
        }

    } catch (err) {
        res.status(500).send(err);
    }
};

export const detail = async (req, res) => {
    let id = req.params.id;
    MCover.findById(id, function (err, data) {
        if (err) {
            return errorResponse(res, "Not found", err);
        }
        successResponse(res, data, "Load detail cover successfully");
    });
};

export const update = async (req, res) => {
    let id = req.params.id;
    let formData = req.body;

    if (req.files) {
        const image = req.files.image;
        const path = "upload/images/covers/" + crypto.randomUUID() + image.name;
        image.mv('public/' + path, (err) => {
            if (err) {
                return errorResponse(res, "Load list image failed", err);
            }
        });
        formData.image = path;
        //remove file
        MCover.findById(id, function (err, data) {
            if (err) {
                return errorResponse(res, "Update cover failed!", err);
            }
            var oldpath = './public/' + data.image;
            if (fs.existsSync(oldpath)) {
                fs.rmSync(oldpath, { recursive: true });
            }
        });
    }
    MCover.findByIdAndUpdate(id, formData, { new: true }, function (err, data) {
        if (err) {
            return errorResponse(res, "Update cover failed!", err);
        }
        successResponse(res, data, "Update cover successfully!");
    });
};

export const destroy = async (req, res) => {
    let id = req.params.id;
    try {
        MCover.findByIdAndDelete(id, function (err, data) {
            if (err) {
                return errorResponse(res, "Not found", err);
            }
            let path = './public/' + data.image;
            if (fs.existsSync(path)) {
                fs.rmSync(path, { recursive: true });
            }
            return res.json({ message: "success", data: true });
        });
    } catch (err) {
        res.status(500).send(err);
    }
};

export default router;
