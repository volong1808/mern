import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MFrame from "../models/m_frame.js";

const router = express.Router();

export const index = async (req, res) => {
  MFrame.find({}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list MFrame failed", err);
    }
    successResponse(res, datas, "Load list MFrame successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let frame = new MFrame(formData);
  frame.save(function (err) {
    if (err) {
      return errorResponse(res, "Create MFrame failed", err);
    }
    successResponse(res, frame, "Create MFrame successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MFrame.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "MFrame not found", err);
    }
    successResponse(res, data, "Load detail MFrame successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MFrame.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update MFrame failed!", err);
      }
      successResponse(res, data, "Update MFrame successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MFrame.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete MFrame failed", err);
    }
    successResponse(res, data, "Delete MFrame successfully");
  });
};

export default router;
