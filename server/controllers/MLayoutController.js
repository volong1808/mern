import express from "express";
import {
  errorResponse,
  successResponse,
} from "../helpers/CommonResponse.js";
import MLayout from "../models/m_layout.js";

const router = express.Router();

export const index = async (req, res) => {
  MLayout.find(req.query ? req.query : {}, function (err, datas) {
    if (err) {
      return errorResponse(res, "Load list MLayout failed", err);
    }
    successResponse(res, datas, "Load list MLayout successfuly");
  });
};

export const store = async (req, res) => {
  let formData = req.body;
  let layout = new MLayout(formData);
  layout.save(function (err) {
    if (err) {
      return errorResponse(res, "Create MLayout failed", err);
    }
    successResponse(res, layout, "Create MLayout successfull");
  });
};

export const detail = async (req, res) => {
  let id = req.params.id;
  MLayout.findById(id, function (err, data) {
    if (err) {
      return errorResponse(res, "MLayout not found", err);
    }
    successResponse(res, data, "Load detail MLayout successfully");
  });
};

export const update = async (req, res) => {
  let id = req.params.id;
  let formData = req.body;
  MLayout.findByIdAndUpdate(
    id,
    formData,
    { new: true },
    function (err, data) {
      if (err) {
        return errorResponse(res, "Update MLayout failed!", err);
      }
      successResponse(res, data, "Update MLayout successfully!");
    }
  );
};

export const destroy = async (req, res) => {
  let id = req.params.id;
  MLayout.findByIdAndDelete(id, function (err, data) {
    if (err) {
      return errorResponse(res, "Delete MLayout failed", err);
    }
    successResponse(res, data, "Delete MLayout successfully");
  });
};

export default router;
