import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const priceValid = body("price")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Price can not be empty!")
  .bail();

export const validatorAdd = [
  priceValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  priceValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
