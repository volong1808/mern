import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const typeValid = body("type")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Type can not be empty!")
  .bail();

const imageValid = body("image")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Image can not be empty!")
  .bail();

const widthValid = body("width")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Width can not be empty!")
  .bail();

const heightValid = body("height")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Height can not be empty!")
  .bail();

export const validatorAdd = [
  typeValid,
  imageValid,
  widthValid,
  heightValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  typeValid,
  imageValid,
  widthValid,
  heightValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
