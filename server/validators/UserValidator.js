import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const fullNameValid = body("full_name")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Full Name can not be empty!")
  .bail();

const phoneValid = body("phone")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Phone Number can not be empty!")
  .bail();

const usernameValid = body("username")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("UserName can not be empty!")
  .bail();

const passwordValid = body("password")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Password can not be empty!")
  .bail();
// Add more validate

export const validatorAdd = [
  fullNameValid,
  phoneValid,
  usernameValid,
  passwordValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  fullNameValid,
  phoneValid,
  usernameValid,
  passwordValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
