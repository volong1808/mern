import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const positionValid = body("position")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Position can not be empty!")
  .bail();

export const validatorAdd = [
  positionValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  positionValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
