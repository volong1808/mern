import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const widthValid = body("width")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Width can not be empty!")
  .bail();

const heightValid = body("height")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Height can not be empty!")
  .bail();

export const validatorAdd = [
  widthValid,
  heightValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  widthValid,
  heightValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
