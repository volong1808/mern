import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const codeValid = body("code")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Code can not be empty!")
  .bail();

const nameValid = body("name")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Name can not be empty!")
  .bail();

const valueValid = body("value")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Value can not be empty!")
  .bail();

export const validatorAdd = [
  codeValid,
  nameValid,
  valueValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  codeValid,
  nameValid,
  valueValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
