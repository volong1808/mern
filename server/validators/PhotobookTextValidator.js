import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const widthValid = body("width")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Width can not be empty!")
  .bail();

const heightValid = body("height")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Height can not be empty!")
  .bail();

const leftValid = body("left")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Position Left can not be empty!")
  .bail();

const topValid = body("top")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Position Top can not be empty!")
  .bail();

export const validatorAdd = [
  widthValid,
  heightValid,
  leftValid,
  topValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  widthValid,
  heightValid,
  leftValid,
  topValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
