import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const ptbTypeNameValid = body("name")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Name can not be empty!")
  .bail();

export const validatorAdd = [
  ptbTypeNameValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  ptbTypeNameValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
