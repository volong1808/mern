import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const fullNameValid = body("photobooks")
  .isEmpty()
  .withMessage("Photobook can not be empty!")
  .bail();

const phoneValid = body("orderInfo")
  .isEmpty()
  .withMessage("Order Info can not be empty!")
  .bail();

// Add more validate

export const validatorAdd = [
  fullNameValid,
  phoneValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  fullNameValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
