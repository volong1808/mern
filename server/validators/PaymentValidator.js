import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const amountValid = body("amount")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Amount can not be empty!")
  .bail();

export const validatorAdd = [
  amountValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  amountValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
