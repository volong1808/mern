import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const nameValid = body("name")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Name can not be empty!")
  .bail();

export const validatorAdd = [
  nameValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  nameValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
