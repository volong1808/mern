import { body } from "express-validator";
import { validationErrorMessage } from "../helpers/CommonResponse.js";

const totalPageValid = body("total_page")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Total Page can not be empty!")
  .bail();

const priceValid = body("price")
  .trim()
  .escape()
  .not()
  .isEmpty()
  .withMessage("Price can not be empty!")
  .bail();

export const validatorAdd = [
  totalPageValid,
  priceValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];

export const validatorEdit = [
  totalPageValid,
  priceValid,
  (req, res, next) => validationErrorMessage(req, res, next),
];
