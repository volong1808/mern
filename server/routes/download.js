import express from "express";
import * as download from "../controllers/DownloadController.js";

const router = express.Router();

router.get("/photobook", download.photobook);

export default router;
