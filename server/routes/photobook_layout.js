import express from "express";
import auth from "../middleware/auth.js";
import * as photobookLayout from "../controllers/PhotobookLayoutController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PhotobookLayoutValidator.js";

const router = express.Router();

router.get("/", photobookLayout.index);
router.post("/", validatorAdd, photobookLayout.store);
router.get("/:id", photobookLayout.detail);
router.put("/:id", validatorEdit, photobookLayout.update);
router.delete("/:id", photobookLayout.destroy);
router.delete("/", auth, photobookLayout.destroyMultiple);

export default router;
