import express from "express";
import auth from "../middleware/auth.js";
import * as mLayout from "../controllers/MLayoutController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MLayoutValidator.js";

const router = express.Router();

router.get("/", mLayout.index);
router.post("/", auth, validatorAdd, mLayout.store);
router.get("/:id", mLayout.detail);
router.put("/:id", auth, validatorEdit, mLayout.update);
router.delete("/:id", auth, mLayout.destroy);

export default router;
