import express from "express";
import auth from "../middleware/auth.js";
import * as mPhotobookType from "../controllers/MPhotobookTypeController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MPhotobookTypeValidator.js";

const router = express.Router();

router.get("/", mPhotobookType.index);
router.post("/", auth, validatorAdd, mPhotobookType.store);
router.get("/:id", mPhotobookType.detail);
router.put("/:id", auth, validatorEdit, mPhotobookType.update);
router.delete("/:id", auth, mPhotobookType.destroy);
router.delete("/", auth, mPhotobookType.destroyMultiple);

export default router;
