import express from "express";
import fs from "fs";
import path from "path";

const __dirname = path.resolve();
const router = express.Router();

router.post("/", function(req, res) {

    const file = req.body.file;
    const pathFile = path.join(__dirname, 'public', req.body.path);
    const name = req.body.name;
    const fileString64 =  file.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

    let saved = false;
    if (fileString64) {
        const bufferFile =  Buffer.from(fileString64[2],'base64');

        if (!fs.existsSync(pathFile)) {
            fs.mkdirSync(pathFile, { recursive: true });
        }
        
        fs.writeFileSync(`${pathFile}/${name}`, bufferFile);

        saved = true;
    }

    return res.status(200).json({saved});
});

export default router;
