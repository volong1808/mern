import express from "express";
import auth from "../middleware/auth.js";
import * as promotion from "../controllers/PromotionController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PromotionValidator.js";

const router = express.Router();

router.get("/", promotion.index);
router.post("/", auth, validatorAdd, promotion.store);
router.get("/:id", promotion.detail);
router.put("/:id", auth, validatorEdit, promotion.update);
router.delete("/:id", auth, promotion.destroy);
router.delete("/", auth, promotion.destroyMultiple);

export default router;
