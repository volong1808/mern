import express from "express";
import auth from "../middleware/auth.js";
import * as mCoverPrice from "../controllers/MCoverPriceController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MCoverPriceValidator.js";

const router = express.Router();

router.get("/", mCoverPrice.index);
router.post("/", auth, validatorAdd, mCoverPrice.store);
router.get("/:id", mCoverPrice.detail);
router.put("/:id", auth, validatorEdit, mCoverPrice.update);
router.delete("/:id", auth, mCoverPrice.destroy);

export default router;
