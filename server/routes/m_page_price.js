import express from "express";
import auth from "../middleware/auth.js";
import * as mPagePrice from "../controllers/MPagePriceController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MPagePriceValidator.js";

const router = express.Router();

router.get("/", mPagePrice.index);
router.post("/", auth, validatorAdd, mPagePrice.store);
router.get("/:id", mPagePrice.detail);
router.put("/:id", auth, validatorEdit, mPagePrice.update);
router.delete("/:id", auth, mPagePrice.destroy);

export default router;
