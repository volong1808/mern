import express from "express";
import * as mConfig from "../controllers/MConfigController.js";

const router = express.Router();

router.get("/get-by-key/:key", mConfig.getByKey);

export default router;
