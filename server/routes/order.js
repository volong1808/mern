import express from "express";
import auth from "../middleware/auth.js";
import * as order from "../controllers/OrderController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/OrderValidator.js";

const router = express.Router();

router.get("/", order.index);
router.post("/", validatorAdd, order.store);
router.get("/price", order.price);
router.get("/:id", order.detail);
router.put("/:id", validatorEdit, order.update);
router.delete("/:id", auth, order.destroy);

export default router;
