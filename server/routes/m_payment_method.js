import express from "express";
import auth from "../middleware/auth.js";
import * as mPaymentMethod from "../controllers/MPaymentMethodController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MPaymentMethodValidator.js";

const router = express.Router();

router.get("/", mPaymentMethod.index);
router.post("/", auth, validatorAdd, mPaymentMethod.store);
router.get("/:id", mPaymentMethod.detail);
router.put("/:id", auth, validatorEdit, mPaymentMethod.update);
router.delete("/:id", auth, mPaymentMethod.destroy);

export default router;
