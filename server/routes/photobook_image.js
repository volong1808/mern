import express from "express";
import auth from "../middleware/auth.js";
import * as photobookImage from "../controllers/PhotobookImageController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PhotobookImageValidator.js";

const router = express.Router();

router.get("/", photobookImage.index);
router.post("/", validatorAdd, photobookImage.store);
router.get("/:id", photobookImage.detail);
router.put("/:id", validatorEdit, photobookImage.update);
router.delete("/:id", photobookImage.destroy);
router.delete("/", auth, photobookImage.destroyMultiple);

export default router;
