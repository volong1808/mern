import express from "express";
import auth from "../middleware/auth.js";
import * as mFrame from "../controllers/MFrameController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MFrameValidator.js";

const router = express.Router();

router.get("/", mFrame.index);
router.post("/", auth, validatorAdd, mFrame.store);
router.get("/:id", mFrame.detail);
router.put("/:id", auth, validatorEdit, mFrame.update);
router.delete("/:id", auth, mFrame.destroy);

export default router;
