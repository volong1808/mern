import express from "express";
import auth from "../middleware/auth.js";
import * as customer from "../controllers/CustomerController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/CustomerValidator.js";

const router = express.Router();

router.get("/", auth, customer.index);
router.post("/", validatorAdd, customer.store);
router.get("/:id", customer.detail);
router.put("/:id", validatorEdit, customer.update);
router.delete("/:id", auth, customer.destroy);

export default router;
