import express from "express";
import auth from "../middleware/auth.js";
import * as user from "../controllers/UserController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/UserValidator.js";

const router = express.Router();

router.get("/", auth, user.index);
router.post("/", auth, validatorAdd, user.store);
router.post("/login", user.login);
router.get("/:id", auth, user.detail);
router.put("/:id", auth, validatorEdit, user.update);
router.delete("/:id", auth, user.destroy);

export default router;
