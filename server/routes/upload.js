import express from "express";
import auth from "../middleware/auth.js";
import * as upload from "../controllers/UploadController.js";

const router = express.Router();

router.post("/upload-file", upload.saveFile);
router.delete("/delete-file", upload.deleteImage);
router.delete("/delete-image-photobook", upload.deleteImagePhotobook);

export default router;
