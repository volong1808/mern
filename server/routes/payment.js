import express from "express";
import auth from "../middleware/auth.js";
import * as payment from "../controllers/PaymentController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PaymentValidator.js";

const router = express.Router();

router.get("/", payment.index);
router.post("/", validatorAdd, payment.store);
router.get("/:id", payment.detail);
router.put("/:id", validatorEdit, payment.update);
router.delete("/:id", payment.destroy);
router.delete("/", auth, payment.destroyMultiple);

export default router;
