import express from "express";
import auth from "../middleware/auth.js";
import * as option from "../controllers/OptionController.js";

const router = express.Router();

router.get("/calculate-price", option.calculatePrice);

export default router;
