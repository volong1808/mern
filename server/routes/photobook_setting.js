import express from "express";
import auth from "../middleware/auth.js";
import * as photobookSetting from "../controllers/PhotobookSettingController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PhotobookSettingValidator.js";

const router = express.Router();

router.get("/", photobookSetting.index);
router.post("/", validatorAdd, photobookSetting.store);
router.get("/:id", photobookSetting.detail);
router.put("/:id", validatorEdit, photobookSetting.update);
router.delete("/:id", photobookSetting.destroy);
router.delete("/", auth, photobookSetting.destroyMultiple);

export default router;
