import express from "express";
import auth from "../middleware/auth.js";
import * as mCover from "../controllers/MCoverController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MCoverValidator.js";

const router = express.Router();

router.get("/", mCover.index);
router.post("/", mCover.store);
router.get("/:id", mCover.detail);
router.put("/:id", mCover.update);
router.delete("/:id", auth, mCover.destroy);

export default router;
