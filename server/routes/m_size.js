import express from "express";
import auth from "../middleware/auth.js";
import * as mSize from "../controllers/MSizeController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/MSizeValidator.js";

const router = express.Router();

router.get("/", mSize.index);
router.post("/", auth, validatorAdd, mSize.store);
router.get("/:id", auth, mSize.detail);
router.put("/:id", auth, validatorEdit, mSize.update);
router.delete("/:id", auth, mSize.destroy);

export default router;
