import express from "express";
import auth from "../middleware/auth.js";
import * as photobookText from "../controllers/PhotobookTextController.js";
import {
  validatorAdd,
  validatorEdit,
} from "../validators/PhotobookTextValidator.js";

const router = express.Router();

router.get("/", photobookText.index);
router.post("/", validatorAdd, photobookText.store);
router.get("/:id", photobookText.detail);
router.put("/:id", validatorEdit, photobookText.update);
router.delete("/:id", photobookText.destroy);
router.delete("/", auth, photobookText.destroyMultiple);

export default router;
