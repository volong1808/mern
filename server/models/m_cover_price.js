import mongoose from "mongoose";

const mPCoverPriceSchema = mongoose.Schema({
  m_size: { type: mongoose.Schema.Types.ObjectId, ref: "MSize" },
  m_cover: { type: mongoose.Schema.Types.ObjectId, ref: "MCover" },
  price: { type: Number, required: true },
},{
  collection: "m_cover_prices"
});

export default mongoose.model("MCoverPrice", mPCoverPriceSchema);