import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  full_name: { type: String, required: true },
  phone: { type: String },
  email: { type: String },
  address: { type: String },
});

export default mongoose.model("User", userSchema);