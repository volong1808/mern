import mongoose from "mongoose";

const mPagePriceSchema = mongoose.Schema({
  m_size: { type: mongoose.Schema.Types.ObjectId, ref: "MSize" },
  page_number: { type: Number, default: 20 },
  price: { type: Number, required: true },
},{
  collection: "m_page_prices"
});

export default mongoose.model("MPagePrice", mPagePriceSchema);