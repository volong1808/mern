import mongoose from "mongoose";

const orderPhotobookImageSchema = mongoose.Schema({
  photobook_layout: { type: mongoose.Schema.Types.ObjectId, ref: "OrderPhotobookLayout" },
  m_frame: { type: mongoose.Schema.Types.ObjectId, ref: "MFrame" },
  image: { type: String },
  image_crop: { type: String },
  width: { type: Number, required: true },
  height: { type: Number, required: true },
  left: { type: Number, required: true },
  right: { type: Number },
  top: { type: Number, required: true },
  bottom: { type: Number },
  zoom: { type: Number, default: 0 },
  rotate: { type: Number, default: 0 },
  infoCrop: { type: String }
},{
  collection: "order_photobook_images",
  timestamps: true
});

export default mongoose.model("OrderPhotobookImage", orderPhotobookImageSchema);
