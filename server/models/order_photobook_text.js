import mongoose from "mongoose";

const orderPhotobookTextSchema = mongoose.Schema({
  photobook_layout: { type: mongoose.Schema.Types.ObjectId, ref: "OrderPhotobookLayout" },
  m_frame: { type: mongoose.Schema.Types.ObjectId, ref: "MFrame" },
  text: { type: String },
  width: { type: Number, required: true },
  height: { type: Number, required: true },
  left: { type: Number, required: true },
  right: { type: Number },
  top: { type: Number, required: true },
  bottom: { type: Number },
  fonts: { type: String, default: "Tahoma" },
  style: { type: String, enum: ["bold", "italic", "normal"], default: "normal" },
  color: { type: String, default: "#000000" },
},{
  collection: "order_photobook_texts",
  timestamps: true
});

export default mongoose.model("OrderPhotobookText", orderPhotobookTextSchema);
