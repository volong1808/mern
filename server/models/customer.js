import mongoose from "mongoose";

const customerSchema = mongoose.Schema({
  full_name: { type: String, required: true },
  phone: { type: String, required: true },
  email: { type: String },
  address: { type: String },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Order' }]
});

export default mongoose.model("Customer", customerSchema);