import mongoose from "mongoose";

const promotionSchema = mongoose.Schema({
  code: { type: String, required: true },
  name: { type: String, required: true },
  descriptions: { type: String },
  type: { type: Number, min: 1, max: 2, default: 1 }, //(1: percent 2: price)Decimal128
  value: { type: mongoose.Schema.Types.Decimal128, required: true },
  min_price_order: { type: mongoose.Schema.Types.Decimal128 },
  time_start: { type: Date },
  time_end: { type: Date }
},{
  timestamps: true
});

export default mongoose.model("Promotion", promotionSchema);