import mongoose from "mongoose";

const orderPhotobookLayoutSchema = mongoose.Schema({
  photobook_setting: { type: mongoose.Schema.Types.ObjectId, ref: "OrderPhotobookSetting" },
  m_layout: { type: mongoose.Schema.Types.ObjectId, ref: "MLayout" },
  position: { type: Number, required: true },
  image: { type: String},
},{
  collection: "order_photobook_layouts",
  timestamps: true
});

export default mongoose.model("OrderPhotobookLayout", orderPhotobookLayoutSchema);
