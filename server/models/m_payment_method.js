import mongoose from "mongoose";

const mPaymentMethodSchema = mongoose.Schema({
  name: { type: String, required: true },
  descriptions: { type: String },
  is_default: { type: Boolean, default: false },
},{
  collection: "m_payment_methods"
});

export default mongoose.model("MPaymentMethod", mPaymentMethodSchema);