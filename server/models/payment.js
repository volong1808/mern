import mongoose from "mongoose";

const paymentSchema = mongoose.Schema({
  order: { type: mongoose.Schema.Types.ObjectId, ref: "Order" },
  method: { type: mongoose.Schema.Types.ObjectId, ref: "MPaymentMethod" },
  amount: { type: Number, required: true },
  deposit_amount: { type: Number },
  code_backing: { type: String },
},{
  timestamps: true
});

export default mongoose.model("Payment", paymentSchema);