import mongoose from "mongoose";

const configSchema = mongoose.Schema({
  key: { type: String, unique : true, required: true },
  value: { type: JSON, required: true }
}, {
  collection: "m_config"
});

export default mongoose.model("MConfig", configSchema);
