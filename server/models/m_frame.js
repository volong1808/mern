import mongoose from "mongoose";

const mFrameSchema = mongoose.Schema({
  m_layout: { type: mongoose.Schema.Types.ObjectId, ref: "MLayout" },
  type: { type: String, enum: ["image", "text"] , required: true },
  width: { type: Number, required: true },
  height: { type: Number, required: true },
  left: { type: Number, required: true },
  right: { type: Number },
  top: { type: Number, required: true },
  bottom: { type: Number },
},{
  collection: "m_frames"
});

export default mongoose.model("MFrame", mFrameSchema);