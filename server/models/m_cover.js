import mongoose from "mongoose";

const mCoverSchema = mongoose.Schema({
  name: { type: String, required: true },
  image: { type: String, required: true },
  descriptions: { type: String },
  background_color: { type: String },
  typeFrame: { type: Number, min: 0, max: 3, default: 0 }, // 0: No Image & No Text, 1: Image & Text, 2: Only Text, 3: Only Image
  status: { type: Number, min: 0, max: 1, default: 1 }, // 0: Hidden, 1: Display
},{
  collection: "m_covers"
});

export default mongoose.model("MCover", mCoverSchema);