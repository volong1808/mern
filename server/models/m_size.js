import mongoose from "mongoose";

const mSizeSchema = mongoose.Schema({
  width: { type: Number, required: true },
  height: { type: Number, required: true },
  unit: { type: String, enum: ["px", "cm", "in"], default: "cm" },
},{
  collection: "m_sizes"
});

export default mongoose.model("MSize", mSizeSchema);