import mongoose from "mongoose";

const mLayoutSchema = mongoose.Schema({
  type: { type: Number, min: 1, max: 4, required: true },
  typeFrame: { type: Number, min: 0, max: 3, default: 1 }, // 0: No Image & No Text, 1: Image & Text, 2: Only Text, 3: Only Image
  image: { type: String, required: true },
  width: { type: Number, required: true, default: 500 },
  height: { type: Number, required: true, default: 250 },
  is_cover: { type: Boolean, default: false },
  background_color: { type: String },
  m_covers: { type: mongoose.Schema.Types.ObjectId, ref: "MCover" },
  m_frames: [{ type: mongoose.Schema.Types.ObjectId, ref: "MFrame" }],
},{
  collection: "m_layouts"
});

export default mongoose.model("MLayout", mLayoutSchema);