import mongoose from "mongoose";

const orderSchema = mongoose.Schema({
  code: { type: String },
  type: { type: String, enum: ["photobook"], default: "photobook" },
  status: { type: Number, min: 0, max: 2, default: 0 },
  promotion_code: { type: mongoose.Schema.Types.ObjectId, ref: "Promotion" },
  total_price: { type: Number, required: true },
  discount_price: { type: Number },
  payment_price: { type: Number, required: true },
  advance_price: { type: Number, required: true },
  address_ship: { type: String, required: true },
  path_images: { type: String},
  ordered_at: { type: Date, required: true },
  note: { type: String },
  customer: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
},{
  timestamps: true
});

export default mongoose.model("Order", orderSchema);
