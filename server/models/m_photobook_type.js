import mongoose from "mongoose";

const mPhotobookTypeSchema = mongoose.Schema({
  name: { type: String, required: true },
  descriptions: { type: String },
  image: { type: String, required: true }
},{
  collection: "m_photobook_types"
});

export default mongoose.model("MPhotobookType", mPhotobookTypeSchema);