import mongoose from "mongoose";

const orderPhotobookSettingSchema = mongoose.Schema({
  order: { type: mongoose.Schema.Types.ObjectId, ref: "Order" },
  m_photobook_type: { type: mongoose.Schema.Types.ObjectId, ref: "MPhotobookType" },
  total_page: { type: Number, required: true },
  m_size: { type: mongoose.Schema.Types.ObjectId, ref: "MSize" },
  m_cover: { type: mongoose.Schema.Types.ObjectId, ref: "MCover" },
  price: { type: Number, required: true },
  quantity: { type: Number, default: 1 },
},{
  collection: "order_photobook_settings",
  timestamps: true
});

export default mongoose.model("OrderPhotobookSetting", orderPhotobookSettingSchema);
