import { createAction } from "redux-actions";
import * as API from "../../api/index.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";

export const Actions = {
  initState: createAction("INIT_STATE_AUTH"),
  setState: createAction("SET_STATE_AUTH"),

  login: (formData) => async dispatch => {
    dispatch(appMenuAction.showProgress());

    try {
      const { data } = await API.login(formData);

      localStorage.setItem('profile', JSON.stringify({ ...data }));
      dispatch(appMenuAction.hideProgress());
      dispatch(Actions.setState({ loginUser: data.result }));

    } catch (error) {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "error", message: "Thông tin đăng nhập không đúng. Vui lòng kiểm tra lại" }));
    }
  }
};
