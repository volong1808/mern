import { createAction } from "redux-actions";

export const Actions = {

  toggleMenu: createAction("TOGGLE_MENU"),
  closeMenu: createAction("CLOSE_MENU"),

  showProgress: createAction("SHOW_PROGRESS_FILTER"),
  hideProgress: createAction("HIDE_PROGRESS_FILTER"),

  showProfile: createAction("SHOW_PROFILE"),
  closeProfile: createAction("HIDE_PROFILE"),

  handleSignOut: (history) => () => {
    localStorage.removeItem("profile");
    history.push("/");
  },
  
};