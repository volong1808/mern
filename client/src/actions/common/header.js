import { createAction } from "redux-actions";

export const Actions = {
  showProgress: createAction("SHOW_PROGRESS_FILTER"),
  hideProgress: createAction("HIDE_PROGRESS_FILTER"),
};
