import { createAction } from "redux-actions";

export const Actions = {
  showMessages: createAction("SHOW_MESSAGES", messages => {
    return messages.map(mes => {
      const uuid = new Date().getTime().toString(16) + Math.floor(Math.random() * 1000).toString(16);
      return {...mes, uuid};
    });
  }),

  showMessage: message => Actions.showMessages([message]),
  
  closeMessage: createAction("CLOSE_MESSAGE", key => ({uuid: key})),

  handleError: e => dispatch =>  {
    if (Array.isArray(e)) {
      dispatch(Actions.showMessages(e));
    } else if (e.message) {
      dispatch(Actions.showMessage({
        type: "error",
        message: e.message
      }));
    } else {
      dispatch(Actions.showMessage(e));
    }
    return Promise.reject(e);
  }
};