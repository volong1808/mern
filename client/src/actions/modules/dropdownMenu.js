// ------------------------
// action def
// ------------------------
export const Actions = {
  TOGGLE_DD_MENU: "TOGGLE_DD_MENU",
  CLOSE_DD_MENU: "CLOSE_DD_MENU"
};

// ------------------------
// action creators
// ------------------------

export const openMenu = (id, e) => {
  return {
    type: Actions.TOGGLE_DD_MENU,
    id: id,
    payload: {
      isOpen: true,
      anchorEl: e.currentTarget
    }
  };
}
export const closeMenu = id => {
  return {
    type: Actions.CLOSE_DD_MENU,
    id: id,
    payload: {
      isOpen: false
    }
  };
}
  