import { createAction } from "redux-actions";
import * as API from "../../api/index.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";

const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

const modifyImagePath = (coverList) => {
    coverList.forEach((item) => {
        item.image = API_URL + '/' + item.image;
    })
    return coverList;
}

const getErrorDefault = () => {
    return {
        name: false,
        status: false,
        typeFrame: false,
    }
}

export const Actions = {
    initState: createAction("INIT_STATE_ADMIN_COVER"),
    setState: createAction("SET_STATE_ADMIN_COVER"),

    // handle Event to add cover
    handleClickButton: () => (dispatch, getState) => {
        const textLayout = {
            formTitle: "Thêm ảnh bìa",
            netStepName: "Tạo ảnh bìa",
            action: 'create',
        };
        const error = getErrorDefault();

        const coverState = getState().admin.covers;
        const cover_price = coverState.m_sizes.map(size => {
            return { m_size: size, price: '' };
        });

        dispatch(Actions.setState({
            isOpenFormAddCover: true,
            status: 1,
            typeFrame: 0,
            name: '',
            descriptions: '',
            layout: textLayout,
            error: error,
            isExistsImage: false,
            cover_price
        }));
    },

    handleClickEditButton: (coverInfo) => async (dispatch, getState) => {
        dispatch(appMenuAction.showProgress());
        const coverState = getState().admin.covers;
        const textLayout = {
            formTitle: "Chỉnh ảnh bìa",
            netStepName: "Cập nhật ảnh bìa",
            action: 'edit',
        };
        const error = getErrorDefault();

        const response = { error: error, layout: textLayout };

        try {
            
            const coverDetail = await API.fetchMCover(coverInfo.id);
            if (coverDetail) {
                response.name = coverDetail.name;
                response.status = coverDetail.status;
                response.typeFrame = coverDetail.typeFrame;
                response.descriptions = coverDetail.descriptions;
                response.isOpenFormAddCover = true;
                response.src = coverInfo.image;
                response.isExistsImage = true;
                response.id = coverInfo.id;
            }

            const m_sizes = coverState.m_sizes;

            let cover_price = await Promise.all(coverState.m_sizes.map(async size => {
                const coverPrice = await API.fetchMCoverPrices({ m_size: size._id, m_cover: coverInfo.id });
                let prices = {};
                if (coverPrice) {
                    prices = coverPrice[0];
                }
                return Promise.resolve(prices);
            }));

            cover_price = cover_price.map(item => {
                item.m_size = m_sizes.find(size => size._id == item.m_size);
                return item;
            })

            response.cover_price = cover_price;

        } catch (e) {
            response.isOpenFormAddCover = false;
            console.log(e);
        }
        dispatch(Actions.setState(response));
        dispatch(appMenuAction.hideProgress());
    },

    handleDeleteButton: (row) => dispatch => {
        dispatch(Actions.setState({ isOpenModalConfirm: true, coverId: row.id }));
    },

    closeFormModal: () => (dispatch) => {
        dispatch(Actions.setState({ isOpenFormAddCover: false }));
    },

    closeModalConfirm: () => dispatch => {
        dispatch(Actions.setState({ isOpenModalConfirm: false }));
    },

    handleSumitForm: (formId, attrs) => async (dispatch, getState) => {
        dispatch(appMenuAction.showProgress());
        const coverState = getState().admin.covers;
        let { error, isOpenFormAddCover, layout, id } = attrs;
        let action = layout.action;
        let inputList = (document.getElementById(formId)).getElementsByTagName("input");
        let check = true;

        for (let i = 0; i < inputList.length; i++) {
            if (inputList[i].type == "text" || inputList[i].type == "radio" || inputList[i].type == "number") {
                if (inputList[i].value.trim() == '') {
                    error[inputList[i].name] = true;
                    check = false;
                } else {
                    error[inputList[i].name] = false;
                }
            }
            else {
                if ((!inputList[i].files || !inputList[i].files[0]) && action == 'create') {
                    if (check == true) {
                        alert("Vui lòng chọn ảnh");
                    }
                    check = false;
                }
            }
        }

        const response = {
            error: error
        }

        if (check) {
            isOpenFormAddCover = false;
            response.isOpenFormAddCover = false;
            const form = new FormData(document.getElementById(formId));
            try {
                if (action == "create") {
                    var result = await API.addMCover(form);
                    await Promise.all(coverState.cover_price.map(async item => {
                        let formData = new FormData();
                        formData.append('m_size', item.m_size._id);
                        formData.append('m_cover', result._id);
                        formData.append('price', parseInt(item.price));
                        await API.storeMCoverPrice(formData)
                        return Promise.resolve();
                    }));

                } else {
                    var result = await API.updateMCover(form, id);

                    await Promise.all(coverState.cover_price.map(async item => {
                        let formData = new FormData();
                        formData.append('price', parseInt(item.price));
                        await API.updateMCoverPrice(item._id, formData)
                        return Promise.resolve();
                    }));
                    
                }
                if (result) {
                    let list = await API.fetchMCovers();
                    if (Array.isArray(list)) {
                        modifyImagePath(list);
                        response.coverList = list;
                    }
                }
                if (action == "create") {
                    dispatch(msgAction.showMessage({ type: "success", message: "Thêm mới Loại bìa thành công!" }));
                } else {
                    dispatch(msgAction.showMessage({ type: "success", message: "Cập nhật Loại bìa thành công!" }));
                }
            } catch (e) {
                console.log(e);
            }
        }
        dispatch(Actions.setState(response));
        dispatch(appMenuAction.hideProgress());
    },

    setValueFrame: (e) => dispatch => {
        const value = e.currentTarget.dataset.value;
        dispatch(Actions.setState({ typeFrame: value }));
    },

    setValuePrice: (e, m_size) => (dispatch, getState) => {
        const coverState = getState().admin.covers;
        const value = e.target.value;
        let cover_price = coverState.cover_price;
        const index = cover_price.findIndex(item => item.m_size == m_size);
        cover_price[index].price = value;
        dispatch(Actions.setState({ cover_price }));
    },

    setValueStatus: (e) => dispatch => {
        const value = parseInt(e.target.value);
        dispatch(Actions.setState({ status: value }));
    },

    setValueName: (e) => dispatch => {
        const value = e.currentTarget.value;
        dispatch(Actions.setState({ name: value }));
    },

    setValueDescriptions: (e) => dispatch => {
        const value = e.currentTarget.value;
        dispatch(Actions.setState({ descriptions: value }));
    },

    deleteCover: (id) => async (dispatch, getState) => {
        dispatch(appMenuAction.showProgress());
        const coverState = getState().admin.covers;
        const response = { isOpenModalConfirm: false };
        let list = [];
        try {
            let result = await API.deleteMCover(id);
            if (result) {
                list = await API.fetchMCovers();
                if (Array.isArray(list)) {
                    modifyImagePath(list);
                    response.coverList = list;
                }
            }

            await Promise.all(coverState.m_sizes.map(async size => {
                const coverPrice = await API.fetchMCoverPrices({ m_size: size._id, m_cover: id });
                if (coverPrice) {
                    await API.deleteMCoverPrice(coverPrice[0]._id);
                }
            }
            ));

        } catch (e) {
            console.log(e);
        }
        dispatch(Actions.setState(response));
        dispatch(appMenuAction.hideProgress());
    },

    fetchDatas: () => async (dispatch) => {
        dispatch(appMenuAction.showProgress());
        let list = [];
        const response = {
            typeList: [
                { id: 0, title: "Không hình không chữ" },
                { id: 1, title: "Có hình có chữ" },
                { id: 2, title: "Chỉ có chữ" },
                { id: 3, title: "Chỉ có hình" },
            ]
        };
        try {
            list = await API.fetchMCovers();
            if (Array.isArray(list)) {
                response.coverList = modifyImagePath(list);
            }
            const m_sizes = await API.fetchMSizes();
            response.m_sizes = m_sizes;
            
            const cover_price = m_sizes.map(size => {
                return { m_size: size, price: '' };
            });
            response.cover_price = cover_price;

        } catch (e) {
            console.log(e);
        }

        dispatch(Actions.setState(response));
        dispatch(appMenuAction.hideProgress());
    },

    onChangeImage: (e) => (dispatch) => {
        let ele = e.currentTarget;
        let response = {};

        if (ele.files && ele.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                let image = document.getElementById("image-preview");
                image.src = e.target.result
            }

            reader.readAsDataURL(ele.files[0]);
            response.isExistsImage = true;
        } else {
            response.isExistsImage = false;
        }
        dispatch(Actions.setState(response));
    }
};
