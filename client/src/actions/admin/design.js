import { createAction } from "redux-actions";
import * as API from "../../api/index.js"
import fillTextInit from "../../ultils/fillTextCustom.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";
import { Actions as orderDetailAction } from "../admin/orderDetail.js";

import dotenv from 'dotenv';
dotenv.config();


const renderHTML = async (frames) => {
  fillTextInit();
  const ratio = (window.innerWidth > 600) ? 2 : 0.8
  let canvas = document.createElement('canvas'); 
  canvas.width = 500*ratio;
  canvas.height = 250*ratio;
  const ctx = canvas.getContext('2d');
  // Save the current state
  ctx.save();

  ctx.fillStyle = "rgba(255,255,255,1)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.restore();
  let framesText = [];
  let framesImage = [];
  frames.forEach(frame => {
    if (frame.image) {
      framesImage.push(frame);
    } else {
      framesText.push(frame);
    }
  });
  await Promise.all(framesImage.map(async frame => {
    let img = new Image();
    img.crossOrigin = "anonymous";
    img.src = frame.contentCrop || frame.content;
    await img.decode().then(async () => {
      let canvasInner = document.createElement('canvas'); 
      canvasInner.width = frame.width*ratio;
      canvasInner.height = frame.height*ratio;
      const ctxInner = canvasInner.getContext('2d');
      // get the scale
      const scale = Math.max(canvasInner.width / img.width, canvasInner.height / img.height);
      // get the top left position of the image
      const x = (canvasInner.width / 2) - (img.width / 2) * scale;
      const y = (canvasInner.height / 2) - (img.height / 2) * scale;
      ctxInner.drawImage(img, x, y, img.width * scale, img.height * scale);

      let imgFrame = new Image();
      imgFrame.crossOrigin = "anonymous";
      imgFrame.src = canvasInner.toDataURL("image/jpg");
      await imgFrame.decode().then(() => {
        ctx.drawImage(imgFrame, frame.left*ratio, frame.top*ratio);
        ctx.restore();
        return Promise.resolve();
      });
    });
    return Promise.resolve();
  }));

  const fontSize = canvas.width*16/1096;
  const lineHeight = fontSize*1.5;
  if(framesText.length) {
    framesText.forEach(frame => {
      ctx.font = `${frame.style} ${fontSize}px ${frame.fonts}`;
      ctx.fillStyle = frame.color;
      ctx.fillTextCustom(frame.content, frame.left*ratio, frame.top*ratio - fontSize/2, frame.width*ratio, frame.height*ratio, 'center', 'center', lineHeight);
      ctx.restore();
    });
  }

  return canvas.toDataURL("image/jpg");
}

const errorSystem = { type: "error", message: "Có một lỗi đã xảy ra! Vui lòng thử lại" };

export const Actions = {
  initState: createAction("INIT_STATE_DESIGN"),
  setState: createAction("SET_STATE_DESIGN"),

  fetchDatas: (photobook_current) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    const orderDetailState = JSON.parse(JSON.stringify(getState().admin.orderDetail));
    const index = orderDetailState.photobooks.findIndex(photobook => photobook._id == photobook_current);
    const photobook = JSON.parse(JSON.stringify(orderDetailState.photobooks[index]));

    let pages = photobook.photobook_layout.length;
    let pagesImage = photobook.photobook_layout.map(page => {
      page.title = page.position == 0 ? `Trang bìa` : `Trang ${((page.position + 1) + " - " + (page.position + 2))}`;
      return page;
    });

    const framesLayout = pagesImage[0].m_frames;
    const selectPageId = 0;

    const data = {
      pages,
      pagesImage,
      framesLayout,
      selectPageId,
      editText: {
        fonts: ["Alfa Slab One", "Amatic SC", "Anton", "Bangers", "Calistoga", "Charm", "Charmonman", "Chonburi", "Coiny",
          "Cormorant", "Lemonada", "Lobster", "Lora", "Mali", "Montserrat", "Noto Serif", "Oswald", "Pacifico", "Patrick Hand",
          "Pattaya", "Pridi", "Prompt", "Roboto Slab", "Sedgwick Ave", "Tahoma", "Viaoda Libre", "Yeseva One"],
        styles: { "normal": "Bình Thường", "bold": "In Đậm", "italic": "In Nghiêng" },
      },
      photobook_current
    };

    dispatch(Actions.setState({ ...data }));
    dispatch(appMenuAction.hideProgress());
  },

  openImageBox: createAction("OPEN_IMAGE_BOX", () => ({ isOpenImageBox: true })),
  closeImageBox: createAction("CLOSE_IMAGE_BOX", () => ({ isOpenImageBox: false, selectImages: [] })),

  openAddImage: createAction("OPEN_ADD_IMAGE_DIALOG", () => ({ isOpenAddImage: true })),
  closeAddImage: createAction("CLOSE_ADD_IMAGE_DIALOG", () => ({ isOpenAddImage: false })),

  openEditImage: (frameImage) => (dispatch) => {
    dispatch(appMenuAction.showProgress());
    dispatch(Actions.setState({ isOpenEditImage: true, frameImage }));
  },
  closeEditImage: createAction("CLOSE_EDIT_IMAGE_DIALOG", () => ({ isOpenEditImage: false })),

  openEditText: createAction("OPEN_EDIT_TEXT_DIALOG", (frameText) => ({ isOpenEditText: true, frameText, frameTextEdit: JSON.parse(JSON.stringify(frameText)) })),
  closeEditText: createAction("CLOSE_EDIT_TEXT_DIALOG", () => ({ isOpenEditText: false })),


  handleChangePage: (selectPageId) => (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    let design = JSON.parse(JSON.stringify(getState().admin.design));

    design.selectPageId = selectPageId;
    design.framesLayout = design.pagesImage[selectPageId].m_frames;

    dispatch(Actions.setState({
      selectPageId: design.selectPageId,
      framesLayout: design.framesLayout,
    }));
    dispatch(appMenuAction.hideProgress());
  },

  handleSelectPage: (pageId) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().admin.design));
    let pagesImage = JSON.parse(JSON.stringify(design.pagesImage));

    dispatch(Actions.setState({ framesLayout: pagesImage[pageId].m_frames }));
    dispatch(Actions.setState({ pagesImage, selectPageId: pageId }));
    dispatch(appMenuAction.hideProgress());
  },

  updateFrameText: (frameText) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().admin.design));
    const index = design.framesLayout.findIndex(frame => frame._id == frameText._id);
    design.framesLayout[index] = frameText;

    dispatch(Actions.setState({ framesLayout: design.framesLayout }));

    design.pagesImage[design.selectPageId].m_frames[index] = frameText;

    design.pagesImage[design.selectPageId].image = await renderHTML(design.pagesImage[design.selectPageId].m_frames);
    

    dispatch(Actions.setState({ pagesImage: design.pagesImage }));

    dispatch(Actions.setState({ isOpenEditText: false }));
    dispatch(appMenuAction.hideProgress());
  },

  handleCropImage: (frame, cropImage, infoCrop ) =>  async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    let design = JSON.parse(JSON.stringify(getState().admin.design));
    
    const index = JSON.parse(JSON.stringify(design.framesLayout)).findIndex(item => item._id == frame._id);
    
    if (index >= 0) {
      design.framesLayout[index].contentCrop = cropImage;
      design.framesLayout[index].infoCrop = infoCrop;
    }

    dispatch(Actions.setState({ framesLayout: design.framesLayout }));

    design.pagesImage[design.selectPageId].image = await renderHTML(design.framesLayout);
    design.pagesImage[design.selectPageId].m_frames = design.framesLayout;

    dispatch(Actions.setState({ isOpenEditImage: false, pagesImage: design.pagesImage }))

    dispatch(appMenuAction.hideProgress());
  },

  handleSaveDesign: (history) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());

    try {
      const design = JSON.parse(JSON.stringify(getState().admin.design));
      const orderDetail = JSON.parse(JSON.stringify(getState().admin.orderDetail)).detail;

      // const index = orderDetail.photobooks.findIndex(photobook => photobook._id == design.photobook_current);
      // let photobook = JSON.parse(JSON.stringify(orderDetail.photobooks[index]));
      await Promise.all(design.pagesImage.map(async (pageItem) => {
        const file = pageItem.image;
          if(file.match(/data:image/)) {
            const path = `upload/photobooks/${orderDetail._id}/${design.photobook_current}/pagesImage/${pageItem.position}`;
            const name = `page_${pageItem.position}.jpg`;
            const savedImage = await API.saveFileDesign({ file: file, path: path, name: name });
    
            if (pageItem.m_frames) {
              await Promise.all(pageItem.m_frames.map(async frame => {
                if (frame.contentCrop && frame.contentCrop.match(/data:image/)) {
                  const fileCrop = frame.contentCrop;
                  const pathCrop = `upload/photobooks/${orderDetail._id}/${design.photobook_current}/pagesImage/${pageItem.position}/frameCrop`;
                  const nameCrop = `${frame._id}.jpg`;
                  const savedFrameCrop = await API.saveFileDesign({ file: fileCrop, path: pathCrop, name: nameCrop });
                  
                  if(savedFrameCrop) {
                    let formDataImage = new FormData();
                    formDataImage.append('width', frame.width);
                    formDataImage.append('height', frame.height);
                    formDataImage.append('left', frame.left);
                    formDataImage.append('top', frame.top);
                    formDataImage.append('image_crop', `${pathCrop}/${nameCrop}`);
                    formDataImage.append('infoCrop', JSON.stringify(frame.infoCrop));
                    await API.updatePhotobookImage(frame._id, formDataImage);
                  }
                }
                if (frame.text) {
                  let formDataText = new FormData();
                  formDataText.append('width', frame.width);
                  formDataText.append('height', frame.height);
                  formDataText.append('left', frame.left);
                  formDataText.append('top', frame.top);
                  formDataText.append('fonts', frame.fonts);
                  formDataText.append('style', frame.style);
                  formDataText.append('color', frame.color);
                  formDataText.append('text', frame.content);
                  await API.updatePhotobookText(frame._id, formDataText);
                }
                return Promise.resolve(frame);
              }));
            }
          }
        return Promise.resolve(pageItem);
      }));


      dispatch(appMenuAction.hideProgress());
      window.location.reload();

    } catch (error) {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage(errorSystem));
    }
  },
};
