import { createAction } from "redux-actions";
import * as API from "../../api/index";
import { Actions as headerAction } from "../common/header.js";
import { Actions as msgAction } from "../common/messages.js";
import dotenv from "dotenv";
dotenv.config();

export const Actions = {
  initState: createAction("INIT_STATE_ADMIN_PROMOTION"),
  setState: createAction("SET_STATE_ADMIN_PROMOTION"),

  fetchDatas: () => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      const listPromotion = (await API.fetchPromotions()).map((promotion) => ({
        ...promotion,
        value: promotion.value?.$numberDecimal,
        min_price_order: promotion.min_price_order?.$numberDecimal,
      }));
      dispatch(Actions.setState({ listPromotion, listItemChecked: [] }));
    } catch (error) {
      console.log(error)
      dispatch(
        msgAction.showMessage({
          type: "error",
          message: "Lấy danh sách mã giảm giá thất bại.",
        })
      );
    }
    dispatch(headerAction.hideProgress());
  },

  openModalCreate: () => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalCreate: true }));
  },

  openModalEdit: (promotionSelected) => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalEdit: true, promotionSelected }));
  },

  handleItemCheck: (_id, listItemChecked) => (dispatch) => {
    if (listItemChecked.includes(_id)) {
      listItemChecked = listItemChecked.filter((e) => e !== _id);
    } else {
      listItemChecked.push(_id);
    }
    dispatch(Actions.setState({ listItemChecked }));
  },

  handleDeleteMultiple: (listItemChecked) => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      await API.deleteMultiplePromotion(listItemChecked);
      dispatch(
        Actions.setState({
          isOpenModalConfirmMultiple: false,
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "success",
          message: "Xoá danh sách mã khuyến mãi thành công.",
        })
      );
      dispatch(Actions.fetchDatas());
    } catch (error) {
      dispatch(
        Actions.setState({
          isOpenModalConfirmMultiple: false,
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "error",
          message: "Xoá mã khuyến mãi thất bại, đã có lỗi xảy ra.",
        })
      );
    }
    dispatch(headerAction.hideProgress());
  },

  closeModalCreateOrEdit: () => (dispatch) => {
    dispatch(
      Actions.setState({
        isOpenModalCreate: false,
        isOpenModalEdit: false,
        promotionSelected: {},
      })
    );
  },

  openModalConfirm: (promotionSelected) => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalConfirm: true, promotionSelected }));
  },

  openModalConfirmMultiple: () => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalConfirmMultiple: true }));
  },

  closeModalConfirm: () => (dispatch) => {
    dispatch(
      Actions.setState({
        isOpenModalConfirmMultiple: false,
        isOpenModalConfirm: false,
        promotionSelected: {},
      })
    );
  },

  handleInputChange: (e, promotionSelected) => (dispatch) => {
    const { name, value } = e.target;
    dispatch(
      Actions.setState({
        promotionSelected: {
          ...promotionSelected,
          [name]: value,
        },
      })
    );
  },

  handleDateTimePickerChange:
    (name, value, promotionSelected) => (dispatch) => {
      dispatch(
        Actions.setState({
          promotionSelected: {
            ...promotionSelected,
            [name]: value,
          },
        })
      );
    },

  handleProcessSubmitData:
    (isOpenModalCreate, promotionSelected) => async (dispatch) => {
      dispatch(headerAction.showProgress());
      try {
        let res;
        if (isOpenModalCreate) {
          res = await API.storePromotion(promotionSelected);
          dispatch(
            Actions.setState({
              isOpenModalCreate: false,
              promotionSelected: {},
            })
          );
          dispatch(
            msgAction.showMessage({
              type: "success",
              message: "Tạo mới mã khuyến mãi thành công.",
            })
          );
        } else {
          res = await API.updatePromotion(
            promotionSelected._id,
            promotionSelected
          );
          dispatch(
            Actions.setState({
              isOpenModalEdit: false,
              promotionSelected: {},
            })
          );
          dispatch(
            msgAction.showMessage({
              type: "success",
              message: "Cập nhật mã khuyến mãi thành công.",
            })
          );
        }
        dispatch(Actions.fetchDatas());
      } catch (exception) {
        dispatch(
          msgAction.showMessage({
            type: "error",
            message: `${
              isOpenModalCreate ? "Tạo mới" : "Cập nhật"
            } mã khuyến mãi thất bại, đã có lỗi xảy ra.`,
          })
        );
      }
      dispatch(headerAction.hideProgress());
    },

  handleDelete: (_id) => async (dispatch) => {
    try {
      await API.deletePromotion(_id);
      dispatch(
        Actions.setState({
          isOpenModalConfirm: false,
          photobookTypeSelected: {},
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "success",
          message: "Xoá mã khuyến mãi thành công.",
        })
      );
      dispatch(Actions.fetchDatas());
    } catch (error) {
      dispatch(
        msgAction.showMessage({
          type: "error",
          message: "Xoá mã khuyến mãi thất bại, đã có lỗi xảy ra.",
        })
      );
    }
  },
};
