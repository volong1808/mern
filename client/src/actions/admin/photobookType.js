import { createAction } from "redux-actions";
import * as API from "../../api/index";
import { Actions as headerAction } from "../common/header.js";
import { Actions as msgAction } from "../common/messages.js";
import dotenv from "dotenv";
dotenv.config();

export const Actions = {
  initState: createAction("INIT_STATE_ADMIN_PHOTOBOOK_TYPE"),
  setState: createAction("SET_STATE_ADMIN_PHOTOBOOK_TYPE"),

  fetchDatas: () => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      const listPhotobookType = (await API.fetchMPhotobookTypes()).map(
        (photobookType) => ({
          ...photobookType,
          image: process.env.REACT_APP_API_URL + "/" + photobookType.image,
        })
      );
      dispatch(Actions.setState({ listPhotobookType, listItemChecked: [] }));
    } catch (error) {
      console.log(error);
    }
    dispatch(headerAction.hideProgress());
  },

  closeModalNotify: () => async (dispatch) => {
    dispatch(Actions.setState({ msgNotify: "" }));
  },

  openModalCreate: () => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalCreate: true }));
  },

  openModalEdit: (photobookTypeSelected) => (dispatch) => {
    dispatch(
      Actions.setState({ isOpenModalEdit: true, photobookTypeSelected })
    );
  },

  handleItemCheck: (_id, listItemChecked) => (dispatch) => {
    if (listItemChecked.includes(_id)) {
      listItemChecked = listItemChecked.filter((e) => e !== _id);
    } else {
      listItemChecked.push(_id);
    }
    dispatch(Actions.setState({ listItemChecked }));
  },

  handleDeleteMultiple: (listItemChecked) => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      await API.deleteMultiplePhotobookType(listItemChecked);
      dispatch(
        Actions.setState({
          isOpenModalConfirmMultiple: false,
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "success",
          message: "Xoá danh sách photobook thành công.",
        })
      );
      dispatch(Actions.fetchDatas());
    } catch (error) {
      dispatch(
        Actions.setState({
          isOpenModalConfirmMultiple: false,
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "error",
          message: "Xoá danh sách photobook thất bại, đã có lỗi xảy ra.",
        })
      );
    }
    dispatch(headerAction.hideProgress());
  },

  closeModalCreateOrEdit:
    (objectUrl = "") =>
    (dispatch) => {
      URL.revokeObjectURL(objectUrl);
      dispatch(
        Actions.setState({
          isOpenModalCreate: false,
          isOpenModalEdit: false,
          photobookTypeSelected: {},
          objectUrl: "",
        })
      );
    },

  openModalConfirm: (photobookTypeSelected) => (dispatch) => {
    dispatch(
      Actions.setState({ isOpenModalConfirm: true, photobookTypeSelected })
    );
  },

  openModalConfirmMultiple: () => (dispatch) => {
    dispatch(Actions.setState({ isOpenModalConfirmMultiple: true }));
  },

  closeModalConfirm: () => (dispatch) => {
    dispatch(
      Actions.setState({
        isOpenModalConfirmMultiple: false,
        isOpenModalConfirm: false,
        photobookTypeSelected: {},
      })
    );
  },

  handleInputChange: (e, photobookTypeSelected) => (dispatch) => {
    const { name, value } = e.target;
    dispatch(
      Actions.setState({
        photobookTypeSelected: {
          ...photobookTypeSelected,
          [name]: value,
        },
      })
    );
  },

  handleImageChange: (e) => (dispatch) => {
    const selectedFile = e.target.files[0];
    const objectUrl = URL.createObjectURL(selectedFile);
    dispatch(Actions.setState({ objectUrl }));
  },

  handleProcessSubmitData:
    (isOpenModalCreate, photobookTypeSelected) => async (dispatch) => {
      dispatch(headerAction.showProgress());
      const formData = new FormData();
      formData.append("name", photobookTypeSelected.name ?? "");
      formData.append("descriptions", photobookTypeSelected.descriptions ?? "");
      if (photobookTypeSelected.image) {
        formData.append("image", photobookTypeSelected.image);
      }
      try {
        let res;
        if (isOpenModalCreate) {
          res = await API.storePhotobookType(formData);
          dispatch(
            Actions.setState({
              isOpenModalCreate: false,
              photobookTypeSelected: {},
              objectUrl: "",
            })
          );
          dispatch(
            msgAction.showMessage({
              type: "success",
              message: "Tạo mới loại photobook thành công.",
            })
          );
        } else {
          res = await API.updatePhotobookType(
            photobookTypeSelected._id,
            formData
          );
          dispatch(
            Actions.setState({
              isOpenModalEdit: false,
              photobookTypeSelected: {},
              objectUrl: "",
            })
          );
          dispatch(
            msgAction.showMessage({
              type: "success",
              message: "Cập nhật loại photobook thành công.",
            })
          );
        }
        dispatch(Actions.fetchDatas());
      } catch (exception) {
        dispatch(
          msgAction.showMessage({
            type: "error",
            message: "Đã có lỗi xảy ra.",
          })
        );
      }
      dispatch(headerAction.hideProgress());
    },

  handleDelete: (_id) => async (dispatch) => {
    try {
      await API.deletePhotobookType(_id);
      dispatch(
        Actions.setState({
          isOpenModalConfirm: false,
          photobookTypeSelected: {},
        })
      );
      dispatch(
        msgAction.showMessage({
          type: "success",
          message: "Xoá loại photobook thành công.",
        })
      );
      dispatch(Actions.fetchDatas());
    } catch (error) {
      dispatch(
        msgAction.showMessage({
          type: "error",
          message: "Xoá loại photobook thất bại, đã có lỗi xảy ra.",
        })
      );
    }
  },
};
