import { createAction } from "redux-actions";
import * as API from "../../api/index.js"

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";

export const Actions = {
  initState: createAction("INIT_STATE_ADMIN_PRICE_BASIC"),
  setState: createAction("SET_STATE_ADMIN_PRICE_BASIC"),
 
  fetchDatas: () => async (dispatch) => {
    const listSize = await API.fetchMSizes();
    const listPagePrice = await API.fetchMPagePrices({ m_size: listSize[0]._id});
    dispatch(Actions.setState({ listSize, listPagePrice }))
  },

  handleChangeTab: (indexTab) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    const priceBasicState = JSON.parse(JSON.stringify(getState().admin.priceBasic));
    const listPagePrice = await API.fetchMPagePrices({ m_size: priceBasicState.listSize[indexTab]._id});
    dispatch(Actions.setState({ indexTab, listPagePrice }));
    dispatch(appMenuAction.hideProgress());
  },

  handleInputChanged: (page_number, price) => (dispatch, getState) => {
    const priceBasicState = JSON.parse(JSON.stringify(getState().admin.priceBasic));
    let listPagePrice = priceBasicState.listPagePrice;
    listPagePrice = listPagePrice.map(pagePrice => {
      if (pagePrice.page_number == page_number) {
        pagePrice.price = price;
      }
      return pagePrice;
    })
    dispatch(Actions.setState({ listPagePrice }));
  },

  handleSave: () => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    const priceBasicState = JSON.parse(JSON.stringify(getState().admin.priceBasic));
    let listPagePrice = priceBasicState.listPagePrice;

    await Promise.all(listPagePrice.map(item => {
      let formData = new FormData();
      formData.append("page_number", item.page_number);
      formData.append("price", item.price);
      try {
        const result = API.updateMPagePrice(item._id, formData);
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(error)
      }
    })).then(() => {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "success", message: "Cập nhật Giá Cơ Bản thành công!" }));
    }).catch(error => {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "error", message: "Có lỗi đã xảy ra. Vui lòng thử lại!" }));
    })
    
  },

};
