import { createAction } from "redux-actions";
import * as API from "../../api/index.js";
import fillTextInit from "../../ultils/fillTextCustom.js";
import getCroppedCanvas from "../../ultils/cropper.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";

import dotenv from "dotenv";
dotenv.config();

const renderHTML = async (frames) => {
  fillTextInit();
  const ratio = 5;
  let canvas = document.createElement('canvas'); 
  canvas.width = 500*ratio;
  canvas.height = 250*ratio;
  const ctx = canvas.getContext('2d');
  // Save the current state
  ctx.save();

  ctx.fillStyle = "rgba(255,255,255,1)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.restore();
  let framesText = [];
  let framesImage = [];
  frames.forEach(frame => {
    if (frame.image) {
      framesImage.push(frame);
    } else {
      framesText.push(frame);
    }
  });
  await Promise.all(framesImage.map(async frame => {

    let img = new Image();
    img.crossOrigin = "anonymous";
    img.src = frame.image;
    await img.decode().then(async () => {
      let srcImg;
      if (frame.infoCrop) {
        let canvasData = JSON.parse(frame.infoCrop).canvasData;
        let imageData = JSON.parse(frame.infoCrop).imageData;
        let cropBoxData = JSON.parse(frame.infoCrop).cropBoxData;
        canvasData.width = canvasData.width*ratio;
        canvasData.height = canvasData.height*ratio;
        canvasData.left = canvasData.left*ratio;
        canvasData.top = canvasData.top*ratio;

        imageData.width = imageData.width*ratio;
        imageData.height = imageData.height*ratio;
        imageData.left = imageData.left*ratio;
        imageData.top = imageData.top*ratio;

        cropBoxData.width = cropBoxData.width*ratio;
        cropBoxData.height = cropBoxData.height*ratio;
        cropBoxData.left = cropBoxData.left*ratio;
        cropBoxData.top = cropBoxData.top*ratio;
        

        srcImg = getCroppedCanvas(img, canvasData, imageData, cropBoxData).toDataURL("image/jpg", 1);
      } else {
        let canvasInner = document.createElement('canvas'); 
        canvasInner.width = frame.width*ratio;
        canvasInner.height = frame.height*ratio;
        const ctxInner = canvasInner.getContext('2d');
        // get the scale
        const scale = Math.max(canvasInner.width / img.width, canvasInner.height / img.height);
        // get the top left position of the image
        const x = (canvasInner.width / 2) - (img.width / 2) * scale;
        const y = (canvasInner.height / 2) - (img.height / 2) * scale;
        ctxInner.drawImage(img, x, y, img.width * scale, img.height * scale);

        srcImg = canvasInner.toDataURL("image/jpg", 1); 
      }
      

      let imgFrame = new Image();
      imgFrame.crossOrigin = "anonymous";
      imgFrame.src = srcImg;
      await imgFrame.decode().then(() => {
        ctx.drawImage(imgFrame, frame.left*ratio, frame.top*ratio, frame.width*ratio, frame.height*ratio);
        ctx.restore();
        return Promise.resolve();
      });
      
    });
    return Promise.resolve();
  }));

  const fontSize = canvas.width*16/1096;
  const lineHeight = fontSize*1.5;
  if(framesText.length) {
    framesText.forEach(frame => {
      ctx.font = `${frame.style == "regular" ? 'normal' : frame.style} ${fontSize}px ${frame.fonts}`;
      ctx.fillStyle = frame.color;
      ctx.fillTextCustom(frame.content, frame.left*ratio, frame.top*ratio - fontSize/2, frame.width*ratio, frame.height*ratio, 'center', 'center', lineHeight);
      ctx.restore();
    });
  }

  return canvas.toDataURL("image/jpg", 1);
}

export const Actions = {
  initState: createAction("INIT_STATE_ADMIN_ORDER_DETAIL"),
  setState: createAction("SET_STATE_ADMIN_ORDER_DETAIL"),

  openEditPhotobook: createAction("OPEN_ADMIN_EDIT_PHOTOBOOK_DIALOG", (photobook_current) => ({ isOpenEditPhotobook: true, photobook_current })),
  closeEditPhotobook: createAction("CLOSE_ADMIN__EDIT_PHOTOBOOK_DIALOG", () => ({ isOpenEditPhotobook: false })),

  fetchDatas: (id, history) => async (dispatch) => {
    dispatch(appMenuAction.showProgress());
    try {
      const order = await API.getOrder(id);
      const detail = order.order;
      const payment = await API.fetchPayment({ order: id});
      let photobooks = await API.fetchOrderPhotobook({ order: id});

      photobooks = await Promise.all(photobooks.map(async photobook => {
        photobook.page = 0;
        photobook.m_photobook_type = await API.getMPhotobookTypes(photobook.m_photobook_type);
        photobook.m_size = await API.fetchMSize(photobook.m_size);
        photobook.m_cover = await API.fetchMCover(photobook.m_cover);
        let photobook_layout = await API.fetchPhotobookLayouts({ photobook_setting: photobook._id});
        photobook.photobook_layout = await Promise.all(photobook_layout.map(async layout => {
          layout.m_layout = await API.fetchMLayout(layout.m_layout);
          let frame_image = await API.fetchPhotobookFrameImage({ photobook_layout: layout._id });
          frame_image = frame_image.map(frame => ({ ...frame, image: process.env.REACT_APP_API_URL + "/" + frame.image.replace("/thumb/", "/origin/"), image_crop: frame.image_crop ? process.env.REACT_APP_API_URL + "/" + frame.image_crop : "" }))
          let frame_text = await API.fetchPhotobookFrameText({ photobook_layout: layout._id });
          frame_image = frame_image.map(frame => ({ ...frame, content: frame.image, contentCrop: frame.image_crop}));
          frame_text = frame_text.map(frame => ({ ...frame, content: frame.text }));

          layout.m_frames = [ ...frame_image, ...frame_text ];
          layout.image = process.env.REACT_APP_API_URL + "/" + layout.image;

          return Promise.resolve(layout)
        }));

        photobook.photobook_layout.sort((a, b) => a.position - b.position);
        return Promise.resolve(photobook);
      }));

      dispatch(Actions.setState({ detail: { ...detail, payment: payment.length ? payment[0] : {} }, photobooks }));
      dispatch(appMenuAction.hideProgress());
    } catch (error) {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "error", message: "Đơn hàng không hợp lệ" }));
      history.push("/admin/order");
    }
  },

  handleOnFlip: (page, indexPhotpbook) => (dispatch, getState) => {
    const orderDetailState = JSON.parse(JSON.stringify(getState().admin.orderDetail));
    let photobooks = orderDetailState.photobooks;
    photobooks[indexPhotpbook].page = page;
    dispatch(Actions.setState({ photobooks }));
  },

  handleUpdateStatus: (status) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    try {
      const orderDetailState = JSON.parse(JSON.stringify(getState().admin.orderDetail));
      let detail = orderDetailState.detail;
      detail.status = parseInt(status);
      let formData = new FormData();
      formData.append("total_price", detail.total_price);
      formData.append("payment_price", detail.payment_price);
      formData.append("advance_price", detail.advance_price);
      formData.append("address_ship", detail.address_ship);
      formData.append("ordered_at", detail.ordered_at);
      formData.append("status", detail.status);
      const result = await API.updateOrder(detail._id, formData);
      dispatch(Actions.setState({ detail }));
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "success", message: "Cập nhật Trạng Thái thành công!" }))
    } catch (error) {
      dispatch(appMenuAction.hideProgress());
      dispatch(msgAction.showMessage({ type: "error", message: "Có lỗi đã xảy ra. Vui lòng thử lại!" }))
    }
  },

  handleDownload: (order_id, photobook_setting_id) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    try {
      const orderDetailState = JSON.parse(JSON.stringify(getState().admin.orderDetail));
      const index = orderDetailState.photobooks.findIndex(photobook => photobook._id == photobook_setting_id);
      const photobook = JSON.parse(JSON.stringify(orderDetailState.photobooks[index]));

      await Promise.all(photobook.photobook_layout.map(async pageItem => {
        const pageImage =  await renderHTML(pageItem.m_frames);
        const path = `upload/temp/${order_id}/${photobook_setting_id}/pagesImage`;
        const name = `page_${pageItem.position}.jpg`;
        const savedImage = await API.saveFileDesign({ file: pageImage, path: path, name: name });
        return Promise.resolve();
      }));

      const result64 = (await API.downloadPhotobook(order_id, photobook_setting_id)).data;
      let link = document.createElement("a");
      link.href = `data:application/zip;base64,${result64.data}`;
      link.download = `photobook_${photobook_setting_id}.zip`;
      link.click();
    } catch (error) {
      dispatch(msgAction.showMessage({ type: "error", message: "Có lỗi đã xảy ra. Vui lòng thử lại!" }));
    }
    dispatch(appMenuAction.hideProgress());
  },
};
