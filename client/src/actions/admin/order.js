import { createAction } from "redux-actions";
import * as API from "../../api/index.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as appMenuAction } from "../common/appMenu.js";

export const Actions = {
  initState: createAction("INIT_STATE_ADMIN_ORDER"),
  setState: createAction("SET_STATE_ADMIN_ORDER"),

  fetchDatas: () => async (dispatch) => {
    dispatch(appMenuAction.showProgress());
    let listOrder = [];
    try {
      listOrder = await API.fetchOrders();
      if (listOrder.length) {
        listOrder = await Promise.all(listOrder.map(async order => {
          const customer = await API.getCustomer(order.customer);
          order.customer = customer;
          return Promise.resolve(order);
        }));
      }
    } catch (error) {
      dispatch(msgAction.showMessage({ type: "error", message: "Có lỗi đã xảy ra. Vui lòng thử lại!" }));
    }
  
    dispatch(Actions.setState({ listOrder: listOrder, listOrderSearch: listOrder }));
    dispatch(appMenuAction.hideProgress());
  },

  handleSearch: (searchText) => (dispatch, getState) => {
    const orderState = JSON.parse(JSON.stringify(getState().admin.order));
    let listOrderSearch = [];
    if(searchText.length !== 0) {
      listOrderSearch = JSON.parse(JSON.stringify(orderState.listOrder)).filter(order => order.code.toLowerCase().includes(searchText.toLowerCase()) || order.customer.full_name.toLowerCase().includes(searchText.toLowerCase()));
    } else {
      listOrderSearch = orderState.listOrder;
    }

    dispatch(Actions.setState({ listOrderSearch }));
  },
  handleDelete: (id) => async (dispatch, getState) => {
    dispatch(appMenuAction.showProgress());
    try {
      const orderState = JSON.parse(JSON.stringify(getState().admin.order));
      await API.deleteOrder(id);

      let photobooks = await API.fetchOrderPhotobook({ order: id});
      await API.deleteMultiSetting(photobooks.map(item => item._id));

      await Promise.all(photobooks.map(async photobook => {
        let photobook_layout = await API.fetchPhotobookLayouts({ photobook_setting: photobook._id});
        await API.deleteMultiLayout(photobook_layout.map(item => item._id));
        
        await Promise.all(photobook_layout.map(async layout => {
          let frame_image = await API.fetchPhotobookFrameImage({ photobook_layout: layout._id });
          await API.deleteMultiImage(frame_image.map(item => item._id))
          
          let frame_text = await API.fetchPhotobookFrameText({ photobook_layout: layout._id });
          await API.deleteMultiText(frame_text.map(item => item._id))

          return Promise.resolve()
        }));

        return Promise.resolve();
      }));

      const payment = await API.fetchPayment({ order: id});
      await API.deleteMultiPayment(payment.map(item => item._id));

      const listOrder = orderState.listOrder.filter(item => item._id != id);
      dispatch(Actions.setState({ listOrder: listOrder, listOrderSearch: listOrder }));
      
    } catch (error) {
      dispatch(msgAction.showMessage({ type: "error", message: "Có lỗi đã xảy ra. Vui lòng thử lại!" }));
    } 
    dispatch(appMenuAction.hideProgress());
  },
};
