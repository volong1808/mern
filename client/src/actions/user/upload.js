import { createAction } from "redux-actions";
import * as API from "../../api/index.js";
import * as Storage from "../../storage/index";
import { Actions as msgAction } from "../common/messages.js";
import { Actions as headerAction } from "../common/header.js";

const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

export const Actions = {
    initState: createAction("INIT_STATE_UPLOAD"),
    setState: createAction("SET_STATE_UPLOAD"),

    //fetch book current info
    fetchDatas: () => async (dispatch) => {
        dispatch(headerAction.showProgress());
        let currentPhotobookId = Storage.getCurrentIdPhotobookLocalStorage();
        let listPhotobook = Storage.getListPhotobookLocalStorage();
        let photoBook = listPhotobook[currentPhotobookId];

        let listCurrentImage = Storage.getAttrPhotobook("upload") || [];

        listCurrentImage = await Promise.all(listCurrentImage.map(image => {
            let canvas = document.createElement('canvas'); 
            let img = new Image();
            img.crossOrigin = "anonymous";
            img.src = image.path;
            img.onload = function(){
                canvas.width = img.width;
                canvas.height = img.height;
                const ctx = canvas.getContext('2d');
                ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                image.src = canvas.toDataURL("image/jpg");
            };
            return Promise.resolve(image);
        }));

        let imagesPerPage = 1;
        dispatch(Actions.setState({
            min: photoBook.option.pageTotal,
            max: photoBook.option.pageTotal * imagesPerPage,
            perPage: imagesPerPage,
            currentPhotobookId: currentPhotobookId,
            listCurrentImage
        })
        );
        dispatch(headerAction.hideProgress());
    },

    //handle button handle image
    handleOnclickBtn: (e, { actions, history, attrs }) => (dispatch) => {
        let action = e.currentTarget.dataset.action;
        if (action == "addImage") {
            document.getElementById("upload_image").click();
        } else if (action == "nextStep") {
            if (attrs.listCurrentImage >= attrs) {
                Storage.setAttrPhotobook("step", "design");
                history.push("/design");
            } else {
                history.push("/upload");

            }
        }
        dispatch(Actions.setState({}));
    },

    //handle upload onchange input
    handleOnChange: (e, form) => async (dispatch, getState) => {
        dispatch(headerAction.showProgress());
        const upload = JSON.parse(JSON.stringify(getState().user.upload));
        let formImage = document.getElementById(form.id);
        let formData = new FormData(formImage);
        formData.append("current_id", form.currentId);

        let listCurrentImage = upload.listCurrentImage

        try {
            let result = await API.uploadFile(formData);
            if (Array.isArray(result)) {
                result.forEach((item) => {
                    listCurrentImage.unshift({
                        key: item.key,
                        path: API_URL + "/" + item.path,
                        name: item.name,
                    })
                })
            }

            listCurrentImage = await Promise.all(listCurrentImage.map(image => {
                let canvas = document.createElement('canvas'); 
                let img = new Image();
                img.crossOrigin = "anonymous";
                img.src = image.path;
                img.onload = function(){
                    canvas.width = img.width;
                    canvas.height = img.height;
                    const ctx = canvas.getContext('2d');
                    ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                    image.src = canvas.toDataURL("image/jpg");
                };
                return Promise.resolve(image);
            }));

            dispatch(Actions.setState({ listCurrentImage: listCurrentImage }));
            Storage.setAttrPhotobook("upload", JSON.parse(JSON.stringify(listCurrentImage)).map(item => { delete item.src; return item; }));
        } catch (e) {
            console.log(e);
        }
        dispatch(headerAction.hideProgress());
    },

    //handle zoom image
    handleImage: (value) => (dispatch) => {
        dispatch(headerAction.showProgress());
        if (value.type == "deleteImage") {
            let imageId = value.id;
            var listCurrentImage = Storage.getAttrPhotobook("upload");

            for (let i = 0; i < listCurrentImage.length; i++) {
                if (listCurrentImage[i].key == imageId) {
                    var image = listCurrentImage[i];
                }
            }
            var isOpenModalConfirm = true

        } else if (value.type == "zoomImage") {
            var imagePreview = value.path;
            var isOpenModalImage = true
        }
        dispatch(Actions.setState({
            isOpenModalImage: isOpenModalImage | false,
            imagePreview: imagePreview ? imagePreview : "",
            isOpenModalConfirm: isOpenModalConfirm | false,
            imageInfor: image
        }));
        dispatch(headerAction.hideProgress());
    },

    // close modal image
    closeModalImage: () => (dispatch) => {
        dispatch(Actions.setState({ isOpenModalImage: false }));
    },

    // close modal image
    closeModalConfirm: () => (dispatch) => {
        dispatch(Actions.setState({ isOpenModalConfirm: false }));
    },

    // delete Image
    handleAccept: (imageInfor) => async (dispatch) => {
        dispatch(headerAction.showProgress());
        const design = Storage.getAttrPhotobook("design");
        let imagesFrames = [];
        if (design.pagesImage && design.pagesImage.length) {
            design.pagesImage.forEach(page => {
                if (page.layoutItem?.m_frames && Array.isArray(page.layoutItem.m_frames) && page.layoutItem.m_frames.length) {
                    page.layoutItem.m_frames.forEach(frame => {
                      if (frame?.content) {
                        imagesFrames.push(frame.content);
                      }
                    });
                  }
            });
        }

        if (imagesFrames.length && imagesFrames.includes(imageInfor.path)) {
            dispatch(msgAction.showMessage({
                type: "error",
                message: "Ảnh đang được dùng trong thiết kế nên không thể xóa"
            }));
            dispatch(headerAction.hideProgress());
            dispatch(Actions.setState({ isOpenModalConfirm: false }));
        } else {
            let imageId = imageInfor.key;
            let listCurrentImage = Storage.getAttrPhotobook("upload");
            let result = await API.deleteFileWithIdImage(imageInfor)
    
            if (result[1]) {
                listCurrentImage = listCurrentImage.filter(item => item.key != imageId);
                Storage.setAttrPhotobook("upload", listCurrentImage);
                dispatch(Actions.setState({ listCurrentImage: listCurrentImage, isOpenModalConfirm: false }));
                dispatch(headerAction.hideProgress());
            } else {
                dispatch(headerAction.hideProgress());
                dispatch(Actions.setState({ isOpenModalConfirm: false }));
            }
        }
    }
};
