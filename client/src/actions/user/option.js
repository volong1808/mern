import { createAction } from "redux-actions";
import * as API from "../../api/index";
import * as Storage from "../../storage/index";

import { Actions as headerAction } from "../common/header.js";

import dotenv from 'dotenv';
dotenv.config();
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

export const Actions = {
  initState: createAction("INIT_STATE_OPTION"),
  setState: createAction("SET_STATE_OPTION"),

  fetchStorageData: () => async (dispatch) => {
    const data = Storage.getAttrPhotobook("option");
    const photobookType = Storage.getAttrPhotobook("photobookType");
    dispatch(Actions.setState({ ...data, photobookType, pageCoverImage: photobookType.image }));
  }, 

  fetchDatas: () => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      const listPageCover = await API.fetchMCovers({ status: 1 });
      const listPageSize = await API.fetchMSizes();
      const listPageTotal = [ 20, 30, 40, 50, 60, 80, 100 ];
      dispatch(
        Actions.setState({ listPageTotal, listPageSize, listPageCover })
      );
    } catch (error) {
      console.log(error);
    }
    dispatch(headerAction.hideProgress());
  },

  handleSubmit: (e, attrs) => (dispatch) => {
    e.preventDefault();
    const { pageSize, pageTotal, pageCover, price } = attrs;
    Storage.setAttrPhotobook("option", { pageSize, pageTotal, pageCover, price });
    Storage.setAttrPhotobook("step", "upload");
  },

  handleInputChange: (e) => (dispatch, getState) => {
    const { name, value } = e.target;

    if (name == "pageCover") {
      const optionState = JSON.parse(JSON.stringify(getState().user.option));
      const listPageCover = optionState.listPageCover;
      const pageCover = listPageCover.find(item => item._id == value);
      dispatch(Actions.setState({ pageCoverImage: API_URL + "/" + pageCover.image, pageCoverDesciption: pageCover.descriptions }));
    }

    dispatch(Actions.setState({ [name]: value }));
  },

  handleResetPrice: () => (dispatch) => {
    dispatch(Actions.setState({ price: 0 }));
  },

  handleGetPrice: (attrs) => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      const price = await API.getPrice(attrs);
      dispatch(Actions.setState({ price }));
    } catch (e) {
      console.log(e);
    }
    dispatch(headerAction.hideProgress());
  },
};
