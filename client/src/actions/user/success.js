import { createAction } from "redux-actions";
import * as API from "../../api/index.js";
import dotenv from 'dotenv';
dotenv.config();

export const Actions = {
  initState: createAction("INIT_STATE_SUCCESS"),
  setState: createAction("SET_STATE_SUCCESS"),

  initPage: (orderId) => async dispatch => {
    try {
      let orderDetail = await API.getOrderById(orderId);
      orderDetail.company = await API.getConfigByKey('company');
      console.log(orderDetail);
      dispatch(Actions.setState(orderDetail))
    } catch (error) {
      console.log(error);
    }
  },

  moneyFormat: (price, sign = 'đ') => dispatch => {
    return price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' + sign
  },
};
