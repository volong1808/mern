import { createAction } from "redux-actions";
import * as API from "../../api/index.js";
import * as Storage from "../../storage/index";

import { Actions as headerAction } from "../common/header.js";

import dotenv from 'dotenv';
dotenv.config();
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

export const Actions = {
  initState: createAction("INIT_STATE_HOME"),
  setState: createAction("SET_STATE_HOME"),

  openModalConfirm: createAction("OPEN_MODAL_CONFIRM", () => ({
    isModalOpen: true,
  })),
  closeModalConfirm: createAction("CLOSE_MODAL_CONFIRM", () => ({
    isModalOpen: false,
  })),

  fetchDatas: () => async (dispatch) => {
    dispatch(headerAction.showProgress());
    try {
      const listPhotobookType = (await API.fetchMPhotobookTypes()).map(photobookType => ({ ...photobookType, image: API_URL + "/" + photobookType.image}));
      dispatch(Actions.setState({ listPhotobookType }));
    } catch (error) {
      console.log(error);
    }
    dispatch(headerAction.hideProgress());
  },
  handleGoToSettingPage: (photobookType, history) => async (dispatch) => {
    try {
      dispatch(Actions.setState({ photobookType }));
      if (Storage.getCurrentIdPhotobookLocalStorage()) {
        dispatch(Actions.openModalConfirm());
      } else {
        dispatch(Actions.handleCreateNewPhotobook(history));
      }
    } catch (error) {
      console.log(error);
    }
  },

  handleCreateNewPhotobook: (history) => async (dispatch, getState) => {
    dispatch(Actions.closeModalConfirm());
    dispatch(headerAction.showProgress());

    const idphotobookOld = Storage.getCurrentIdPhotobookLocalStorage();
    const resultDelete = await API.deleteFileWithIdPhotobook(idphotobookOld);
    const photobooksStorage = Storage.getListPhotobookLocalStorage();
    delete photobooksStorage[idphotobookOld];
    localStorage.setItem("photobook", JSON.stringify(photobooksStorage));

    Storage.setCurrentIdPhotobookLocalStorage();
    const homeState = getState().user.home;
    const newPhotobook = {
      ...Storage.getDefaultPhotobookItem(),
      photobookType: {
        ...homeState.photobookType,
      },
    };
    Storage.setItemPhotobookLocalStorage(newPhotobook);
    history.push("/option");

    dispatch(headerAction.hideProgress());
    },

  handleContinueSettingPhotobook: (history) => async (dispatch) => {
      dispatch(Actions.closeModalConfirm());
      const step = Storage.getAttrPhotobook("step");
      if (step) {
        return history.push(`/${step}`);
      }
      history.push("/option");
    },
};
