import { createAction } from "redux-actions";
import * as API from "../../api/index.js"
import * as Storage from "../../storage/index";
import fillTextInit from "../../ultils/fillTextCustom.js";

import { Actions as msgAction } from "../common/messages.js";
import { Actions as headerAction } from "../common/header.js";

import dotenv from 'dotenv';
dotenv.config();
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

const fetchMaster = async () => {
  const mCover = await API.fetchMCover(Storage.getAttrPhotobook("option").pageCover);
  const mLayouts = await API.fetchMLayouts({ is_cover: false });
  const mLayoutsCover = await API.fetchMLayouts({ is_cover: true, typeFrame: mCover.typeFrame });
  
  return { mCover, mLayouts, mLayoutsCover };
};

const renderHTML = async (frames) => {
  fillTextInit();
  const ratio = (window.innerWidth > 600) ? 2 : 0.8
  let canvas = document.createElement('canvas'); 
  canvas.width = 500*ratio;
  canvas.height = 250*ratio;
  const ctx = canvas.getContext('2d');
  // Save the current state
  ctx.save();

  ctx.fillStyle = "rgba(255,255,255,1)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.restore();
  let framesText = [];
  let framesImage = [];
  frames.forEach(frame => {
    if (frame.type == "image") {
      framesImage.push(frame);
    } else {
      framesText.push(frame);
    }
  });
  await Promise.all(framesImage.map(async frame => {
    let img = new Image();
    img.crossOrigin = "anonymous";
    img.src = frame.contentCrop || frame.content;
    await img.decode().then(async () => {
      let canvasInner = document.createElement('canvas'); 
      canvasInner.width = frame.width*ratio;
      canvasInner.height = frame.height*ratio;
      const ctxInner = canvasInner.getContext('2d');
      // get the scale
      const scale = Math.max(canvasInner.width / img.width, canvasInner.height / img.height);
      // get the top left position of the image
      const x = (canvasInner.width / 2) - (img.width / 2) * scale;
      const y = (canvasInner.height / 2) - (img.height / 2) * scale;
      ctxInner.drawImage(img, x, y, img.width * scale, img.height * scale);

      let imgFrame = new Image();
      imgFrame.crossOrigin = "anonymous";
      imgFrame.src = canvasInner.toDataURL("image/jpg");
      await imgFrame.decode().then(() => {
        ctx.drawImage(imgFrame, frame.left*ratio, frame.top*ratio);
        ctx.restore();
        return Promise.resolve();
      });
    });
    return Promise.resolve();
  }));

  const fontSize = canvas.width*16/1096;
  const lineHeight = fontSize*1.5;
  if(framesText.length) {
    framesText.forEach(frame => {
      ctx.font = `${frame.style == "regular" ? 'normal' : frame.style} ${fontSize}px ${frame.fonts}`;
      ctx.fillStyle = frame.color;
      ctx.fillTextCustom(frame.content, frame.left*ratio, frame.top*ratio - fontSize/2, frame.width*ratio, frame.height*ratio, 'center', 'center', lineHeight);
      ctx.restore();
    });
  }

  return canvas.toDataURL("image/jpg");
}

const errorSystem = { type: "error", message: "Có một lỗi đã xảy ra! Vui lòng thử lại" };

export const Actions = {
  initState: createAction("INIT_STATE_DESIGN"),
  setState: createAction("SET_STATE_DESIGN"),

  fetchDatas: () => async (dispatch) => {
    dispatch(headerAction.showProgress());
    const option = Storage.getAttrPhotobook("option");
    const upload = Storage.getAttrPhotobook("upload");
    const design = Storage.getAttrPhotobook("design");

    let pages = 10;
    if (design.pages) {
      pages = design.pages
    } else if (option.pageTotal) {
      pages = option.pageTotal / 2;
    }

    let pagesImage = design.pagesImage || [
      {
        page: 0,
        title: `Trang bìa`,
        image: API_URL + "/upload/images/frames/layout-default.jpg",
        layoutItem: { type: 0, m_frames: [] },
        layouts: []
      },
      ...[...Array(pages).keys()].map((page) => ({
        page: page + 1,
        title: `Trang ${((page + 1) + " - " + (page + 2))}`,
        image: API_URL + "/upload/images/frames/layout-default.jpg",
        layoutItem: { type: 0, m_frames: [] },
        layouts: []
      }))
    ];
    
    const layoutsImage = pagesImage[0].layouts;
    const framesLayout = pagesImage[0].layoutItem.m_frames;
    const selectLayoutId = pagesImage[0].layoutItem?._id || "";
    const selectPageId = 0;

    const data = {
      images: upload,
      pages,
      pagesImage,
      layoutsImage,
      framesLayout,
      selectLayoutId,
      selectPageId,
      editText: {
        fonts: ["Alfa Slab One", "Amatic SC", "Anton", "Bangers", "Calistoga", "Charm", "Charmonman", "Chonburi", "Coiny",
          "Cormorant", "Lemonada", "Lobster", "Lora", "Mali", "Montserrat", "Noto Serif", "Oswald", "Pacifico", "Patrick Hand",
          "Pattaya", "Pridi", "Prompt", "Roboto Slab", "Sedgwick Ave", "Tahoma", "Viaoda Libre", "Yeseva One"],
        styles: { "normal": "Bình Thường", "bold": "In Đậm", "italic": "In Nghiêng" },
      },
      selectImages: []
    };

    dispatch(Actions.setState({ ...data }));
    dispatch(headerAction.hideProgress());
  },

  openImageBox: createAction("OPEN_IMAGE_BOX", () => ({ isOpenImageBox: true })),
  closeImageBox: createAction("CLOSE_IMAGE_BOX", () => ({ isOpenImageBox: false, selectImages: [] })),

  openAddImage: createAction("OPEN_ADD_IMAGE_DIALOG", () => ({ isOpenAddImage: true })),
  closeAddImage: createAction("CLOSE_ADD_IMAGE_DIALOG", () => ({ isOpenAddImage: false })),

  openEditImage: (frameImage) => (dispatch) => {
    dispatch(headerAction.showProgress());
    dispatch(Actions.setState({ isOpenEditImage: true, frameImage }));
  },
  closeEditImage: createAction("CLOSE_EDIT_IMAGE_DIALOG", () => ({ isOpenEditImage: false })),

  openEditText: createAction("OPEN_EDIT_TEXT_DIALOG", (frameText) => ({ isOpenEditText: true, frameText, frameTextEdit: JSON.parse(JSON.stringify(frameText)) })),
  closeEditText: createAction("CLOSE_EDIT_TEXT_DIALOG", () => ({ isOpenEditText: false })),


  handleChangePage: (selectPageId) => (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    let design = JSON.parse(JSON.stringify(getState().user.design));

    design.selectPageId = selectPageId;
    design.framesLayout = design.pagesImage[selectPageId].layoutItem.m_frames;
    design.layoutsImage = design.pagesImage[selectPageId].layouts;
    design.selectLayoutId = design.pagesImage[selectPageId].layoutItem._id || "";

    dispatch(Actions.setState({
      selectPageId: design.selectPageId,
      framesLayout: design.framesLayout,
      layoutsImage: design.layoutsImage,
      selectLayoutId: design.selectLayoutId
    }));
    dispatch(headerAction.hideProgress());
  },

  handleAddImageLayout: () => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().user.design));
    const selectImages = design.selectImages;

    const mCover = await API.fetchMCover(Storage.getAttrPhotobook("option").pageCover);

    const type = design.pagesImage[design.selectPageId].layoutItem.type + selectImages.length;

    let images = design.pagesImage[design.selectPageId].layoutItem.m_frames ? design.pagesImage[design.selectPageId].layoutItem.m_frames.filter(frame => (frame.type == "image")).map(frame => frame.contentCrop || frame.content) : [];

    if (design.selectPageId == 0 && (mCover.typeFrame == 0 || mCover.typeFrame == 2) ) {
      dispatch(Actions.setState({isOpenAddImage: false , isOpenImageBox: false, selectImages: []}));
      dispatch(headerAction.hideProgress());
      dispatch(msgAction.showMessage({
        type: "error",
        message: `Không thể thêm được ảnh. Thiết kế của ${mCover.name} không có ảnh bìa`
      }))
    } else if (design.selectPageId == 0 && (selectImages.length + images.length) > 1 ) {
      dispatch(Actions.setState({isOpenAddImage: false , isOpenImageBox: false, selectImages: []}));
      dispatch(headerAction.hideProgress());
      dispatch(msgAction.showMessage({
        type: "error",
        message: "Không thể thêm được ảnh. Thiết kế tối đa 1 ảnh bìa"
      }))
    } else {
      if ((selectImages.length + images.length) > 4) {
        dispatch(Actions.setState({isOpenAddImage: false , isOpenImageBox: false, selectImages: []}));
        dispatch(headerAction.hideProgress());
        dispatch(msgAction.showMessage({
          type: "error",
          message: "Không thể thêm được ảnh. Thiết kế tối đa 4 khung ảnh"
        }))
      } else {
        try {
  
          let layouts = await API.fetchMLayouts({ is_cover: false, type: type });
          layouts = layouts.map(layout => ({ ...layout, image: API_URL + "/" + layout.image }));
  
          if (design.selectPageId == 0) {
            layouts = await API.fetchMLayouts({ is_cover: true, typeFrame: mCover.typeFrame });
            layouts = layouts.map(layout => ({ ...layout, image: API_URL + "/" + layout.image }));
          }
  
          images = [...images, ...selectImages.map(item => item.path)];
  
          let layoutSelect = layouts[Math.floor(Math.random()*layouts.length)];
  
          let framesLayout = await Promise.all(layoutSelect.m_frames.map(m_frame => API.fetchMFrame(m_frame?._id || m_frame)));
  
          framesLayout = framesLayout.map((frame) => {
            let item = frame;
            let content;
            if (frame.type == "image") {
              content = images.shift();
            } else {
              content = "Write Text Here";
            }
            return { ...item, content };
          });
  
          
          layoutSelect.m_frames = framesLayout;
  
          design.selectLayoutId = layoutSelect._id;
          design.pagesImage[design.selectPageId].layoutItem = layoutSelect;
          design.pagesImage[design.selectPageId].layouts = layouts;
  
          design.pagesImage[design.selectPageId].image = await renderHTML(layoutSelect.m_frames);
  
          
          design.layoutsImage = layouts;
          design.framesLayout = framesLayout;
  
  
          dispatch(Actions.setState({ ...design }));
          dispatch(Actions.setState({ isOpenAddImage: false, isOpenImageBox: false, selectImages: [] }));
  
          dispatch(headerAction.hideProgress());
        } catch (error) {
          dispatch(headerAction.hideProgress());
          dispatch(Actions.setState({ isOpenAddImage: false, isOpenImageBox: false, selectImages: [] }));
          dispatch(msgAction.showMessage(errorSystem));
        }
  
      }
    }
  },

  handleReplaceImageLayout: (imageReplace, frame) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().user.design));
    const index = design.framesLayout.findIndex(item => item._id == frame._id);
    design.framesLayout[index].content = imageReplace.path;
    design.framesLayout[index].contentCrop = "";

    design.pagesImage[design.selectPageId].layoutItem.m_frames = design.framesLayout;
    design.pagesImage[design.selectPageId].image = await renderHTML(design.framesLayout);

    design.frameImage.content = imageReplace.path;

    dispatch(Actions.setState({ pagesImage: design.pagesImage, framesLayout: design.framesLayout, frameImage: design.frameImage }));
    dispatch(Actions.closeAddImage());
    dispatch(Actions.closeImageBox());
    // dispatch(headerAction.hideProgress());
  },

  handleSelectLayout: (layoutId) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    let design = JSON.parse(JSON.stringify(getState().user.design));
    let pagesImage = JSON.parse(JSON.stringify(design.pagesImage));
    let framesLayout = JSON.parse(JSON.stringify(design.framesLayout));
    let layoutSelect = pagesImage[design.selectPageId].layouts.find(layout => layout._id == layoutId);

    try {
      let updateFramesLayout = await Promise.all(JSON.parse(JSON.stringify(layoutSelect)).m_frames.map(m_frame => API.fetchMFrame(m_frame?._id || m_frame)));
      updateFramesLayout = updateFramesLayout.map(frame => {
        let item = frame;
        let content;
        if (frame.type == "image") {
          const index = framesLayout.findIndex(fr => fr.type == frame.type);
          if (index >= 0) {
            content = framesLayout[index]?.contentCrop || framesLayout[index]?.content ;
            framesLayout.splice(index, 1)
          }
        } else {
          content = "Write Text Here"
        }
        
        return { ...item, content };
      });
    
      dispatch(Actions.setState({ framesLayout: updateFramesLayout }));

      layoutSelect.m_frames = updateFramesLayout;
      pagesImage[design.selectPageId].layoutItem = layoutSelect;
      pagesImage[design.selectPageId].image = await renderHTML(updateFramesLayout);
      dispatch(Actions.setState({ pagesImage, selectLayoutId: layoutId }));
      dispatch(headerAction.hideProgress());
    } catch (error) {
      dispatch(headerAction.hideProgress());
      dispatch(msgAction.showMessage(errorSystem));
    }
  },

  handleSelectPage: (pageId) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().user.design));
    let pagesImage = JSON.parse(JSON.stringify(design.pagesImage));
    const layoutsImage = pagesImage[pageId].layouts;
    const layoutSelect = pagesImage[pageId].layoutItem;

    dispatch(Actions.setState({ framesLayout: layoutSelect.m_frames }));
    dispatch(Actions.setState({ pagesImage, layoutsImage, selectPageId: pageId, selectLayoutId: layoutSelect._id || "" }));
    dispatch(headerAction.hideProgress());
  },

  handleAddPage: () => (dispatch, getState) => {
    if(window.confirm("Bạn có muốn thêm 2 trang trắng ở cuối cùng không?")) {
      dispatch(headerAction.showProgress());
      const design = JSON.parse(JSON.stringify(getState().user.design));
      const pagesLength = JSON.parse(JSON.stringify(design.pagesImage)).length;

      design.pagesImage.push({
        page: pagesLength,
        title: `Trang ${((pagesLength) + " - " + (pagesLength + 1))}`,
        image: API_URL + "/upload/images/frames/layout-default.jpg",
        layoutItem: { type: 0 },
        layouts: []
      })
      
      dispatch(Actions.setState({ pagesImage: design.pagesImage, pages: design.pagesImage.length - 1 }));
      dispatch(headerAction.hideProgress());
    }
  },

  handleDeletePage: () => (dispatch, getState) => {
    if(window.confirm("Bạn có muốn xóa 2 trang ở cuối cùng không?")) {
      dispatch(headerAction.showProgress());
      const design = JSON.parse(JSON.stringify(getState().user.design));
      if (design.pagesImage.length == 11) {
        dispatch(msgAction.showMessage({ type: "error", message: "Không thể xóa. Thiết kế photobook tối thiểu 20 trang" }));
      } else {
        design.pagesImage.splice(design.pagesImage.length - 1);
        dispatch(Actions.setState({ pagesImage: design.pagesImage, pages: design.pagesImage.length - 1 }));
      }
      dispatch(headerAction.hideProgress());
    }
  },

  handleSwapPage: (pageCurrent) => (dispatch,getState) => {
    const design = JSON.parse(JSON.stringify(getState().user.design));

    [design.pagesImage[pageCurrent - 1], design.pagesImage[pageCurrent]] = [design.pagesImage[pageCurrent], design.pagesImage[pageCurrent - 1]];

    design.pagesImage[pageCurrent].page = pageCurrent;
    design.pagesImage[pageCurrent].title = `Trang ${pageCurrent} - ${pageCurrent + 1}`;

    design.pagesImage[pageCurrent - 1].page = pageCurrent - 1;
    design.pagesImage[pageCurrent - 1].title = `Trang ${pageCurrent - 1} - ${pageCurrent}`;

    let selectPageId = design.selectPageId;
    if (design.selectPageId == pageCurrent) {
      selectPageId = pageCurrent;
    } else if (design.selectPageId == pageCurrent - 1) {
      selectPageId = pageCurrent - 1;
    }
    
    dispatch(Actions.setState({
      pagesImage: design.pagesImage,
      framesLayout: design.pagesImage[selectPageId].layoutItem.m_frames,
      layoutsImage: design.pagesImage[selectPageId].layouts,
      selectPageId
    }));
  },

  handleAutoRender: () => async (dispatch, getState) => {
    if(window.confirm("Bạn có chắc muốn bỏ thiết kế hiện tại và tạo thiết kế tự động không?")) {
      dispatch(headerAction.showProgress());
      const design = JSON.parse(JSON.stringify(getState().user.design));
      const images = design.images;

      try {
        let { mCover, mLayouts, mLayoutsCover } = await fetchMaster();
        const selectPageId = 0;

        let imagePageCover = API_URL + "/upload/images/frames/layout-default.jpg";
        let layoutCoverItem = { type: 0, typeFrame: mCover.typeFrame };
        let layoutsCover = [];
        let framesLayoutCover = [];
        if (mLayoutsCover.length) {
          mLayoutsCover = mLayoutsCover.map(mLayout => ({ ...mLayout, image: API_URL + "/" +  mLayout.image}));
          layoutCoverItem = mLayoutsCover[Math.floor(Math.random()*mLayoutsCover.length)];
          framesLayoutCover = await Promise.all(layoutCoverItem.m_frames.map(m_frame => API.fetchMFrame(m_frame)));
          framesLayoutCover = framesLayoutCover.map(frame => ({
            ...frame,
            content: (frame.type === "image") ? images[Math.floor(Math.random()*images.length)].path : "Write Text Here"
          }));
          layoutCoverItem.m_frames = framesLayoutCover;

          imagePageCover = await renderHTML(framesLayoutCover);
          layoutsCover = mLayoutsCover.filter(mlayout => mlayout.type === layoutCoverItem.type);
        }
        
        let pagesImage = [
          {
            page: 0,
            title: `Trang bìa`,
            image: imagePageCover ,
            layoutItem: layoutCoverItem,
            layouts: layoutsCover,
          }
        ];

        mLayouts = mLayouts.map(mLayout => ({ ...mLayout, image: API_URL + "/" +  mLayout.image}) );
        let itemsPage = await Promise.all([...Array(design.pages).keys()].map(async key => {
          const page = key + 1;
          let layoutPageItem = mLayouts[Math.floor(Math.random()*mLayouts.length)];
          const layoutsItem = mLayouts.filter(mlayout => mlayout.type === layoutPageItem.type);
          let framesLayoutItem = await Promise.all(layoutPageItem.m_frames.map(m_frame => API.fetchMFrame(m_frame)));
          framesLayoutItem = framesLayoutItem.map(frame => {
            let content = images[Math.floor(Math.random()*images.length)].path;
            if (frame.type === "text") {
              content = "Write Text Here";
            }
            return {...frame, content}
          }); 
          layoutPageItem.m_frames = framesLayoutItem;
          return {
            page: page,
            title: `Trang ${((page) + " - " + (page + 1))}`,
            image: API_URL + "/upload/images/frames/layout-default.jpg",
            layoutItem: layoutPageItem,
            layouts: layoutsItem
          };
        }));

        itemsPage = await Promise.all(itemsPage.map(async itemPage => {
          const imageItem = await renderHTML(itemPage.layoutItem.m_frames);
          return { ...itemPage, image: imageItem };
        }));
        pagesImage = [...pagesImage, ...itemsPage];
        
        const layoutsImage = pagesImage[0].layouts; 
        const selectLayoutId = pagesImage[0].layoutItem._id;
        const data = { pagesImage, layoutsImage, framesLayout: framesLayoutCover, selectLayoutId, selectPageId  };
        dispatch(Actions.setState(data));
        
      } catch (error) {
        dispatch(msgAction.showMessage(errorSystem));
      }
      dispatch(headerAction.hideProgress());
    }

    },

  updateFrameText: (frameText) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().user.design));
    const index = design.framesLayout.findIndex(frame => frame._id == frameText._id);
    design.framesLayout[index] = frameText;

    dispatch(Actions.setState({ framesLayout: design.framesLayout }));

    design.pagesImage[design.selectPageId].layoutItem.m_frames[index] = frameText;

    design.pagesImage[design.selectPageId].image = await renderHTML(design.pagesImage[design.selectPageId].layoutItem.m_frames);
    

    dispatch(Actions.setState({ pagesImage: design.pagesImage }));

    dispatch(Actions.setState({ isOpenEditText: false }));
    dispatch(headerAction.hideProgress());
  },

  handleCropImage: (frame, cropImage, infoCrop) =>  async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
  
    let design = JSON.parse(JSON.stringify(getState().user.design));
    
    const index = JSON.parse(JSON.stringify(design.framesLayout)).findIndex(item => item._id == frame._id);
    
    if (index >= 0) {
      design.framesLayout[index].contentCrop = cropImage;
      design.framesLayout[index].infoCrop = infoCrop;
    }

    dispatch(Actions.setState({ framesLayout: design.framesLayout }))

    design.pagesImage[design.selectPageId].image = await renderHTML(design.framesLayout);
    design.pagesImage[design.selectPageId].layoutItem.m_frames = design.framesLayout;

    dispatch(Actions.setState({ isOpenEditImage: false, pagesImage: design.pagesImage }))

    dispatch(headerAction.hideProgress());
  },

  handleSaveDesign: (history, next) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const design = JSON.parse(JSON.stringify(getState().user.design));
    const photobookId = Storage.getCurrentIdPhotobookLocalStorage();
    let pagesImage = [];

    try {
      pagesImage = await Promise.all(design.pagesImage.map((pageItem, index) => {
        return new Promise(async (rs, rj) => {
          const file = pageItem.image;
          const path = `upload/temp/${photobookId}/pagesImage/${pageItem.page}`;
          const name = `page_${pageItem.page}.jpg`;
          const savedImage = await API.saveFileDesign({ file: file, path: path, name: name });
          if(savedImage) {
            pageItem.image = `${API_URL}/${path}/${name}`;
          }
  
          if (pageItem.layoutItem?.m_frames) {
            const mFrames = await Promise.all(pageItem.layoutItem.m_frames.map(frame => {
              return new Promise(async (resolve, reject) => {
                if (frame.contentCrop) {
                  const fileCrop = frame.contentCrop;
                  const pathCrop = `upload/temp/${photobookId}/pagesImage/${pageItem.page}/frameCrop`;
                  const nameCrop = `${frame._id}.jpg`;
                  const savedFrameCrop = await API.saveFileDesign({ file: fileCrop, path: pathCrop, name: nameCrop }); 
                  if(savedFrameCrop) {
                    frame.contentCrop = `${API_URL}/${pathCrop}/${nameCrop}`;
                  }
                }
                resolve(frame);
              })
            })).then(datas => datas);
            pageItem.layoutItem.m_frames = mFrames;
          }

          rs(pageItem);
        });
      })).then(datas => datas);

      Storage.setAttrPhotobook('design', {
        pages: design.pages,
        pagesImage: pagesImage,
        layoutsImage: design.layoutsImage,
        framesLayout: design.framesLayout,
        selectLayoutId: design.selectLayoutId,
        selectPageId: design.selectPageId,
      });

      dispatch(headerAction.hideProgress());
      Storage.setAttrPhotobook("step", next);
      history.push(`/${next}`);

    } catch (error) {
      dispatch(headerAction.hideProgress());
      dispatch(msgAction.showMessage(errorSystem));
    }
  },

  handleSelectImages: (image) => (dispatch,getState) => {
    const designState = getState().user.design;
    if (designState.selectImages.includes(image)) {
      designState.selectImages = designState.selectImages.filter(item => item.key != image.key);
    } else {
      designState.selectImages.push(image);
    }
    dispatch(Actions.setState({ selectImages: designState.selectImages }));
  },

  handleUploadImage: (e, form) => async (dispatch, getState) => {
    dispatch(headerAction.showProgress());
    const designState = JSON.parse(JSON.stringify(getState().user.design));
    let formImage = document.getElementById(form.id);
    let formData = new FormData(formImage);
    formData.append("current_id", form.currentId);

    let images = designState.images;

    try {
        let result = await API.uploadFile(formData);
        if (Array.isArray(result)) {
            result.forEach((item) => {
              images.unshift({
                    key: item.key,
                    path: API_URL + "/" + item.path,
                    name: item.name,
                })
            })
        }

        Storage.setAttrPhotobook("upload", images);

        dispatch(Actions.setState({images: images}));
    } catch (e) {
        console.log(e);
    }
    dispatch(headerAction.hideProgress());
},
};
