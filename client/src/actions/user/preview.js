import { createAction } from "redux-actions";
import * as Storage from "../../storage/index";

export const Actions = {
  initState: createAction("INIT_STATE_PREVIEW"),
  setState: createAction("SET_STATE_PREVIEW"),

  fetchDatas: () => async (dispatch) => {
    let pagesImage = Storage.getAttrPhotobook("design")?.pagesImage || [];
    if (pagesImage.length) {
      pagesImage = await Promise.all(pagesImage.map(page => {
        let canvas = document.createElement('canvas'); 
        let img = new Image();
        img.crossOrigin = "anonymous";
        img.src = page.image;
        img.onload = function(){
          canvas.width = img.width;
          canvas.height = img.height;
          const ctx = canvas.getContext('2d');
          ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
          page.image = canvas.toDataURL("image/jpg");
        };
        return Promise.resolve(page);
      }));
    }
    const totalPage = pagesImage.length - 1;
    dispatch(Actions.setState({ pagesImage, totalPage }));
  },

  onPage: (e) => (dispatch) => {
    dispatch(Actions.setState({ page: e.data }));
  },

  handleContinueOrder: (history) => async (dispatch) => {
      Storage.setAttrPhotobook("preview", {});
      Storage.setAttrPhotobook("step", "order");
      history.push("/order");
    },
};
