import { createAction } from "redux-actions";
import * as API from "../../api/index.js";
import * as Storage from "../../storage/index";

import { Actions as headerActions } from "../common/header.js";
import { Actions as msgAction } from "../common/messages.js";

import dotenv from 'dotenv';
dotenv.config();
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";
const ORDER_STEP_NAME = 'order';

const photobooksOrder = () => {
  const photobooks = Storage.getListPhotobookLocalStorage();
  let photobooksOrder = [];
  Object.keys(photobooks).forEach((item, index) => {
    if (photobooks[item].step == ORDER_STEP_NAME) {
      photobooksOrder.push({
        idphotobook: item,
        ...photobooks[item]
      });
    }
  });

  photobooksOrder.reverse();

  return photobooksOrder;
};

const getInfoPhotobook = async (photobookOrder) => {
  photobookOrder = await Promise.all(photobookOrder.map((item, index) => {
    return new Promise(async (rs, rj) => {
      const size = await API.fetchMSize(item.option.pageSize);
      item.pageTotal = (item.design.pages)*2;
      item.price = await API.getPriceOrder({ pageSize: item.option.pageSize, pageCover: item.option.pageCover, pageTotal: item.pageTotal });
      item.pageSizeName = size.width + 'x' + size.height + '(cm)';
      item.quantity = 1;
      item.isPrint = (index == 0) ? true : false;
      rs(item);
    });
  }));

  return photobookOrder;
};

const getPhotoBookPrint = (photobooks) => {
  let photobooksPrint = [];
  Object.keys(photobooks).forEach((item, index) => {
    if (photobooks[item].isPrint === true) {
      photobooksPrint.push(photobooks[item]);
    }
  });

  photobooksPrint.reverse();
  return photobooksPrint;
};

const getInfoOrder = (attrs) => {
  let orderInfo = {
    full_name: attrs.full_name,
    phone: attrs.phone,
    address: attrs.address,
    note: attrs.note,
  };
  return orderInfo;
}

const getTotalPrice = (photobooksOrder) =>  {
  let total = 0;
  photobooksOrder.forEach((item, index) => {
    if (item.isPrint) {
      total = total + item.price*item.quantity;
    }
  });
  return total;
};

const getPaymentPrice = (totalPrice, promotion) =>  {
  if (promotion) {
    totalPrice = (promotion.type == 1) ? (totalPrice - totalPrice*promotion.value.$numberDecimal/100) : (totalPrice - promotion.value.$numberDecimal);
  }
  
  return totalPrice;
};

export const Actions = {
  initState: createAction("INIT_STATE_ORDER"),
  setState: createAction("SET_STATE_ORDER"),

  initPage: () => async dispatch => {
    dispatch(headerActions.showProgress());
    try {
      const paymentMethods = await API.fetchMPaymentMethod();
      const photobooks = await getInfoPhotobook(photobooksOrder());
      const totalPrice = getTotalPrice(photobooks);
      dispatch(Actions.setState({photobooks: photobooks, totalPrice, paymentPrice: getPaymentPrice(totalPrice), paymentMethods: paymentMethods}));
    } catch (error) {
      console.log(error);
    }
    dispatch(headerActions.hideProgress());
  },

  moneyFormat: (price, sign = 'đ') => dispatch => {
    return price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' + sign
  },

  handleDeletePhotobook: (idphotobook) => async (dispatch, getState) => {
    if(window.confirm("Bạn có chắc muốn xóa photobook đã thiết kế này không?")) {
      dispatch(headerActions.showProgress());
      let result = await API.deleteFileWithIdPhotobook(idphotobook);
      
      if (result[1]) {
        const orderState = getState().user.order;
        const photobooks = JSON.parse(JSON.stringify(orderState.photobooks)).filter(photobook => photobook.idphotobook != idphotobook);
        const totalPrice = getTotalPrice(photobooks);
        const paymentPrice = getPaymentPrice(totalPrice, orderState.promotion);

        dispatch(Actions.setState({ photobooks, totalPrice, paymentPrice }));

        const photobooksStorage = Storage.getListPhotobookLocalStorage();
        delete photobooksStorage[idphotobook];
        localStorage.setItem("photobook", JSON.stringify(photobooksStorage));

        dispatch(headerActions.hideProgress());
      } else {
        dispatch(headerActions.hideProgress());
        dispatch(msgAction.showMessage({ type: "error", message: "Có một lỗi đã xảy ra! Không thể xóa photobook" }));
      }
    }
  },

  handleInputChange: (e) => (dispatch) => {
    const { name, value } = e.target;
    dispatch(Actions.setState({ [name]: value }));
  },

  handleQuantity: (action, idphotobook) => (dispatch, getState) => {
    let orderState = getState().user.order;
    console.log(getState())

    const photobooks = JSON.parse(JSON.stringify(orderState.photobooks)).map(photobook => {
      if(photobook.idphotobook == idphotobook) {
        if (action == "plus") {
          photobook.quantity++;
        } else if (action == "minus" && photobook.quantity > 1) {
          photobook.quantity--;
        }
      
      }

      return photobook;
    });

    const totalPrice = getTotalPrice(photobooks);

    dispatch(Actions.setState({ photobooks, totalPrice, paymentPrice: getPaymentPrice(totalPrice, orderState.promotion) }));
  },
 
  handleOnPrint: (idphotobook) => (dispatch, getState) => {
    let orderState = getState().user.order;

    const photobooks = JSON.parse(JSON.stringify(orderState.photobooks)).map(photobook => {
      if(photobook.idphotobook == idphotobook) {
        photobook.isPrint = !photobook.isPrint;
      }

      return photobook;
    });

    const totalPrice = getTotalPrice(photobooks);

    dispatch(Actions.setState({ photobooks, totalPrice, paymentPrice: getPaymentPrice(totalPrice, orderState.promotion) }));
  },

  handlePromotion: (code) => async (dispatch, getState) => {
    dispatch(headerActions.showProgress());
    try {
      let orderState = getState().user.order;
      const promotions = await API.fetchPromotions({ code });
      if (promotions && promotions.length) {
        const promotion = promotions[0];
        const paymentPrice = getPaymentPrice(orderState.totalPrice, promotion);
        dispatch(Actions.setState({ promotion, paymentPrice, promotionError: false }));
      } else {
        dispatch(Actions.setState({ promotionError: true, promotion: null }));
      }
    } catch (error) {
      dispatch(Actions.setState({ promotionError: true, promotion: null }));
    }
    dispatch(headerActions.hideProgress());
  },

  handleOrder: (history) => async (dispatch, getState) => {
    dispatch(headerActions.showProgress());
    let orderState = getState().user.order;
    let photobookPrint = getPhotoBookPrint(orderState.photobooks);
    console.log(orderState);
    let orderInfo = getInfoOrder(orderState);
    let promotion = orderState.promotion;
    const order = await API.order({ photobooks: photobookPrint, orderInfo: orderInfo, promotion: promotion });
    Storage.deletePhotobook();
    //go to success page
    history.push("/success/" + order.order._id);

    dispatch(headerActions.hideProgress());
  }

};
