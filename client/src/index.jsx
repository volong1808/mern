import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import { syncHistoryWithStore, routerReducer } from "react-router-redux";
import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";

import reducers from "./reducers";
import App from "./App.jsx";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";

const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer
  }),
  applyMiddleware(thunk)
);
const theme = createTheme({
  palette: {
    primary: {
      main: "#0da0c5",
      contrastText: "#fff",
    },
    secondary: {
      main: "#107791",
      contrastText: "#fff",
    },
    success: {
      main: "#4caf50",
      contrastText: "#fff",
    },
    warning: {
      main: "#ff9800",
      contrastText: "#fff",
    },
    error: {
      main: "#ef5350",
      contrastText: "#fff",
    },
  },
  typography: {
    button: {
      textTransform: 'none'
    }
  }
});

const history = syncHistoryWithStore(createBrowserHistory(), store);

render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <App />
      </Router>
    </MuiThemeProvider>
  </Provider>
  ,
  document.getElementById("root")
);
