var IS_BROWSER = typeof window !== 'undefined' && typeof window.document !== 'undefined';
var WINDOW = IS_BROWSER ? window : {};

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}


var isNaN = Number.isNaN || WINDOW.isNaN;
  /**
   * Check if the given value is a number.
   * @param {*} value - The value to check.
   * @returns {boolean} Returns `true` if the given value is a number, else `false`.
   */

  function isNumber(value) {
    return typeof value === 'number' && !isNaN(value);
  }
  /**
   * Check if the given value is a positive number.
   * @param {*} value - The value to check.
   * @returns {boolean} Returns `true` if the given value is a positive number, else `false`.
   */

  var isPositiveNumber = function isPositiveNumber(value) {
    return value > 0 && value < Infinity;
  };
  /**
   * Check if the given value is undefined.
   * @param {*} value - The value to check.
   * @returns {boolean} Returns `true` if the given value is undefined, else `false`.
   */

  function isUndefined(value) {
    return typeof value === 'undefined';
  }
  /**
   * Check if the given value is an object.
   * @param {*} value - The value to check.
   * @returns {boolean} Returns `true` if the given value is an object, else `false`.
   */

  function isObject(value) {
    return _typeof(value) === 'object' && value !== null;
  }
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  /**
   * Check if the given value is a plain object.
   * @param {*} value - The value to check.
   * @returns {boolean} Returns `true` if the given value is a plain object, else `false`.
   */

  function isPlainObject(value) {
    if (!isObject(value)) {
      return false;
    }

    try {
      var _constructor = value.constructor;
      var prototype = _constructor.prototype;
      return _constructor && prototype && hasOwnProperty.call(prototype, 'isPrototypeOf');
    } catch (error) {
      return false;
    }
  }

function isFunction(value) {
  return typeof value === 'function';
}

var slice = Array.prototype.slice;


function toArray(value) {
  return Array.from ? Array.from(value) : slice.call(value);
}


function forEach(data, callback) {
  if (data && isFunction(callback)) {
    if (Array.isArray(data) || isNumber(data.length)
    /* array-like */
    ) {
        toArray(data).forEach(function (value, key) {
          callback.call(data, value, key, data);
        });
      } else if (isObject(data)) {
      Object.keys(data).forEach(function (key) {
        callback.call(data, data[key], key, data);
      });
    }
  }

  return data;
}

var REGEXP_DECIMALS = /\.\d*(?:0|9){12}\d*$/;
function normalizeDecimalNumber(value) {
  var times = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100000000000;
  return REGEXP_DECIMALS.test(value) ? Math.round(value * times) / times : value;
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function getAdjustedSizes(_ref4) // or 'cover'
  {
    var aspectRatio = _ref4.aspectRatio,
        height = _ref4.height,
        width = _ref4.width;
    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'contain';
    var isValidWidth = isPositiveNumber(width);
    var isValidHeight = isPositiveNumber(height);

    if (isValidWidth && isValidHeight) {
      var adjustedWidth = height * aspectRatio;

      if (type === 'contain' && adjustedWidth > width || type === 'cover' && adjustedWidth < width) {
        height = width / aspectRatio;
      } else {
        width = height * aspectRatio;
      }
    } else if (isValidWidth) {
      height = width / aspectRatio;
    } else if (isValidHeight) {
      width = height * aspectRatio;
    }

    return {
      width: width,
      height: height
    };
  }

function getSourceCanvas(image, _ref6, _ref7, _ref8) {
  var imageAspectRatio = _ref6.aspectRatio,
      imageNaturalWidth = _ref6.naturalWidth,
      imageNaturalHeight = _ref6.naturalHeight,
      _ref6$rotate = _ref6.rotate,
      rotate = _ref6$rotate === void 0 ? 0 : _ref6$rotate,
      _ref6$scaleX = _ref6.scaleX,
      scaleX = _ref6$scaleX === void 0 ? 1 : _ref6$scaleX,
      _ref6$scaleY = _ref6.scaleY,
      scaleY = _ref6$scaleY === void 0 ? 1 : _ref6$scaleY;
  var aspectRatio = _ref7.aspectRatio,
      naturalWidth = _ref7.naturalWidth,
      naturalHeight = _ref7.naturalHeight;
  var _ref8$fillColor = _ref8.fillColor,
      fillColor = _ref8$fillColor === void 0 ? 'transparent' : _ref8$fillColor,
      _ref8$imageSmoothingE = _ref8.imageSmoothingEnabled,
      imageSmoothingEnabled = _ref8$imageSmoothingE === void 0 ? true : _ref8$imageSmoothingE,
      _ref8$imageSmoothingQ = _ref8.imageSmoothingQuality,
      imageSmoothingQuality = _ref8$imageSmoothingQ === void 0 ? 'low' : _ref8$imageSmoothingQ,
      _ref8$maxWidth = _ref8.maxWidth,
      maxWidth = _ref8$maxWidth === void 0 ? Infinity : _ref8$maxWidth,
      _ref8$maxHeight = _ref8.maxHeight,
      maxHeight = _ref8$maxHeight === void 0 ? Infinity : _ref8$maxHeight,
      _ref8$minWidth = _ref8.minWidth,
      minWidth = _ref8$minWidth === void 0 ? 0 : _ref8$minWidth,
      _ref8$minHeight = _ref8.minHeight,
      minHeight = _ref8$minHeight === void 0 ? 0 : _ref8$minHeight;
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  var maxSizes = getAdjustedSizes({
    aspectRatio: aspectRatio,
    width: maxWidth,
    height: maxHeight
  });
  var minSizes = getAdjustedSizes({
    aspectRatio: aspectRatio,
    width: minWidth,
    height: minHeight
  }, 'cover');
  var width = Math.min(maxSizes.width, Math.max(minSizes.width, naturalWidth));
  var height = Math.min(maxSizes.height, Math.max(minSizes.height, naturalHeight)); // Note: should always use image's natural sizes for drawing as
  // imageData.naturalWidth === canvasData.naturalHeight when rotate % 180 === 90

  var destMaxSizes = getAdjustedSizes({
    aspectRatio: imageAspectRatio,
    width: maxWidth,
    height: maxHeight
  });
  var destMinSizes = getAdjustedSizes({
    aspectRatio: imageAspectRatio,
    width: minWidth,
    height: minHeight
  }, 'cover');
  var destWidth = Math.min(destMaxSizes.width, Math.max(destMinSizes.width, imageNaturalWidth));
  var destHeight = Math.min(destMaxSizes.height, Math.max(destMinSizes.height, imageNaturalHeight));
  var params = [-destWidth / 2, -destHeight / 2, destWidth, destHeight];
  canvas.width = normalizeDecimalNumber(width);
  canvas.height = normalizeDecimalNumber(height);
  context.fillStyle = fillColor;
  context.fillRect(0, 0, width, height);
  context.save();
  context.translate(width / 2, height / 2);
  context.rotate(rotate * Math.PI / 180);
  context.scale(scaleX, scaleY);
  context.imageSmoothingEnabled = imageSmoothingEnabled;
  context.imageSmoothingQuality = imageSmoothingQuality;
  context.drawImage.apply(context, [image].concat(_toConsumableArray(params.map(function (param) {
    return Math.floor(normalizeDecimalNumber(param));
  }))));
  context.restore();
  return canvas;
}

function getData(imageData, canvasData, cropBoxData) {
  var rounded = false;
  var options = {};
  var data;

  data = {
    x: cropBoxData.left - canvasData.left,
    y: cropBoxData.top - canvasData.top,
    width: cropBoxData.width,
    height: cropBoxData.height
  };
  var ratio = imageData.width / imageData.naturalWidth;
  forEach(data, function (n, i) {
    data[i] = n / ratio;
  });

  if (rounded) {
    // In case rounding off leads to extra 1px in right or bottom border
    // we should round the top-left corner and the dimension (#343).
    var bottom = Math.round(data.y + data.height);
    var right = Math.round(data.x + data.width);
    data.x = Math.round(data.x);
    data.y = Math.round(data.y);
    data.width = right - data.x;
    data.height = bottom - data.y;
  }

  data.rotate = imageData.rotate || 0;

  if (options.scalable) {
    data.scaleX = imageData.scaleX || 1;
    data.scaleY = imageData.scaleY || 1;
  }

  return data;
}

function getCroppedCanvas(image, canvasData, imageData, cropBoxData) {
  var options = {};

  var source = getSourceCanvas(image, imageData, canvasData, options); // Returns the source canvas if it is not cropped.

  var _this$getData = getData(imageData, canvasData, cropBoxData),
      initialX = _this$getData.x,
      initialY = _this$getData.y,
      initialWidth = _this$getData.width,
      initialHeight = _this$getData.height;

  var ratio = source.width / Math.floor(canvasData.naturalWidth);

  if (ratio !== 1) {
    initialX *= ratio;
    initialY *= ratio;
    initialWidth *= ratio;
    initialHeight *= ratio;
  }

  var aspectRatio = initialWidth / initialHeight;
  var maxSizes = getAdjustedSizes({
    aspectRatio: aspectRatio,
    width: options.maxWidth || Infinity,
    height: options.maxHeight || Infinity
  });
  var minSizes = getAdjustedSizes({
    aspectRatio: aspectRatio,
    width: options.minWidth || 0,
    height: options.minHeight || 0
  }, 'cover');

  var _getAdjustedSizes = getAdjustedSizes({
    aspectRatio: aspectRatio,
    width: options.width || initialWidth,
    height: options.height || initialHeight
  }),
      width = _getAdjustedSizes.width,
      height = _getAdjustedSizes.height;

  width = Math.min(maxSizes.width, Math.max(minSizes.width, width));
  height = Math.min(maxSizes.height, Math.max(minSizes.height, height));
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  canvas.width = normalizeDecimalNumber(width);
  canvas.height = normalizeDecimalNumber(height);
  context.fillStyle = options.fillColor || 'transparent';
  context.fillRect(0, 0, width, height);
  var _options$imageSmoothi = options.imageSmoothingEnabled,
      imageSmoothingEnabled = _options$imageSmoothi === void 0 ? true : _options$imageSmoothi,
      imageSmoothingQuality = options.imageSmoothingQuality;
  context.imageSmoothingEnabled = imageSmoothingEnabled;

  if (imageSmoothingQuality) {
    context.imageSmoothingQuality = imageSmoothingQuality;
  } // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D.drawImage


  var sourceWidth = source.width;
  var sourceHeight = source.height; // Source canvas parameters

  var srcX = initialX;
  var srcY = initialY;
  var srcWidth;
  var srcHeight; // Destination canvas parameters

  var dstX;
  var dstY;
  var dstWidth;
  var dstHeight;

  if (srcX <= -initialWidth || srcX > sourceWidth) {
    srcX = 0;
    srcWidth = 0;
    dstX = 0;
    dstWidth = 0;
  } else if (srcX <= 0) {
    dstX = -srcX;
    srcX = 0;
    srcWidth = Math.min(sourceWidth, initialWidth + srcX);
    dstWidth = srcWidth;
  } else if (srcX <= sourceWidth) {
    dstX = 0;
    srcWidth = Math.min(initialWidth, sourceWidth - srcX);
    dstWidth = srcWidth;
  }

  if (srcWidth <= 0 || srcY <= -initialHeight || srcY > sourceHeight) {
    srcY = 0;
    srcHeight = 0;
    dstY = 0;
    dstHeight = 0;
  } else if (srcY <= 0) {
    dstY = -srcY;
    srcY = 0;
    srcHeight = Math.min(sourceHeight, initialHeight + srcY);
    dstHeight = srcHeight;
  } else if (srcY <= sourceHeight) {
    dstY = 0;
    srcHeight = Math.min(initialHeight, sourceHeight - srcY);
    dstHeight = srcHeight;
  }

  var params = [srcX, srcY, srcWidth, srcHeight]; // Avoid "IndexSizeError"

  if (dstWidth > 0 && dstHeight > 0) {
    var scale = width / initialWidth;
    params.push(dstX * scale, dstY * scale, dstWidth * scale, dstHeight * scale);
  } // All the numerical parameters should be integer for `drawImage`
  // https://github.com/fengyuanchen/cropper/issues/476


  context.drawImage.apply(context, [source].concat(_toConsumableArray(params.map(function (param) {
    return Math.floor(normalizeDecimalNumber(param));
  }))));
  return canvas;
}

export default getCroppedCanvas;