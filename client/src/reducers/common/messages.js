import { handleActions } from "redux-actions";
import { Actions } from "../../actions/common/messages";

export default handleActions({
  [Actions.showMessages]: (state, { payload }) =>  {
    if (state && state.length) {
      return ([].concat(state.filter(m => 
        !payload.some(r => {
          /* eslint eqeqeq: 0 */
          return m.check == r.check 
            && m.target == r.target
            && m.type == r.type
            && m.message == r.message
        })), payload));
    }
    return (payload);
  },
  [Actions.closeMessage]: (state, { payload }) => (state.filter(r => payload.uuid !== r.uuid)),
  
}, []);