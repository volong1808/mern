import { handleActions } from "redux-actions";
import { Actions } from "../../actions/common/header";

export default handleActions({
  
  [Actions.showProgress]: (state) => ({ ...state, progress: true }),
  [Actions.hideProgress]: (state) => ({ ...state, progress: false }),

}, []);

