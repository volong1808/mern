import { handleActions } from "redux-actions";
import { Actions } from "../../actions/common/appMenu.js";

const initialState = {
  isOpen: true
};

export default handleActions({
  [Actions.setTitle]: (state, { payload }) => ({ ...state, title: payload }),
  // [Actions.setCurrentUser]: (state, { payload }) => ({ ...state, ...payload }),
  [Actions.showProgress]: (state) => ({ ...state, progress: true }),
  [Actions.hideProgress]: (state) => ({ ...state, progress: false }),
  [Actions.toggleMenu]: (state) => ({ ...state, isOpen: !state.isOpen }),
  [Actions.closeMenu]: (state) => ({ ...state, isOpen: false }),
  [Actions.showProfile]: (state) => ({ ...state, profile: true }),
  [Actions.closeProfile]: (state) => ({ ...state, profile: false }),

}, initialState);