/* eslint-disable import/no-anonymous-default-export */
import { combineReducers } from 'redux';

import auth from './auth/auth';

import messages from './common/messages';
import header from './common/header';
import appMenu from './common/appMenu';

import dropdownMenu from './modules/dropdownMenu';

import home from './user/home';
import design from './user/design';
import upload from './user/upload';
import option from './user/option';
import preview from './user/preview';
import order from './user/order';
import success from './user/success';

import photobookType from './admin/photobookType';
import covers from './admin/covers';
import priceBasic from './admin/priceBasic';
import orderAdmin from './admin/order';
import orderDetailAdmin from './admin/orderDetail';
import designAdmin from './admin/design';
import promotion from './admin/promotion';

export default {
    auth: combineReducers({
        auth
    }),
    common: combineReducers({
        messages,
        header,
        appMenu
    }),
    modules: combineReducers({
        dropdownMenu
    }),
    user: combineReducers({
        home,
        design,
        upload,
        option,
        preview,
        order,
        success
    }),
    admin: combineReducers({
        photobookType,
        covers,
        priceBasic,
        order: orderAdmin,
        orderDetail: orderDetailAdmin,
        design: designAdmin,
        promotion
    }),
};
