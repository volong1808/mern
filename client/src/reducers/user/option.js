import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/option";

const initialState = {
  pageSize: "",
  pageTotal: "",
  pageCover: "",
  listPageSize: [],
  listPageTotal: [],
  listPageCover: [],
  price: 0,
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
