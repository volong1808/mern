import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/home";

const initialState = {
  listPhotobookType: [],
  photobookType: {},
  isModalOpen: false,
};

export default handleActions({
  [Actions.initState]: () => ({ ...initialState }),
  [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),

  [Actions.openModalConfirm]: (state, { payload }) => ({ ...state, ...payload }),
  [Actions.closeModalConfirm]: (state, { payload }) => ({ ...state, ...payload }),

}, { ...initialState });

