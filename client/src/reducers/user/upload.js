import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/upload";

const initialState = {};

export default handleActions({
  [Actions.initState]: () => ({ ...initialState }),
  [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),

}, { ...initialState });

