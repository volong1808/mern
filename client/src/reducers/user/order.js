import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/order";

const initialState = {};

export default handleActions({
  [Actions.initState]: () => ({ ...initialState }),
  [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),

}, { ...initialState });

