import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/preview";

const initialState = {
  page: 0,
  totalPage: 0,
  screeHt: 1,
  pagesImage: [],
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
