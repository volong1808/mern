import { handleActions } from "redux-actions";
import { Actions } from "../../actions/user/design";

const initialState = {};

export default handleActions({
  [Actions.initState]: () => ({ ...initialState }),
  [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),

  [Actions.openImageBox]: (state, { payload }) => ({ ...state, ...payload }),
  [Actions.closeImageBox]: (state, { payload }) => ({ ...state, ...payload }),

  [Actions.openAddImage]: (state, { payload }) => ({ ...state, ...payload }),
  [Actions.closeAddImage]: (state, { payload }) => ({ ...state, ...payload }),

  [Actions.closeEditImage]: (state, { payload }) => ({ ...state, ...payload }),

  [Actions.openEditText]: (state, { payload }) => ({ ...state, ...payload }),
  [Actions.closeEditText]: (state, { payload }) => ({ ...state, ...payload }),

}, { ...initialState });

