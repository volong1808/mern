import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/photobookType.js";

const initialState = { 
  page: 0,
  listPhotobookType: [],
  isOpenModalConfirmMultiple: false,
  isOpenModalConfirm: false,
  isOpenModalCreate: false,
  isOpenModalEdit: false,
  photobookTypeSelected: {},
  objectUrl: "",
  listItemChecked : [],
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
