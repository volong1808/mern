import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/order.js";

const initialState = { page: 0, listOrder: [], listOrderSearch: [] };

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
