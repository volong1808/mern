import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/covers.js";

const initialState = { page: 0 };

export default handleActions(
    {
        [Actions.initState]: () => ({ ...initialState }),
        [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
    },
    { ...initialState }
);
