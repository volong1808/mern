import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/promotion.js";

const initialState = { 
  page: 0,
  listPromotion: [],
  isOpenModalConfirmMultiple: false,
  isOpenModalConfirm: false,
  isOpenModalCreate: false,
  isOpenModalEdit: false,
  promotionSelected: {},
  listItemChecked : [],
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
