import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/orderDetail.js";

const initialState = { 
  detail: {},
  photobooks: [],
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),

    [Actions.openEditPhotobook]: (state, { payload }) => ({ ...state, ...payload }),
    [Actions.closeEditPhotobook]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
