import { handleActions } from "redux-actions";
import { Actions } from "../../actions/admin/priceBasic.js";

const initialState = {
  indexTab: 0, 
  listSize: [], 
  listPagePrice: [], 
  pageNumbers: [ 20, 30, 40, 50, 60, 80, 100 ] 
};

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
