import { Actions } from "../../actions/modules/dropdownMenu";

export default (state = {}, action) => {
  // console.log(action)
  switch (action.type) {
    case Actions.TOGGLE_DD_MENU:
    case Actions.CLOSE_DD_MENU:
      return Object.assign({}, state, {
        ...state,
        [action.id]: {
          ...state[action.id],
          ...action.payload
        }
      });
    default:
      return state;
  }
};