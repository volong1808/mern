import { handleActions } from "redux-actions";
import { Actions } from "../../actions/auth/auth.js";

const initialState = { username: '', password: '' };

export default handleActions(
  {
    [Actions.initState]: () => ({ ...initialState }),
    [Actions.setState]: (state, { payload }) => ({ ...state, ...payload }),
  },
  { ...initialState }
);
