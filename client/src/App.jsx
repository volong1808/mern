import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter, BrowserRouter, Switch, Route } from "react-router-dom";

import Home from './components/user/Home';
import Design from './components/user/Design';
import Upload from './components/user/Upload';
import Option from './components/user/Option';
import Preview from './components/user/Preview';
import Order from './components/user/Order';
import Success from './components/user/Success';
import Auth from './components/auth/Auth';
import AppAdmin from './AppAdmin';

export class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/design" component={Design} />
          <Route path="/upload" exact component={Upload} />
          <Route path="/option" exact component={Option} />
          <Route path="/preview" exact component={Preview} />
          <Route path="/order" exact component={Order} />
          <Route path="/success/:idOrder" exact component={Success} />
          <Auth>
            <AppAdmin />
          </Auth>
        </Switch >
      </BrowserRouter >
    );
  }
};

function mapDispatchToProps(dispatch) {
  return {
    actions: {
    }
  };
}

function mapStateToProps(state) {
  return {};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
