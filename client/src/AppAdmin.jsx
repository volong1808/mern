import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter, BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import AppMenu from './components/common/AppMenu';
import PhotobookType from './components/admin/PhotobookType';
import Covers from './components/admin/Covers';
import PriceBasic from './components/admin/PriceBasic';
import Order from './components/admin/Order';
import OrderDetail from './components/admin/OrderDetail';
import Promotion from './components/admin/Promotion';

export class AppAdmin extends React.Component {

  render() {

    return (
      <AppMenu>
        <Route path="/admin" render={() => <Redirect to={{ pathname: "/admin/order" }} />} />
        <Switch>
          <Route exact path="/admin/order" component={Order} />
          <Route exact path="/admin/order/:id" component={OrderDetail} />
        </Switch>
        <Route path="/admin/photobook-type" exact component={PhotobookType} />
        <Route path="/admin/covers" exact component={Covers} />
        <Route path="/admin/price-basic" exact component={PriceBasic} />
        <Route path="/admin/promotion" exact component={Promotion} />
        <Route path="*" render={() => <Redirect to={{ pathname: "/admin/order" }} />} />
      </AppMenu>
    );
  }
};

function mapDispatchToProps(dispatch) {
  return {
    actions: {
    }
  };
}

function mapStateToProps(state) {
  return {};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppAdmin));
