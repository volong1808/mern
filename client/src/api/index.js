import axios from "axios";

const option = {
    baseURL: process.env.REACT_APP_API_URL || "http://localhost:5000",
};
const API = axios.create(option);

API.interceptors.request.use((req) => {
    if (localStorage.getItem('profile')) {
        req.headers.Authorization = `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`;
    }

    return req;
});

export const fetchDemo = () => API.get(`/demo`);

export async function fetchMPhotobookTypes() {
    const res = await API.get("/m-photobook-type");
    return res.data.data;
}

export async function getMPhotobookTypes(id) {
    const res = await API.get("/m-photobook-type/" + id);
    return res.data.data;
}

export async function storePhotobookType(formData) {
    const config = {
        headers: { "content-type": "multipart/form-data" },
    };
    const res = await API.post(`/m-photobook-type`, formData, config);
    return res.data.data;
}

export async function updatePhotobookType(_id, formData) {
    const config = {
        headers: { "content-type": "multipart/form-data" },
    };
    const res = await API.put(`/m-photobook-type/${_id}`, formData, config);
    return res.data.data;
}

export async function deletePhotobookType(_id) {
    const res = await API.delete(`/m-photobook-type/${_id}`);
    return res.data.data;
}

export async function deleteMultiplePhotobookType(ids) {
    const res = await API.delete(`/m-photobook-type`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export const fetchMCovers = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`
        return (await API.get(`/m-cover?${query}`)).data.data
    } else {
        return (await API.get(`/m-cover`)).data.data
    }
};

export async function deleteMCover(id) {
    const url = `/m-cover/${id}`;
    try {
        return API.delete(url, { params: { id } })
            .then(response => {
                let result = Object.values(response.data);
                if (result[0] == "success") {
                    return true;
                }
            })
            .catch(error => {
                console.log(error);
            });
    } catch (err) {
        console.log(err);
    }
}

// add cover
export const addMCover = async (dataSubmit) => {
    const url = '/m-cover';
    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    }
    try {
        return API.post(url, dataSubmit, config)
            .then(response => {
                if (response.data.data) {
                    return response.data.data;
                } else {
                    return false;
                }
            })
            .catch(error => {
                console.log(error);
            });
    } catch (ex) {
        console.log(ex);
    }
};
// edit image
export const updateMCover = async (dataSubmit, id) => {

    const url = `/m-cover/${id}`;
    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    }
    try {
        return API.put(url, dataSubmit, config)
            .then(response => {
                let result = Object.values(response.data.data);
                if (Array.isArray(result)) {
                    return true;
                } else {
                    return false;
                }
            })
            .catch(error => {
                console.log(error);
            });
    } catch (ex) {
        console.log(ex);
    }
};

export const fetchMCoverPrices = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`
        return (await API.get(`/m-cover-price?${query}`)).data.data
    } else {
        return (await API.get(`/m-cover-price`)).data.data
    }
};

export const updateMCoverPrice = async (_id, formData) => {
    const res = await API.put(`/m-cover-price/${_id}`, formData);
    return res.data.data;
};

export const storeMCoverPrice = async (formData) => {
    const res = await API.post(`/m-cover-price`, formData);
    return res.data.data;
};

export const deleteMCoverPrice = async (id) => {
    const res = await API.delete(`/m-cover-price/${id}`);
    return res.data.data;
};

export async function fetchMSizes() {
    const res = await API.get("/m-size");
    return res.data.data;
}

export async function getPrice({ pageSize, pageTotal, pageCover }) {
    const res = await API.get("option/calculate-price", {
        params: {
            m_size: pageSize,
            m_cover: pageCover,
            page_number: pageTotal,
        },
    });
    return res.data.data;
}

// upload image files
export const uploadFile = async (dataSubmit) => {
    const url = 'upload/upload-file';
    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    };
    try {
        return API.post(url, dataSubmit, config)
            .then(response => {
                let result = Object.values(response.data.data);
                return result;
            })
            .catch(error => {
                console.log(error);
            });
    } catch (ex) {
        console.log(ex);
    }
};

export const deleteFileWithIdImage = async (image) => {
    const url = 'upload/delete-file';

    try {
        return API.delete(url, { params: image })
            .then(response => {
                let result = Object.values(response.data);
                return result;
            })
            .catch(error => {
                console.log(error);
            });
    } catch (ex) {
        console.log(ex);
    }
};

export const deleteFileWithIdPhotobook = async (idphotobook) => {
    const url = 'upload/delete-image-photobook';
    try {
        return API.delete(url, { params: { idphotobook } })
            .then(response => {
                let result = Object.values(response.data);
                return result;
            })
            .catch(error => {
                console.log(error);
            });
    } catch (err) {
        console.log(err);
    }
};

export const fetchMCover = async (id) => (await API.get(`/m-cover/${id}`)).data.data;

export const fetchMLayouts = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`
        return (await API.get(`/m-layout?${query}`)).data.data
    } else {
        return (await API.get(`/m-layout`)).data.data
    }
};

export const fetchMLayout = async (id) => (await API.get(`/m-layout/${id}`)).data.data;

export const fetchMFrame = async (id) => (await API.get(`/m-frame/${id}`)).data.data;

export const saveFileDesign = async ({ file, path, name }) => (await API.post(`/save-file-design`, { file, path, name })).data.saved;

export async function fetchMPaymentMethod() {
    const res = await API.get("/m-payment-method");
    return res.data.data;
}

export const fetchMSize = async (id) => (await API.get(`/m-size/${id}`)).data.data;

export async function getPriceOrder({ pageSize, pageCover, pageTotal }) {
    const res = await API.get("/order/price", {
        params: {
            m_size: pageSize,
            m_cover: pageCover,
            pageTotal: pageTotal,
        },
    });
    return res.data.data;
}

export const fetchPromotions = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`
        return (await API.get(`/promotion?${query}`)).data.data
    } else {
        return (await API.get(`/promotion`)).data.data
    }
};

export const login = (formData) => API.post('/user/login', formData);


export const fetchMPagePrices = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`
        return (await API.get(`/m-page-price?${query}`)).data.data
    } else {
        return (await API.get(`/m-page-price`)).data.data
    }
};

export const updateMPagePrice = async (_id, formData) => {
    const res = await API.put(`/m-page-price/${_id}`, formData);
    return res.data.data;
};

export async function order({ photobooks, orderInfo, promotion }) {
    const res = await API.post("/order", {
        params: {
            photobooks: photobooks,
            orderInfo: orderInfo,
            promotion: promotion,
        },
    });
    return res.data.data;
};

export async function getOrderById(orderId) {
    const res = await API.get('/order/' + orderId, {});
    return res.data.data;
}


export async function getConfigByKey(key) {
    const res = await API.get('/m-config/get-by-key/' + key, {});
    return res.data.data;
}

export const fetchOrders = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/order?${query}`)).data.data
    } else {
        return (await API.get(`/order`)).data.data
    }
};

export const getOrder = async (id) => (await API.get(`/order/${id}`)).data.data;

export const updateOrder = async (_id, formData) => {
    const res = await API.put(`/order/${_id}`, formData);
    return res.data.data;
};

export const getCustomer = async (id) => (await API.get(`/customer/${id}`)).data.data;

export const fetchPayment = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/payment?${query}`)).data.data
    } else {
        return (await API.get(`/payment`)).data.data
    }
};

export const fetchOrderPhotobook = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/photobook-setting?${query}`)).data.data
    } else {
        return (await API.get(`/photobook-setting`)).data.data
    }
};

export const fetchPhotobookLayouts = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/photobook-layout?${query}`)).data.data
    } else {
        return (await API.get(`/photobook-layout`)).data.data
    }
};

export const fetchPhotobookFrameImage = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/photobook-image?${query}`)).data.data
    } else {
        return (await API.get(`/photobook-image`)).data.data
    }
};

export const fetchPhotobookFrameText = async (condition) => {
    if (condition) {
        const query = `${Object.keys(condition).map(key => `${key}=${condition[key]}`).join("&")}`;
        return (await API.get(`/photobook-text?${query}`)).data.data
    } else {
        return (await API.get(`/photobook-text`)).data.data
    }
};

export const updatePhotobookImage = async (_id, formData) => {
    const res = await API.put(`/photobook-image/${_id}`, formData);
    return res.data.data;
};

export const updatePhotobookText = async (_id, formData) => {
    const res = await API.put(`/photobook-text/${_id}`, formData);
    return res.data.data;
};

export const downloadPhotobook = async (order_id, photobook_setting_id, size) => {
    return await API.get(`/download/photobook?order_id=${order_id}&photobook_setting_id=${photobook_setting_id}&size=${JSON.stringify(size)}`);
};

export async function deleteOrder(_id) {
    const res = await API.delete(`/order/${_id}`);
    return res.data.data;
}

export async function deleteMultiSetting(ids) {
    const res = await API.delete(`/photobook-setting`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export async function deleteMultiLayout(ids) {
    const res = await API.delete(`/photobook-setting`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export async function deleteMultiImage(ids) {
    const res = await API.delete(`/photobook-image`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export async function deleteMultiText(ids) {
    const res = await API.delete(`/photobook-text`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export async function deleteMultiPayment(ids) {
    const res = await API.delete(`/payment`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}

export async function storePromotion(formData) {
    const res = await API.post(`/promotion`, formData);
    return res.data.data;
}

export async function updatePromotion(_id, formData) {
    const res = await API.put(`/promotion/${_id}`, formData);
    return res.data.data;
}

export async function deletePromotion(_id) {
    const res = await API.delete(`/promotion/${_id}`);
    return res.data.data;
}

export async function deleteMultiplePromotion(ids) {
    const res = await API.delete(`/promotion`, {
        data: {
            ids: ids,
        },
    });
    return res.data.data;
}