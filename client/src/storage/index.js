export function setCurrentIdPhotobookLocalStorage(
    idPhotobook = new Date().getTime()
) {
    localStorage.setItem("photobook_current", idPhotobook);
}

export function getCurrentIdPhotobookLocalStorage() {
    return localStorage.getItem("photobook_current");
}

export function getDefaultPhotobookItem() {
    return {
        photobookType: {},
        option: {
            pageSize: "",
            pageTotal: "",
            pageCover: "",
            price: 0,
        },
        upload: [],
        design: {},
        order: {},
        step: "option",
    };
}

export function getListPhotobookLocalStorage() {
    const photobooks = localStorage.getItem("photobook");
    return JSON.parse(photobooks) ?? {};
}

export function setListPhotobookLocalStorage(photobooks = {}) {
    const strPhotobooks = JSON.stringify(photobooks);
    localStorage.setItem("photobook", strPhotobooks);
}

export function getItemPhotobookLocalStorage() {
    const photobooks = getListPhotobookLocalStorage();
    const currentId = getCurrentIdPhotobookLocalStorage();
    return photobooks[currentId] ?? getDefaultPhotobookItem();
}

export function setItemPhotobookLocalStorage(photobook) {
    const currentId = getCurrentIdPhotobookLocalStorage();
    const newPhotobooks = {
        [currentId]: photobook,
    };
    setListPhotobookLocalStorage(newPhotobooks);
}

export function getAttrPhotobook(name) {
    const photobookItem = getItemPhotobookLocalStorage();
    return photobookItem?.[name];
}

export function setAttrPhotobook(name, attr) {
    const photobook = getItemPhotobookLocalStorage();
    const newPhotobook = {
        ...photobook,
        [name]: attr,
    };
    setItemPhotobookLocalStorage(newPhotobook);
}

export function deletePhotobook() {
    localStorage.removeItem("photobook_current");
    localStorage.removeItem("photobook");
}


function getStep(name) {
    switch (name) {
        case "option":
            return "upload";
        case "upload":
            return "design";
        case "design":
            return "preview";
        case "preview":
            return "order";
        default:
            return null;
    }
}
