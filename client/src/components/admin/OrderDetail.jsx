import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import { Paper, Chip, Button, Typography, Grid, Box, RadioGroup, Radio, FormControlLabel, DialogTitle, IconButton } from '@material-ui/core';

import RecentActorsIcon from '@material-ui/icons/RecentActors';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import EventNoteIcon from '@material-ui/icons/EventNote';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import EditIcon from '@material-ui/icons/Edit';
import GetAppIcon from '@material-ui/icons/GetApp';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from "@material-ui/icons/Save";

import ProgressScreen from "../modules/ProgressScreen";
import FlipBook from "../modules/FlipBook";

import { Actions } from "../../actions/admin/orderDetail";
import { Actions as designActions } from "../../actions/admin/design";

import ResponsiveComponent from "../common/ResponsiveComponent";
import ResponsiveDialog from "../modules/ResponsiveDialog";
import Design from "./Design";

const styles = (theme) => ({
  root: {
    padding: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    margin: "20px 0"
  },
  
  colorPrimary: {
    color: "#fff",
    backgroundColor: theme.palette.primary.main
  },
  colorWarning: {
    color: "#fff",
    backgroundColor: theme.palette.warning.main
  },
  colorSuccess: {
    color: "#fff",
    backgroundColor: theme.palette.success.main
  },
  warning: {
    color: theme.palette.warning.main
  },
  success: {
    color: theme.palette.success.main
  },
  infoItem: {
    padding: "15px 0",
    borderBottom: "1px solid #ccc",
    "&:last-child": {
      borderBottom: "none"
    }
  },
  infoItemTitle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    "& p": {
      fontWeight: 600,
      fontSize: "15px",
      marginLeft: "5px"
    }
  },
  infoItemPrice: {
    maxWidth: 300,
    textAlign: "right"
  },
  boxTitle: {
    marginBottom: 15,
    fontWeight: "bold"
  },
  boxWrap: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  textTitle: {
    textAlign: "center",
    fontSize: "15px",
    fontWeight: "bold",
    marginBottom: 15
  },
  btnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    padding: "5px 0",
    alignItems: "center",
  },
});

class OrderDetail extends ResponsiveComponent {

  renderStatus(status) {
    let label, colorClass;
    
    switch (status) {
      case 0:
        label = "Mới";
        colorClass = "colorPrimary";
        break;
      case 1:
        label = "Đã In";
        colorClass = "colorWarning";   
        break;
      case 2:
        label = "Đã Giao Hàng";
        colorClass = "colorSuccess";  
        break;
  
    }
    return <Chip size="small" label={label} style={{cursor: "pointer"}} className={this.props.classes[colorClass]} />
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.actions.fetchDatas(id, this.props.history);
  }

  componentWillUnmount() {
    this.props.actions.initState();
  }

  render() {
    const { classes, actions, designActions, attrs, history } = this.props;
    if (!attrs.detail || !attrs.detail.customer) {
      return null
    } else {
      return (
        <div className={classes.root}>
          <Grid container spacing={2} justifyContent="center">
            <Grid item sm={6} xs={12}>
              <Paper className={classes.paper}>
                <Typography variant="h6" className={classes.boxTitle}>Thông tin Khách Hàng</Typography>
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={4} xs={12} className={classes.infoItemTitle}>
                    <RecentActorsIcon fontSize="small" /><Typography>{"Họ & Tên"}</Typography>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    <Typography variant="subtitle2">{attrs.detail.customer.full_name}</Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={4} xs={12} className={classes.infoItemTitle}>
                    <PhoneIphoneIcon fontSize="small" /><Typography>{"Số Điện Thoại"}</Typography>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    <Typography variant="subtitle2">{attrs.detail.customer.phone}</Typography>
                  </Grid>
                </Grid>
                {attrs.detail.customer.email ? 
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={4} xs={12} className={classes.infoItemTitle}>
                    <MailOutlineIcon fontSize="small" /><Typography>{"Email"}</Typography>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    <Typography variant="subtitle2">{attrs.detail.customer.email}</Typography>
                  </Grid>
                </Grid>
                  : null
                }
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={4} xs={12} className={classes.infoItemTitle}>
                    <LocationOnIcon fontSize="small" /><Typography>{"Địa Chỉ Giao Hàng"}</Typography>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    <Typography variant="subtitle2">{attrs.detail.address_ship}</Typography>
                  </Grid>
                </Grid>
                {attrs.detail.note ? 
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={4} xs={12} className={classes.infoItemTitle}>
                    <EventNoteIcon fontSize="small" /><Typography>{"Ghi Chú"}</Typography>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    <Typography variant="subtitle2">{attrs.detail.note}</Typography>
                  </Grid>
                </Grid>
                  : null
                }
              </Paper>
            </Grid>
            <Grid item sm={6} xs={12}>
              <Paper className={classes.paper}>
                <Typography variant="h6" className={classes.boxTitle}>Thông tin Đơn Hàng</Typography>

                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={3} xs={6} className={classes.infoItemTitle}>
                    <MonetizationOnIcon fontSize="small" /><Typography>{"Tổng tiền"}</Typography>
                  </Grid>
                  <Grid item sm={9} xs={6}>
                    <Typography variant="subtitle1" className={classes.infoItemPrice}>{`${attrs.detail.total_price.toLocaleString()}đ`}</Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={3} xs={6} className={classes.infoItemTitle}>
                    <MonetizationOnIcon fontSize="small" /><Typography>{"Giảm giá"}</Typography>
                  </Grid>
                  <Grid item sm={9} xs={6}>
                    <Typography variant="subtitle1" className={classes.infoItemPrice}>{`${attrs.detail.discount_price ? attrs.detail.discount_price.toLocaleString() : 0}đ`}</Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={3} xs={6} className={classes.infoItemTitle}>
                    <MonetizationOnIcon fontSize="small" /><Typography>{"Tiền Thanh toán"}</Typography>
                  </Grid>
                  <Grid item sm={9} xs={6}>
                    <Typography variant="subtitle1" className={classes.infoItemPrice}>{`${attrs.detail.payment_price.toLocaleString()}đ`}</Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={3} xs={6} className={classes.infoItemTitle}>
                    <MonetizationOnIcon fontSize="small" /><Typography>{"Đã Thanh Toán"}</Typography>
                  </Grid>
                  <Grid item sm={9} xs={6}>
                    <Typography variant="subtitle1"className={classes.infoItemPrice}>{`${attrs.detail.payment?.amount ? attrs.detail.payment.amount.toLocaleString() : 0}đ`}</Typography>
                  </Grid>
                </Grid>

                <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                  <Grid item sm={3} xs={6} className={classes.infoItemTitle}>
                    <Typography>{"Trạng Thái"}</Typography>
                  </Grid>
                  <Grid item sm={9} xs={6}>
                    <RadioGroup aria-label="status" name="status" value={attrs.detail.status} onChange={(e) => actions.handleUpdateStatus(e.target.value)} style={{flexDirection: "row"}}>
                      <FormControlLabel value={0} control={<Radio color="primary" />} label={this.renderStatus(0)} />
                      <FormControlLabel value={1} control={<Radio className={classes.warning} />} label={this.renderStatus(1)} />
                      <FormControlLabel value={2} control={<Radio className={classes.success} />} label={this.renderStatus(2)} />
                    </RadioGroup>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            <Grid item sm={12} xs={12}>
              <Typography variant="h6" className={classes.boxTitle}>Sản Phẩm</Typography>
              {attrs.photobooks && attrs.photobooks.length ?
                attrs.photobooks.map((photobook, index) => {
                  return (
                    <Paper className={classes.paper} key={index}>
                      <Box p={[1]} className={classes.boxWrap}>  
                          <FlipBook {...this.props} photobook={photobook} indexPhotobook={index} />
                          <div className={classes.btnGroup}>
                            <Box px={1}>
                              <Button
                                variant="outlined"
                                color="primary"
                                onClick={() => actions.openEditPhotobook(photobook._id)}
                                endIcon={<EditIcon fontSize="small" />}
                              >
                                Sửa thiết kế
                              </Button>
                            </Box>
                            <Box px={1}>
                              <Button
                                variant="contained"
                                color="primary"
                                onClick={() => actions.handleDownload(attrs.detail._id, photobook._id)}
                                endIcon={<GetAppIcon fontSize="small" />}
                              >
                                Tải Xuống
                              </Button>
                            </Box>
                          </div>

                          <Grid container spacing={1} justifyContent="center" className={classes.infoItem} style={{marginTop: 20}}>
                            <Grid item sm={2} xs={12} className={classes.infoItemTitle}>
                              <Typography>{"Loại Photobook"}</Typography>
                            </Grid>
                            <Grid item sm={4} xs={12}>
                              <Typography variant="subtitle2">{photobook.m_photobook_type.name}</Typography>
                            </Grid>
                          </Grid>
                          <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                            <Grid item sm={2} xs={12} className={classes.infoItemTitle}>
                              <Typography>{"Kích thước"}</Typography>
                            </Grid>
                            <Grid item sm={4} xs={12}>
                              <Typography variant="subtitle2"> {`${photobook.m_size.width}x${photobook.m_size.height} ${photobook.m_size.unit}`}</Typography>
                            </Grid>
                          </Grid>
                          <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                            <Grid item sm={2} xs={12} className={classes.infoItemTitle}>
                              <Typography>{"Loại Bìa"}</Typography>
                            </Grid>
                            <Grid item sm={4} xs={12}>
                              <Typography variant="subtitle2">{photobook.m_cover.name}</Typography>
                            </Grid>
                          </Grid>
                          <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                            <Grid item sm={2} xs={12} className={classes.infoItemTitle}>
                              <Typography>{"Giá"}</Typography>
                            </Grid>
                            <Grid item sm={4} xs={12}>
                              <Typography variant="subtitle2">{`${photobook.price.toLocaleString()}đ`}</Typography>
                            </Grid>
                          </Grid>
                          <Grid container spacing={1} justifyContent="center" className={classes.infoItem}>
                            <Grid item sm={2} xs={12} className={classes.infoItemTitle}>
                              <Typography>{"Số Lượng"}</Typography>
                            </Grid>
                            <Grid item sm={4} xs={12}>
                              <Typography variant="subtitle2">{`${photobook.quantity} cuốn`}</Typography>
                            </Grid>
                          </Grid>
                        </Box>
                    </Paper>
                  )
                })
                : null}
            
            </Grid>
          </Grid>
          {attrs.isOpenEditPhotobook && attrs.photobook_current ?
            <ResponsiveDialog open onClose={actions.closeEditPhotobook} scroll="paper" fullScreen>
              <DialogTitle style={{ padding: 0, textAlign: "right" }}>
                 <Button variant="contained" color="primary" style={{marginRight: 20}} onClick={() => designActions.handleSaveDesign(history)} >Lưu<SaveIcon fontSize="small" /></Button>
                <IconButton onClick={actions.closeEditPhotobook} ><CloseIcon /></IconButton>
              </DialogTitle>
              <div>
                <Design photobook_current={attrs.photobook_current}/>
              </div>
            </ResponsiveDialog>
          : null
          }
        </div>
      );
    }
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.admin.orderDetail, 
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
    designActions: bindActionCreators(designActions, dispatch)
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(OrderDetail);
