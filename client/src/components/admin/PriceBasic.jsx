import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import { AppBar, Tabs, Tab, Typography, Box, Paper, TextField, Button } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';

import { Actions } from "../../actions/admin/priceBasic";

import ProgressScreen from "../modules/ProgressScreen";

 const TabPanel = (props) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `scrollable-tab-${index}`,
  };
}

const styles = (theme) => ({
  root: {
    maxWidth: 650
  },
  itemPagePrice: { 
    display: "flex", 
    alignItems: "baseline", 
    justifyContent: "space-around" ,
    padding: "10px 0",
    margin: "10px 0",
    borderTop: "1px solid #ccc",
    "&:first-child": {
      borderTop: "none",
      marginTop: 0
    }
  },
  actions: {
    marginTop: 20,
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  }
});

class PriceBasic extends React.Component {

  componentDidMount() {
    this.props.actions.fetchDatas();
  }

  render() {
    const { classes, actions, attrs, history } = this.props;
    if (!attrs.listPagePrice?.length && !attrs.listCover?.length) {
      return <ProgressScreen />
    } else {
      return (
        <Paper className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={attrs.indexTab}
              onChange={(e, indexTab) => actions.handleChangeTab(indexTab)}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
            >
              {attrs.listSize.map((size, index) => {
                return <Tab label={`${size.width}x${size.height} ${size.unit}`} {...a11yProps(index)} />
              })}
            </Tabs>
          </AppBar>
          {attrs.listSize.map((size, index) => {
            return (
              <TabPanel value={attrs.indexTab} index={index}>
                <form onSubmit={(e) => { e.preventDefault(); actions.handleSave(); }}>
                  {attrs.listPagePrice.map((pagePrice, index) => {
                      return (
                        <div key={index} className={classes.itemPagePrice}>
                          <Typography variant="h6">Số trang {pagePrice.page_number}</Typography>
                          <TextField label="Giá"
                            inputProps={{ style: { maxWidth: 90, textAlign: "right" } }}
                            type="number"
                            required={true}
                            error={pagePrice.price ? false: true}
                            helperText={pagePrice.price ? "" : "Giá là bắt buộc"}
                            name={pagePrice.page_number}
                            defaultValue={pagePrice.price}
                            onBlur={(e) => actions.handleInputChanged(e.target.name, e.target.value)} />
                        </div>
                      )
                    })}
                  
                  <div className={classes.actions}>
                    <Button
                      type="submit"
                      variant="contained" color="primary" endIcon={<SaveIcon />} >Lưu</Button>
                  </div>
                </form>
              </TabPanel>
            )
          })}
          
        </Paper>
      );
    }
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.admin.priceBasic,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(PriceBasic);
