import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import moment from "moment";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import ResponsiveDialog from "../modules/ResponsiveDialog";
import {
  DialogTitle,
  DialogContent,
  TextField,
  FormControl,
  MenuItem,
  Select,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import { Actions } from "../../actions/admin/promotion";

const styles = (theme) => ({
  root: {
    padding: theme.spacing(1),
  },
  toolBarList: {
    justifyContent: "flex-end",
  },
  table: {
    minWidth: 750,
  },
  headModal: {
    display: "flex",
    justifyContent: "space-between",
    jutifyItem: "center",
    borderBottom: "1px solid #aaaaff",
  },

  titleModal: {
    display: "inline-block",
    margin: "auto",
    fontWeight: "bold",
    color: "#0da0c5",
  },
  btn: {
    display: "inline-block !important",
    marginRight: "10px",
    "&:hover": {
      backgroundColor: "#0da0c5",
      color: "white",
    },
    transition: "all 0.2s ease-in-out 0s",
  },
  menuAdd: {
    display: "flex",
    justifyContent: "center",
    padding: "10px",
  },
  imgContain: {
    display: "flex",
    justifyContent: "center",
  },
  imgPreview: {
    width: "100%",
    maxWidth: "400px",
    maxHeight: "250px",
    height: "auto",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  textInput: {
    width: "100%",
    marginBottom: "20px",
  },
  textError: {
    color: "#dc3545",
    marginLeft: "14px",
    fontSize: "0.75rem",
  },
  discount: {
    display: "flex",
    flexDirection: "row",
  },
  discountType: {
    width: "120px",
  },
  halfWidth: {
    width: "50%",
  },
});

class Promotion extends React.Component {
  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    this.props.actions.fetchDatas();
  }

  handleChangePage(e, newPage) {
    this.props.actions.setState({ page: newPage });
  }

  showDiscount(promotion) {
    let type = promotion.type == 1 ? "%" : "đ";
    return parseInt(promotion.value).toLocaleString() + type;
  }

  parseLocaleString(string) {
    if (string) {
      return parseInt(string).toLocaleString();
    }
  }

  formatDateTime(stringDT) {
    const template = "DD/MM/YYYY HH:mm";
    const strFormat = stringDT ? moment(stringDT).format(template) : stringDT;
    return strFormat;
  }

  render() {
    const { classes, actions, attrs, history } = this.props;
    const rowsPerPage = 10;
    const validationSchema = Yup.object().shape(
      {
        code: Yup.string().required("Vui lòng nhập mã code!"),
        name: Yup.string().required("Vui lòng nhập tiêu đề!"),
        descriptions: Yup.string(),
        type: Yup.number()
          .required("Vui lòng chọn đơn vị khuyến mãi!")
          .nullable(),
        value: Yup.number()
          .required("Vui lòng nhập giá trị khuyến mãi!")
          .min(0, "Giá trị tối thiểu là 0!")
          .nullable()
          .when("type", (type, schema) =>
            type == 1 ? schema.max(100, "Giá trị tối đa không hợp lệ!") : schema
          ),
        min_price_order: Yup.number().nullable(),
        time_start: Yup.date()
          .nullable()
          .transform((curr, orig) => (orig === "" ? null : curr))
          .when(
            "time_end",
            (time_end, schema) =>
              time_end &&
              schema
                .max(
                  time_end,
                  "Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc!"
                )
                .required("Vui lòng chọn thời gian bắt đầu khuyến mãi!")
          ),
        time_end: Yup.date()
          .nullable()
          .transform((curr, orig) => (orig === "" ? null : curr))
          .when(
            "time_start",
            (time_start, schema) =>
              time_start &&
              schema
                .min(
                  time_start,
                  "Thời gian kết thúc phải lớn hơn thời gian bắt đầu!"
                )
                .required("Vui lòng chọn thời gian kết thúc khuyến mãi!")
          ),
      },
      [["time_start", "time_end"]]
    );

    const headCells = [
      { id: "code", label: "Mã giảm giá" },
      { id: "name", label: "Tiêu đề" },
      { id: "descriptions", label: "Mô tả" },
      { id: "value", label: "Giá trị khuyến mãi" },
      { id: "min_price_order", label: "Giá sản phẩm tối thiểu" },
      { id: "time_start", label: "Ngày bắt đầu" },
      { id: "time_end", label: "Ngày kết thúc" },
    ];

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Toolbar className={classes.toolBarList}>
            <Tooltip title="Thêm Mới">
              <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
                onClick={actions.openModalCreate}
              >
                Thêm
              </Button>
            </Tooltip>
          </Toolbar>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
              aria-label="enhanced table"
            >
              <TableHead>
                <TableRow>
                  <TableCell padding="checkbox" align="center">
                    <Tooltip title="Xóa Đã Chọn">
                      <IconButton
                        className={classes.danger}
                        aria-label="delete-all"
                        onClick={(e) => actions.openModalConfirmMultiple()}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={"left"}
                      style={{ fontWeight: "bold" }}
                    >
                      {headCell.label}
                    </TableCell>
                  ))}
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {attrs.listPromotion
                  .slice(
                    attrs.page * rowsPerPage,
                    attrs.page * rowsPerPage + rowsPerPage
                  )
                  .map((row, index) => {
                    const labelId = `table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                      >
                        <TableCell align="center" padding="checkbox">
                          <Checkbox
                            inputProps={{ "aria-labelledby": labelId }}
                            checked={attrs.listItemChecked.includes(row._id)}
                            onClick={(e) =>
                              actions.handleItemCheck(
                                row._id,
                                attrs.listItemChecked
                              )
                            }
                          />
                        </TableCell>
                        <TableCell>{row.code}</TableCell>
                        <TableCell>{row.name}</TableCell>
                        <TableCell>{row.descriptions}</TableCell>
                        <TableCell>{this.showDiscount(row)}</TableCell>
                        <TableCell>
                          {this.parseLocaleString(row.min_price_order)}
                        </TableCell>
                        <TableCell>
                          {this.formatDateTime(row.time_start)}
                        </TableCell>
                        <TableCell>
                          {this.formatDateTime(row.time_end)}
                        </TableCell>
                        <TableCell align="right">
                          <Tooltip title="Chỉnh Sửa">
                            <IconButton
                              color="primary"
                              aria-label="edit"
                              onClick={(e) => actions.openModalEdit(row)}
                            >
                              <EditIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Xóa">
                            <IconButton
                              className={classes.danger}
                              aria-label="delete"
                              onClick={(e) => actions.openModalConfirm(row)}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={attrs.listPromotion.length}
            rowsPerPage={rowsPerPage}
            page={attrs.page}
            onPageChange={(e, newPage) => this.handleChangePage(e, newPage)}
          />
        </Paper>

        {/* Modal create or edit */}
        {(attrs.isOpenModalCreate ||
          (attrs.isOpenModalEdit && attrs.promotionSelected._id)) && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>
                  {attrs.isOpenModalCreate ? "Thêm" : "Chỉnh sửa"} mã khuyến mã
                </div>
                <IconButton onClick={(e) => actions.closeModalCreateOrEdit()}>
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <Formik
                enableReinitialize={true}
                initialValues={{
                  code: attrs.promotionSelected?.code,
                  name: attrs.promotionSelected?.name,
                  descriptions: attrs.promotionSelected?.descriptions,
                  type: attrs.promotionSelected.type ?? 1,
                  value: attrs.promotionSelected?.value,
                  min_price_order: attrs.promotionSelected?.min_price_order,
                  time_start: attrs.promotionSelected?.time_start,
                  time_end: attrs.promotionSelected?.time_end,
                  _id: attrs.promotionSelected?._id,
                }}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                  return actions.handleProcessSubmitData(
                    attrs.isOpenModalCreate,
                    values
                  );
                }}
              >
                {({ errors, touched, setFieldValue }) => (
                  <Form className={classes.form}>
                    <TextField
                      id="code"
                      name="code"
                      label="Mã giảm giá*"
                      type="text"
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.promotionSelected.code ?? ""}
                      onChange={(e) => {
                        setFieldValue("code", e.target.value);
                        actions.handleInputChange(e, attrs.promotionSelected);
                      }}
                      error={Boolean(errors.code && touched.code)}
                      helperText={
                        errors.code && touched.code && String(errors.code)
                      }
                    />
                    <TextField
                      id="name"
                      name="name"
                      label="Tiêu đề*"
                      type="text"
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.promotionSelected.name ?? ""}
                      onChange={(e) => {
                        setFieldValue("name", e.target.value);
                        actions.handleInputChange(e, attrs.promotionSelected);
                      }}
                      error={Boolean(errors.name && touched.name)}
                      helperText={
                        errors.name && touched.name && String(errors.name)
                      }
                    />
                    <TextField
                      name="descriptions"
                      label="Mô tả"
                      type="text"
                      multiline
                      minRows={2}
                      maxRows={4}
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.promotionSelected.descriptions ?? ""}
                      onChange={(e) => {
                        setFieldValue("descriptions", e.target.value);
                        actions.handleInputChange(e, attrs.promotionSelected);
                      }}
                      error={Boolean(
                        errors.descriptions && touched.descriptions
                      )}
                      helperText={
                        errors.descriptions &&
                        touched.descriptions &&
                        String(errors.descriptions)
                      }
                    />

                    <div className={classes.discount}>
                      <TextField
                        id="value"
                        name="value"
                        label="Giá trị khuyến mãi*"
                        type="number"
                        variant="outlined"
                        className={classes.textInput}
                        value={attrs.promotionSelected.value ?? ""}
                        onChange={(e) => {
                          setFieldValue("value", e.target.value);
                          actions.handleInputChange(e, attrs.promotionSelected);
                        }}
                        error={Boolean(errors.value && touched.value)}
                        helperText={
                          errors.value && touched.value && String(errors.value)
                        }
                      />
                      <FormControl
                        fullWidth
                        variant="outlined"
                        className={classes.discountType}
                      >
                        <Select
                          name="type"
                          labelId="total-label"
                          value={attrs.promotionSelected.type ?? 1}
                          onChange={(e) => {
                            setFieldValue("type", e.target.value);
                            actions.handleInputChange(
                              e,
                              attrs.promotionSelected
                            );
                          }}
                        >
                          <MenuItem key={1} value={1}>
                            %
                          </MenuItem>
                          <MenuItem key={2} value={2}>
                            Đồng
                          </MenuItem>
                        </Select>
                      </FormControl>
                    </div>

                    <TextField
                      id="min_price_order"
                      name="min_price_order"
                      label="Giá trị sản phẩm tối thiểu"
                      type="number"
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.promotionSelected.min_price_order ?? ""}
                      onChange={(e) => {
                        setFieldValue("min_price_order", e.target.value);
                        actions.handleInputChange(e, attrs.promotionSelected);
                      }}
                      error={Boolean(
                        errors.min_price_order && touched.min_price_order
                      )}
                      helperText={
                        errors.min_price_order &&
                        touched.min_price_order &&
                        String(errors.min_price_order)
                      }
                    />

                    <MuiPickersUtilsProvider
                      utils={DateFnsUtils}
                      className={classes.dateTimePicker}
                    >
                      <DateTimePicker
                        className={classes.halfWidth}
                        name="time_start"
                        autoOk
                        format="dd/MM/yyyy hh:mm"
                        ampm={false}
                        inputVariant="outlined"
                        value={attrs.promotionSelected.time_start ?? null}
                        onChange={(value) => {
                          setFieldValue("time_start", value);
                          actions.handleDateTimePickerChange(
                            "time_start",
                            value,
                            attrs.promotionSelected
                          );
                        }}
                        error={Boolean(errors.time_start && touched.time_start)}
                        helperText={
                          errors.time_start &&
                          touched.time_start &&
                          String(errors.time_start)
                        }
                        label="Thời gian bắt đầu"
                      />
                      <DateTimePicker
                        className={classes.halfWidth}
                        name="time_end"
                        autoOk
                        format="dd/MM/yyyy hh:mm"
                        ampm={false}
                        inputVariant="outlined"
                        value={attrs.promotionSelected.time_end ?? null}
                        onChange={(value) => {
                          setFieldValue("time_end", value);
                          actions.handleDateTimePickerChange(
                            "time_end",
                            value,
                            attrs.promotionSelected
                          );
                        }}
                        error={Boolean(errors.time_end && touched.time_end)}
                        helperText={
                          errors.time_end &&
                          touched.time_end &&
                          String(errors.time_end)
                        }
                        label="Thời gian kết thúc"
                      />
                    </MuiPickersUtilsProvider>

                    <div className={classes.menuAdd}>
                      <Button
                        variant="outlined"
                        color="primary"
                        onClick={(e) =>
                          actions.closeModalCreateOrEdit(attrs?.objectUrl)
                        }
                        size="large"
                        className={classes.btn}
                        type="button"
                      >
                        Hủy
                      </Button>
                      <Button
                        variant="outlined"
                        color="secondary"
                        size="large"
                        className={classes.btn}
                        type="submit"
                      >
                        {attrs.isOpenModalCreate ? "Tạo mới" : "Cập nhật"}
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </DialogContent>
          </ResponsiveDialog>
        )}

        {attrs.isOpenModalConfirmMultiple && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>Xóa Mã Khuyến Mãi</div>
                <IconButton onClick={actions.closeModalConfirm}>
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <p
                style={{ margin: 0 }}
              >{`Bạn muốn xóa ${attrs.listItemChecked.length} mã khuyến mãi`}</p>
            </DialogContent>
            <div className={classes.menuAdd}>
              <Button
                variant="outlined"
                color="primary"
                onClick={actions.closeModalConfirm}
                size="large"
                className={classes.btn}
              >
                Hủy
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                onClick={(e) =>
                  actions.handleDeleteMultiple(attrs.listItemChecked)
                }
                size="large"
                className={classes.btn}
              >
                Xoá
              </Button>
            </div>
          </ResponsiveDialog>
        )}

        {/* Modal confirm delete */}
        {attrs.isOpenModalConfirm && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>Xóa Mã Khuyến Mãi</div>
                <IconButton onClick={actions.closeModalConfirm}>
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <p
                style={{ margin: 0 }}
              >{`Bạn muốn xóa mã khuyến mãi ${attrs.promotionSelected.name} này?`}</p>
            </DialogContent>
            <div className={classes.menuAdd}>
              <Button
                variant="outlined"
                color="primary"
                onClick={actions.closeModalConfirm}
                size="large"
                className={classes.btn}
              >
                Hủy
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                onClick={(e) =>
                  actions.handleDelete(attrs.promotionSelected._id)
                }
                size="large"
                className={classes.btn}
              >
                Xoá
              </Button>
            </div>
          </ResponsiveDialog>
        )}
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.admin.promotion,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Promotion);
