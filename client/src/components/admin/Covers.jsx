import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import { Actions } from "../../actions/admin/covers";

import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from "@material-ui/icons/Close";
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Chip from '@material-ui/core/Chip';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import ResponsiveDialog from '../modules/ResponsiveDialog';
import {
	DialogTitle,
	DialogContent,
} from '@material-ui/core';

const styles = (theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	inputForm: {
		width: "100%",
		marginTop: "10px",
		marginBottom: "10px",
	},
	toolBarList: {
		justifyContent: "flex-end"
	},
	table: {
		minWidth: 750,
	},
	headModal: {
		display: "flex",
		justifyContent: "space-between",
		jutifyItem: "center",
		borderBottom: "1px solid #aaaaff"
	},
	titleModal: {
		display: "inline-block",
		margin: "auto",
		fontWeight: "bold",
		color: "#0da0c5",
	},
	menuAdd: {
		display: "flex",
		justifyContent: "center",
		padding: "10px",
	},

	btn: {
		display: "inline-block !important",
		marginRight: "10px",
		"&:hover": {
			backgroundColor: "#0da0c5",
			color: "white",
		},
		transition: "all 0.2s ease-in-out 0s",
	},
	imageCover: {
		width: "60%",
		height: "60%",
		margin: "auto",
		padding: "10px",
		border: "1px solid #8888aa",
		borderRadius: "6px",
	},

	imagePreview: {
		width: "100%",
		height: "100%",
		objectFit: "cover",
	},
	bgSuccess: {
		backgroundColor: theme.palette.primary.main,
		color: "white"
	},

	bgSecondary: {
		backgroundColor: "#888888",
		color: "white"
	},

	bgWhiteBlue: {
		color: "#eeeeff",
	}
});

class Covers extends React.Component {
	componentWillUnmount() {
		this.props.actions.initState();
	}

	componentDidMount() {
		this.props.actions.fetchDatas();
	}

	handleChangePage(e, newPage) {
		this.props.actions.setState({ page: newPage });
	}

	render() {
		const { classes, actions, attrs, history } = this.props;

		// rowsPerPage = 10
		const rowsPerPage = 10;

		let rows = [];

		//Data demo
		const createData = (image, name, typeFrame, status, id, descriptions) => {
			return { image, name, typeFrame, status, id, descriptions };
		}

		const getFrameType = (value) => {
			for (let i = 0; i < attrs.typeList.length; i++) {
				if (attrs.typeList[i].id == value) {
					return attrs.typeList[i].title;
				}
			}
		}

		if (attrs.coverList) {
			const coverList = attrs.coverList;
			for (let i = 0; i < coverList.length; i++) {
				let item = coverList[i];
				rows.push(
					createData(item.image, item.name, getFrameType(item.typeFrame), item.status, item._id, item.descriptions)
				)
			}
		}

		const headCells = [
			{ id: 'image', label: 'Hình ảnh' },
			{ id: 'name', label: 'Tên ảnh bìa' },
			{ id: 'frameType', label: 'Loại khung' },
			{ id: 'status', label: 'Trạng thái' },
			{ id: 'descriptions', label: 'Mô tả' },
		];
		return (
			<div className={classes.root}>
				<Paper className={classes.paper}>
					<Toolbar className={classes.toolBarList}>
						<Tooltip title="Thêm Mới">
							<Button variant="contained" color="primary" startIcon={<AddIcon />} onClick={actions.handleClickButton}>Thêm</Button>
						</Tooltip>
					</Toolbar>
					<TableContainer>
						<Table
							className={classes.table}
							aria-labelledby="tableTitle"
							size={'medium'}
							aria-label="enhanced table"
						>
							<TableHead>
								<TableRow>
									{headCells.map((headCell) => (
										<TableCell
											key={headCell.id}
											align={'left'}
											style={{ fontWeight: 'bold' }}
										>
											{headCell.label}
										</TableCell>
									))}
									<TableCell></TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{rows
									.slice(attrs.page * rowsPerPage, attrs.page * rowsPerPage + rowsPerPage)
									.map((row, index) => {
										const labelId = `table-checkbox-${index}`;

										return (
											<TableRow
												hover
												role="checkbox"
												tabIndex={-1}
												key={row.name}
											>
												<TableCell >
													<img height={50} src={row.image} />
												</TableCell>
												<TableCell id={labelId}>
													{row.name}
												</TableCell>
												<TableCell >
													{row.typeFrame}
												</TableCell>
												<TableCell >
													{(row.status == 0) && <Chip
														label="Ẩn hiển thị"
														className={classes.bgSecondary}
														icon={<VisibilityOffIcon className={classes.bgWhiteBlue} />}
													/>}
													{(row.status == 1) && <Chip
														label="Hiển thị"
														className={classes.bgSuccess}
														icon={<VisibilityIcon className={classes.bgWhiteBlue} />}
													/>}
												</TableCell>
												<TableCell >
													{row.descriptions}
												</TableCell>
												<TableCell align="right">
													<Tooltip title="Chỉnh Sửa">
														<IconButton color="primary" aria-label="edit" onClick={() => actions.handleClickEditButton(row)}>
															<EditIcon />
														</IconButton>
													</Tooltip>
													<Tooltip title="Xóa">
														<IconButton aria-label="delete" onClick={() => actions.handleDeleteButton(row)}>
															<DeleteIcon />
														</IconButton>
													</Tooltip>
												</TableCell>
											</TableRow>
										);
									})}
							</TableBody>
						</Table>
					</TableContainer>
					<TablePagination
						rowsPerPageOptions={[]}
						component="div"
						count={rows.length}
						rowsPerPage={rowsPerPage}
						page={attrs.page}
						onPageChange={(e, newPage) => this.handleChangePage(e, newPage)}
					/>
				</Paper>
				{
					attrs.isOpenFormAddCover &&
					<ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
						<DialogTitle>
							<div className={classes.headModal}>
								<div className={classes.titleModal}>{attrs.layout.formTitle}</div>
								<IconButton onClick={actions.closeFormModal}><CloseIcon /></IconButton>
							</div>
						</DialogTitle>
						<DialogContent style={{ padding: "10px 20px 20px 20px" }}>
							<form noValidate autoComplete="off" id="cover-add-form">
								<TextField
									error={attrs.error.name}
									helperText={attrs.error.name ? "Tên ảnh bìa không được để trống" : ""}
									id="name"
									label="Tên ảnh bìa"
									variant="outlined"
									className={classes.inputForm}
									name="name"
									value={attrs.name}
									onChange={actions.setValueName}
								/>

								<FormControl variant="outlined" className={classes.inputForm}>
									<InputLabel id="frame-type-label">Loại khung hình</InputLabel>
									<Select
										error={attrs.error.typeFrame}
										helperText={attrs.error.typeFrame ? "Loại khung hình không được để trống" : ""}
										labelId="frame-type-label"
										id="frame-type"
										label="Loại khung hình"
										name="typeFrame"
										value={attrs.typeFrame ? attrs.typeFrame : 0}
										onChange={actions.setValueFrame}
									>
										{attrs.typeList.map(item => (
											<MenuItem value={item.id} key={item.id}>{item.title}</MenuItem>
										))}
									</Select>
								</FormControl>
								<FormLabel component="legend">Giá bìa</FormLabel>
								<Grid container spacing={2}>
									{attrs.cover_price && attrs.cover_price.map((cover_price, index) => (
										<Grid item xs={3} key={index}>
											<TextField
												error={cover_price.price ? false : true}
												required
												type="number"
												label={cover_price.m_size.width + 'x' + cover_price.m_size.height + cover_price.m_size.unit}
												variant="outlined"
												className={classes.inputForm}
												value={cover_price.price}
												onChange={(e) => actions.setValuePrice(e, cover_price.m_size)}
											/>
										</Grid>
									))}
								</Grid>
								<FormControl component="fieldset">
									<FormLabel component="legend">Trạng thái</FormLabel>
									<RadioGroup row aria-label="status" name="status" value={attrs.status} onChange={actions.setValueStatus}>
										<FormControlLabel value={0} control={<Radio />} label="Ẩn hiển thị" />
										<FormControlLabel value={1} control={<Radio />} label="Hiển thị" />
									</RadioGroup>
								</FormControl>
								<FormControl variant="outlined" className={classes.inputForm}>
									<TextField
										id="descriptions"
										label="Mô tả"
										multiline
										rows={4}
										variant="outlined"
										name="descriptions"
										value={attrs.descriptions}
										onChange={actions.setValueDescriptions}
									/>
								</FormControl>
								<input type="file" accept=".png, .jpg, .jpeg" className={classes.inputForm} name="image" id="image" onChange={actions.onChangeImage}></input>
								{
									attrs.isExistsImage &&
									<div className={classes.imageCover}>
										<img src={attrs.src} alt="image load fails" className={classes.imagePreview} id="image-preview" />
									</div>
								}

								<div className={classes.menuAdd}>
									<Button variant="outlined" color="primary" onClick={actions.closeFormModal} size="large" className={classes.btn} >
										Hủy
									</Button>
									<Button variant="outlined" color="secondary" onClick={() => actions.handleSumitForm("cover-add-form", attrs)} size="large" className={classes.btn}>
										{attrs.layout.netStepName}
									</Button>
								</div>
							</form>
						</DialogContent>
					</ResponsiveDialog>
				}
				{
					attrs.isOpenModalConfirm &&
					<ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
						<DialogTitle>
							<div className={classes.headModal}>
								<div className={classes.titleModal}>Xóa ảnh bìa</div>
								<IconButton onClick={actions.closeModalConfirm}><CloseIcon /></IconButton>
							</div>
						</DialogTitle>
						<DialogContent style={{ padding: "0 20px" }}>
							<p style={{ margin: 0 }}>Bạn muốn xóa ảnh này?</p>
						</DialogContent>
						<div className={classes.menuAdd}>
							<Button variant="outlined" color="primary" onClick={actions.closeModalConfirm} size="large" className={classes.btn} >
								Hủy
							</Button>
							<Button variant="outlined" color="secondary" onClick={(e) => actions.deleteCover(attrs.coverId)} size="large" className={classes.btn}>
								Ok
							</Button>
						</div>
					</ResponsiveDialog>
				}
			</div>
		);
	}
}

function mapStateToProps(state, props) {
	return {
		attrs: state.admin.covers,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(Actions, dispatch),
	};
}

export default compose(
	withStyles(styles, { withTheme: true }),
	connect(mapStateToProps, mapDispatchToProps)
)(Covers);
