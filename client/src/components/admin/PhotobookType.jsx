import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import { Field, Form, Formik, ErrorMessage } from "formik";
import * as Yup from "yup";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import ImageIcon from "@material-ui/icons/Image";
import ResponsiveDialog from "../modules/ResponsiveDialog";
import { DialogTitle, DialogContent, TextField } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import { Actions } from "../../actions/admin/photobookType";

const styles = (theme) => ({
  root: {
    padding: theme.spacing(1),
  },
  toolBarList: {
    justifyContent: "flex-end",
  },
  table: {
    minWidth: 750,
  },
  headModal: {
    display: "flex",
    justifyContent: "space-between",
    jutifyItem: "center",
    borderBottom: "1px solid #aaaaff",
  },

  titleModal: {
    display: "inline-block",
    margin: "auto",
    fontWeight: "bold",
    color: "#0da0c5",
  },
  btn: {
    display: "inline-block !important",
    marginRight: "10px",
    "&:hover": {
      backgroundColor: "#0da0c5",
      color: "white",
    },
    transition: "all 0.2s ease-in-out 0s",
  },
  menuAdd: {
    display: "flex",
    justifyContent: "center",
    padding: "10px",
  },
  imgContain: {
    display: "flex",
    justifyContent: "center",
  },
  imgPreview: {
    width: "100%",
    maxWidth: "400px",
    maxHeight: "250px",
    height: "auto",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  textInput: {
    width: "100%",
    marginBottom: "20px",
  },
  textError: {
    color: "#dc3545",
    marginLeft: "14px",
    fontSize: "0.75rem",
  },
});

class PhotobookType extends React.Component {
  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    this.props.actions.fetchDatas();
  }

  handleChangePage(e, newPage) {
    this.props.actions.setState({ page: newPage });
  }

  render() {
    const { classes, actions, attrs, history } = this.props;
    const rowsPerPage = 10;
    const editValidationSchema = Yup.object().shape({
      name: Yup.string().required("Vui lòng nhập tiêu đề!"),
      descriptions: Yup.string(),
      image: Yup.mixed().test(
        "fileFormat",
        "Vui lòng chọn đúng định dạng ảnh!",
        (value) => {
          if (!value) return true;
          return value &&
            ["image/jpg", "image/jpeg", "image/gif", "image/png"].includes(
              value.type
            );
        }
      ),
    });

    const baseValidationSchema = editValidationSchema.shape({
      image: Yup.mixed().test(
        "fileRequired",
        "Vui lòng upload ảnh!",
        (value) => value
      ),
    });

    const headCells = [
      { id: "image", label: "Hình ảnh" },
      { id: "name", label: "Tiêu đề" },
      { id: "description", label: "Mô tả" },
    ];

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Toolbar className={classes.toolBarList}>
            {/* <Tooltip title="Thêm Mới">
              <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
                onClick={actions.openModalCreate}
              >
                Thêm
              </Button>
            </Tooltip> */}
          </Toolbar>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
              aria-label="enhanced table"
            >
              <TableHead>
                <TableRow>
                  {/* <TableCell padding="checkbox" align="center">
                    <Tooltip title="Xóa Đã Chọn">
                      <IconButton
                        className={classes.danger}
                        aria-label="delete-all"
                        onClick={(e) => actions.openModalConfirmMultiple()}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </TableCell> */}
                  {headCells.map((headCell) => (
                    <TableCell
                      key={headCell.id}
                      align={"left"}
                      style={{ fontWeight: "bold" }}
                    >
                      {headCell.label}
                    </TableCell>
                  ))}
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {attrs.listPhotobookType
                  .slice(
                    attrs.page * rowsPerPage,
                    attrs.page * rowsPerPage + rowsPerPage
                  )
                  .map((row, index) => {
                    const labelId = `table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                      >
                        {/* <TableCell align="center" padding="checkbox">
                          <Checkbox
                            inputProps={{ "aria-labelledby": labelId }}
                            checked={attrs.listItemChecked.includes(row._id)}
                            onClick={(e) =>
                              actions.handleItemCheck(
                                row._id,
                                attrs.listItemChecked
                              )
                            }
                          />
                        </TableCell> */}
                        <TableCell>
                          <img height={50} src={row.image} />
                        </TableCell>
                        <TableCell id={labelId}>{row.name}</TableCell>
                        <TableCell>{row.descriptions}</TableCell>
                        <TableCell align="right">
                          <Tooltip title="Chỉnh Sửa">
                            <IconButton
                              color="primary"
                              aria-label="edit"
                              onClick={(e) => actions.openModalEdit(row)}
                            >
                              <EditIcon />
                            </IconButton>
                          </Tooltip>
                          {/* <Tooltip title="Xóa">
                            <IconButton
                              className={classes.danger}
                              aria-label="delete"
                              onClick={(e) => actions.openModalConfirm(row)}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip> */}
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={attrs.listPhotobookType.length}
            rowsPerPage={rowsPerPage}
            page={attrs.page}
            onPageChange={(e, newPage) => this.handleChangePage(e, newPage)}
          />
        </Paper>

        {/* Modal create or edit */}
        {(attrs.isOpenModalCreate ||
          (attrs.isOpenModalEdit && attrs.photobookTypeSelected._id)) && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>
                  {attrs.isOpenModalCreate ? "Thêm" : "Chỉnh sửa"} Loại
                  Photobook
                </div>
                <IconButton
                  onClick={(e) =>
                    actions.closeModalCreateOrEdit(attrs?.objectUrl)
                  }
                >
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <Formik
                enableReinitialize={true}
                initialValues={
                  attrs.isOpenModalCreate
                    ? {
                        name: "",
                        descriptions: "",
                        image: null,
                      }
                    : {
                        name: attrs.photobookTypeSelected.name,
                        descriptions: attrs.photobookTypeSelected.descriptions,
                        image: null,
                        _id: attrs.photobookTypeSelected._id,
                      }
                }
                validationSchema={
                  attrs.isOpenModalCreate
                    ? baseValidationSchema
                    : editValidationSchema
                }
                onSubmit={(values) => {
                  return actions.handleProcessSubmitData(
                    attrs.isOpenModalCreate,
                    values
                  );
                }}
              >
                {({ errors, touched, setFieldValue }) => (
                  <Form className={classes.form}>
                    <TextField
                      id="name"
                      name="name"
                      label="Tiêu đề*"
                      type="text"
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.photobookTypeSelected.name ?? ""}
                      onChange={(e) => {
                        setFieldValue("name", e.target.value);
                        actions.handleInputChange(
                          e,
                          attrs.photobookTypeSelected
                        );
                      }}
                      error={Boolean(errors.name && touched.name)}
                      helperText={
                        errors.name && touched.name && String(errors.name)
                      }
                    />
                    <TextField
                      name="descriptions"
                      label="Mô tả"
                      type="text"
                      multiline
                      minRows={2}
                      maxRows={4}
                      variant="outlined"
                      className={classes.textInput}
                      value={attrs.photobookTypeSelected.descriptions ?? ""}
                      onChange={(e) => {
                        setFieldValue("descriptions", e.target.value);
                        actions.handleInputChange(
                          e,
                          attrs.photobookTypeSelected
                        );
                      }}
                      error={Boolean(
                        errors.descriptions && touched.descriptions
                      )}
                      helperText={
                        errors.descriptions &&
                        touched.descriptions &&
                        String(errors.descriptions)
                      }
                    />

                    {Boolean(errors.image && touched.image) ? (
                      <p id="imageError" className={classes.textError}>
                        {errors.image}
                      </p>
                    ) : (
                      ""
                    )}

                    <Button
                      variant="contained"
                      component="label"
                      color="secondary"
                      style={{ marginBottom: "20px" }}
                    >
                      <AddIcon />
                      <ImageIcon />
                      <input
                        type="file"
                        name="image"
                        accept="image/*"
                        onChange={(e) => {
                          setFieldValue("image", e.target.files[0]);
                          actions.handleImageChange(e);
                        }}
                      />
                    </Button>

                    {attrs.objectUrl ? (
                      <div className={classes.imgContain}>
                        <img
                          src={attrs.objectUrl}
                          alt="image"
                          className={classes.imgPreview}
                        />
                      </div>
                    ) : attrs.photobookTypeSelected.image ? (
                      <div className={classes.imgContain}>
                        <img
                          src={attrs.photobookTypeSelected.image}
                          alt="image"
                          className={classes.imgPreview}
                        />
                      </div>
                    ) : (
                      ""
                    )}
                    <div className={classes.menuAdd}>
                      <Button
                        variant="outlined"
                        color="primary"
                        onClick={(e) =>
                          actions.closeModalCreateOrEdit(attrs?.objectUrl)
                        }
                        size="large"
                        className={classes.btn}
                        type="button"
                      >
                        Hủy
                      </Button>
                      <Button
                        variant="outlined"
                        color="secondary"
                        size="large"
                        className={classes.btn}
                        type="submit"
                      >
                        {attrs.isOpenModalCreate ? "Tạo mới" : "Cập nhật"}
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </DialogContent>
          </ResponsiveDialog>
        )}

        {attrs.isOpenModalConfirmMultiple && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>Xóa Loại Photobook</div>
                <IconButton onClick={actions.closeModalConfirm}>
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <p
                style={{ margin: 0 }}
              >{`Bạn muốn xóa ${attrs.listItemChecked.length} loại photobook?`}</p>
            </DialogContent>
            <div className={classes.menuAdd}>
              <Button
                variant="outlined"
                color="primary"
                onClick={actions.closeModalConfirm}
                size="large"
                className={classes.btn}
              >
                Hủy
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                onClick={(e) =>
                  actions.handleDeleteMultiple(attrs.listItemChecked)
                }
                size="large"
                className={classes.btn}
              >
                Xoá
              </Button>
            </div>
          </ResponsiveDialog>
        )}

        {/* Modal confirm delete */}
        {attrs.isOpenModalConfirm && (
          <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
            <DialogTitle>
              <div className={classes.headModal}>
                <div className={classes.titleModal}>Xóa Loại Photobook</div>
                <IconButton onClick={actions.closeModalConfirm}>
                  <CloseIcon />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent style={{ padding: "0 20px" }}>
              <p
                style={{ margin: 0 }}
              >{`Bạn muốn xóa ${attrs.photobookTypeSelected.name} này?`}</p>
              <div
                style={{
                  display: "flex",
                  padding: "20px",
                  justifyContent: "center",
                }}
              >
                <img
                  src={attrs.photobookTypeSelected.image}
                  alt="image"
                  className={classes.imgPreview}
                />
              </div>
            </DialogContent>
            <div className={classes.menuAdd}>
              <Button
                variant="outlined"
                color="primary"
                onClick={actions.closeModalConfirm}
                size="large"
                className={classes.btn}
              >
                Hủy
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                onClick={(e) =>
                  actions.handleDelete(attrs.photobookTypeSelected._id)
                }
                size="large"
                className={classes.btn}
              >
                Xoá
              </Button>
            </div>
          </ResponsiveDialog>
        )}
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.admin.photobookType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(PhotobookType);
