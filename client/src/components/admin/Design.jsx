import React from 'react';
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import ResponsiveComponent from '../common/ResponsiveComponent';
import DesignPC from './DesignPC';
import DesignSP from './DesignSP';

import { Actions } from "../../actions/admin/design";
import { Actions as appMenuAction } from "../../actions/common/appMenu.js";


const styles = theme => ({
});

class Design extends ResponsiveComponent {

  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    this.props.actions.fetchDatas(this.props.photobook_current);
    const fontText = document.getElementById("renderLayout")?.offsetWidth*16/1096;
    this.props.actions.setState({ fontText });
  }

  renderPC() {
    return <DesignPC {...this.props} />;
  }

  renderSP() {
    return <DesignSP {...this.props} />;
  }

};


function mapStateToProps(state, props) {
  return {
    attrs: state.admin.design
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
    appMenuAction: bindActionCreators(appMenuAction, dispatch)
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Design);
