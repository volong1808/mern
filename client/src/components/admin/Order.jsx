import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles, alpha } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import moment from "moment";

import { Paper, Toolbar, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TablePagination,
  Checkbox, IconButton, Tooltip, InputBase, Chip, Button, Typography } from '@material-ui/core';

import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import SearchIcon from '@material-ui/icons/Search';

import { Actions } from "../../actions/admin/order";

const styles = (theme) => ({
  root: {
    padding: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2)
  },
  toolBarList: {
    justifyContent: "flex-end"
  },
  search: {
    position: 'relative',
    borderRadius: 5,
    backgroundColor: alpha(theme.palette.primary.main, 0.1),
    '&:hover': {
      backgroundColor: alpha(theme.palette.primary.main, 0.15),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  table: {
    minWidth: 800,
  },
  colorPrimary: {
    color: "#fff",
    backgroundColor: theme.palette.primary.main
  },
  colorWarning: {
    color: "#fff",
    backgroundColor: theme.palette.warning.main
  },
  colorSuccess: {
    color: "#fff",
    backgroundColor: theme.palette.success.main
  }
});

class Order extends React.Component {

  renderStatus(status) {
    let label, colorClass;
    
    switch (status) {
      case 0:
        label = "Mới";
        colorClass = "colorPrimary";
        break;
      case 1:
        label = "Đã In";
        colorClass = "colorWarning";   
        break;
      case 2:
        label = "Đã Giao Hàng";
        colorClass = "colorSuccess";  
        break;
  
    }
    return <Chip size="small" label={label} className={this.props.classes[colorClass]} />
  }

  handleChangePage(e, newPage) {
    this.props.actions.setState({ page: newPage });
  }

  componentDidMount() {
    this.props.actions.fetchDatas();
  }

  render() {
    const { classes, actions, attrs, history } = this.props;

    // rowsPerPage = 10
    const rowsPerPage = 10;

    const headCells = [
      { id: 'code', label: 'Mã Đơn Hàng' },
      { id: 'customer.name', label: 'Khách Hàng' },
      { id: 'total_price', label: 'Tổng Tiền' },
      { id: 'status', label: 'Trạng Thái' },
      { id: 'ordered_at', label: 'Thời gian' },
    ];

    let listOrder = attrs.listOrderSearch;

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
            <Toolbar className={classes.toolBarList}>
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  onChange={(e) => actions.handleSearch(e.target.value)}
                  placeholder="Search…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  inputProps={{ 'aria-label': 'search' }}
                />
              </div>
            </Toolbar>
            {listOrder.length ? 
            <>
              <TableContainer>
                <Table
                  className={classes.table}
                  aria-labelledby="tableTitle"
                  size={'medium'}
                  aria-label="enhanced table"
                >
                  <TableHead>
                    <TableRow>
                      {headCells.map((headCell, index) => (
                        <TableCell
                          key={headCell.id}
                          align={'left'}
                          style={{ fontWeight: 'bold' }}
                        >
                          {headCell.label}
                        </TableCell>
                      ))}
                      <TableCell></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {listOrder
                      .slice(attrs.page * rowsPerPage, attrs.page * rowsPerPage + rowsPerPage)
                      .map((row, index) => {
                        const labelId = `table-checkbox-${index}`;

                        return (
                          <TableRow
                            hover
                            role="checkbox"
                            tabIndex={-1}
                            key={index}
                          >
                            <TableCell >
                              {row.code}
                            </TableCell>
                            <TableCell id={labelId}>
                              {row.customer?.full_name}
                            </TableCell>
                            <TableCell >
                              {`${row.total_price.toLocaleString()}đ`}
                            </TableCell>
                            <TableCell >
                              {this.renderStatus(row.status)}
                            </TableCell>
                            <TableCell >
                              <Typography variant="caption">{moment(row.ordered_at).format("H:mm:ss DD/MM/YYYY")}</Typography>
                            </TableCell>
                            <TableCell align="right">
                              <Tooltip title="Chi Tiết">
                                <IconButton color="primary" aria-label="view" onClick={() => history.push(`/admin/order/${row._id}`)}>
                                  <VisibilityIcon />
                                </IconButton>
                              </Tooltip>
                              <Tooltip title="Xóa">
                                <IconButton aria-label="delete" onClick={() => actions.handleDelete(row._id)}>
                                  <DeleteIcon />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                          </TableRow>
                        );
                      })
                    }
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[]}
                component="div"
                count={listOrder.length}
                rowsPerPage={rowsPerPage}
                page={attrs.page}
                onPageChange={(e, newPage) => this.handleChangePage(e, newPage)}
              />
            </>
            : <Typography variant="h6" align="center">Không có đơn hàng nào!</Typography>}
            
          </Paper>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.admin.order, 
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Order);
