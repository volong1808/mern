import React from 'react';
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import { IconButton, Typography } from '@material-ui/core';
//Icon
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import EditImage from '../modules/design/EditImage';
import EditText from '../modules/design/EditText';

import { Actions } from "../../actions/admin/design";

const styles = theme => ({
  root: {
    width: "100%",
    // height: "calc(100vh - 40px)",
    display: "flex"
  },
  flexWrap: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  imageDialogItem: {
    width: "100%",
    display: "flex",
    flexFlow: "row wrap",
    alignContent: "flex-start",
    "& img": {
      width: "22%",
      padding: "3px",
      margin: "3px",
      border: "2px solid #ffffff",
      aspectRatio: "1/1",
      objectFit: "cover",
      cursor: "pointer",
      "&:hover": {
        borderColor: "green"
      }
    },
  },
  designWrap: {
    width: "100%",
    height: "100%",
    padding: "5px",
    backgroundColor: "#f2f2f2",
    position: "relative"
  },
  layoutContainer: {
    backgroundColor: "#cccccc",
    padding: "12px 0",
    display: "flex",
    flexWrap: "nowrap",
    overflowX: "auto"
  },
  layoutItem: {
    display: "inline-block",
    cursor: "pointer",
    padding: "3px",
    margin: "0 15px",
    "& img": {
      verticalAlign: "middle",
      height: "45px",
      background: "#ffffff",
      boxShadow: "0 2px 5px 0 rgb(0 0 0 / 30%)"
    }
  },
  layoutItemActived: {
    backgroundColor: "rgb(18, 160, 197)"
  },
  frameContainer: {
    // height: "calc(100% - 300px)",
    // overflow: "auto",
    textAlign: "center",
    width: "100%"
  },
  frameWrap: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  iconChangePage: {
    margin: "10px",
    padding: 0,
    backgroundColor: "#ffffff",
    color: "#000000",
    borderRadius: "50%",
    cursor: "pointer",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 30%)",
    "&:hover": {
      backgroundColor: "#ffffff",
    }
  },
  frame: {
    position: "relative",
    width: "100%",
    maxWidth: "350px",
    aspectRatio: "2/1",
    background: "#ffffff",
    display: "inline-block",
    boxShadow: "0 2px 6px 0 rgb(0 0 0 / 50%)",
    "&::before": {
      content: `''`,
      display: "inline-block",
      width: "1px",
      height: "100%",
      background: "#cccccc",
      position: "absolute",
      top: 0,
      left: "50%",
    }
  },
  frameItem: {
    position: "absolute",
    overflow: "hidden",
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
    "& p": {
      cursor: "pointer",
      fontSize: "16px",
      margin: "auto 0",
      width: "100%",
      height: "100%",
      display: "table"
    },
    "& img": {
      cursor: "pointer",
      maxWidth: "100%",
      width: "100%",
      maxHeight: "100%",
      height: "100%",
      objectFit: "cover",
    }
  },
  pageContainer: {
    width: "100%",
    // position: "absolute",
    // bottom: 0,
    // left: 0,
    // right: 0,
    backgroundColor: "#cccccc",
    padding: "12px 0",
    display: "flex",
    flexWrap: "nowrap",
    alignItems: "flex-end",
    overflowX: "auto",
    marginTop: "40px"
  },
  pageItem: {
    display: "inline-block",
    textAlign: "center",
    cursor: "pointer",
    margin: "0 15px"
  },
  pageItemActived: {
    "& p": {
      backgroundColor: "#ffffff"
    },
    "& $pageItemImage": {
      border: `2px solid ${theme.palette.primary.main}`
    }
  },
  pageItemTitle: {
    width: "80%",
    height: "22px",
    lineHeight: "22px",
    margin: "5px auto",
    borderRadius: "4px",
    color: "#000000",
    fontSize: "14px"
  },
  pageItemImage: {
    width: "128px",
    height: "64px",
    overflow: "hidden",
    background: "#ffffff",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 30%)",
    "& img": {
      verticalAlign: "middle",
      width: "100%",
      height: "100%",
      objectFit: "cover",
    }
  },
  pageItemAdd: {
    margin: "0 20px 0 10px"
  },
  pageItemSwap: {
    marginBottom: "20px"
  },
  itemSwap: {
    background: "#ffffff",
    "&:hover": {
      background: "#ffffff",
      boxShadow: "0 0 3px 2px rgb(0 0 0 / 30%)"
    }
  },
});

class DesignSP extends React.Component {

  constructor(props) {
    super(props);
    this.pageRef = React.createRef();
    this.pageCurrentRef = React.createRef();
  }

  scrollPage(type) {
    if (this.pageCurrentRef.current) {
      let left = ReactDOM.findDOMNode(this.pageCurrentRef.current).offsetLeft - 128 - 22 - 15 - 15 - 17;
      if (type == "next") {
        left = ReactDOM.findDOMNode(this.pageCurrentRef.current).offsetLeft + 128 + (this.props.attrs.selectPageId !== 0 ? 22 : 0) + 13;
      } 
      ReactDOM.findDOMNode(this.pageRef.current).scrollLeft = left;
    }
  }

  onChangePage = type => {
    let selectPageId;
    if (type === "prev") {
      selectPageId = this.props.attrs.selectPageId - 1;
    } else if (type === "next") {
      selectPageId = this.props.attrs.selectPageId + 1;
    };
    this.props.actions.handleChangePage(selectPageId);
    this.scrollPage(type);
  }

  onRotate = (direction) => {
    const rotate = (direction == "left") ? -90 : 90;
    this.cropper.current.cropper.rotate(rotate);
  }

  onCropImage = () => {
    this.props.actions.setState({ cropImage: this.cropper.current.cropper.getCroppedCanvas().toDataURL() });
  }

  render() {
    const { classes, actions, attrs, history } = this.props;
    return (
      <div>
        <div className={classes.root}>
          <div className={classes.designWrap}>
            <div className={classes.frameContainer}>
              <div className={classes.frameWrap}>
                <div className={classes.frame} id="renderLayout">
                {attrs.framesLayout
                    ? attrs.framesLayout.map((frame, index) => {
                      let element;
                      if (frame.image) {
                        element =  (<img onClick={() => actions.openEditImage(frame)} src={`${frame.contentCrop || frame.content}`} />)
                      } else {
                        frame.fonts = frame?.fonts || "Tahoma";
                        frame.style = frame?.style || "normal";
                        frame.color = frame?.color || "#000000";
                        const style = (frame.style == "bold") ? { fontWeight: frame.style } : { fontStyle: frame.style };
                        element = (<Typography
                          style={{ fontSize: (attrs.fontText || 16), ...style, fontFamily: frame.fonts, color: frame.color, whiteSpace: "pre-line", wordBreak: "break-word" }}
                          onClick={() => actions.openEditText(frame)}>
                            <div style={{display: "table-cell", verticalAlign: "middle"}}>{frame.content}</div>
                          </Typography>)
                      }
                      return <div key={index} className={classes.frameItem} style={{
                          top: frame.top/250*100 + "%",
                          left: frame.left/500*100 + "%",
                          width: frame.width/500*100 + "%",
                          height: frame.height/250*100 + "%",
                        }}>
                          { element }
                        </div>
                    })
                    : null
                  }
                </div>
              </div>
              <div>
                <IconButton className={classes.iconChangePage} variant="container"
                  onClick={() => this.onChangePage("prev")} disabled={attrs.selectPageId == 0 ? true : false } >
                  <ChevronLeftIcon style={{fontSize: "36px"}} />
                </IconButton>
                <IconButton className={classes.iconChangePage} variant="container"
                  onClick={() => this.onChangePage("next")} disabled={attrs.selectPageId == (attrs.pagesImage?.length - 1) ? true : false } >
                  <ChevronRightIcon style={{fontSize: "36px"}} />
                </IconButton>
              </div>

            </div>
            <div className={classes.pageContainer} ref={this.pageRef}>
              {attrs.pagesImage && attrs.pagesImage.map((pageItem, index) => (
                <>
                <div className={`${classes.pageItem} ${attrs.selectPageId == pageItem.position ? classes.pageItemActived : ""}`}
                  ref={attrs.selectPageId == pageItem.position ? this.pageCurrentRef : null}
                  onClick={() => actions.handleSelectPage(pageItem.position)}>
                  <p className={classes.pageItemTitle}>{pageItem.title}</p>
                  <div className={classes.pageItemImage}>
                    <img src={pageItem.image} />
                  </div>
                </div>
                </>
                
              ))}
            </div>
          </div>

            {attrs.isOpenEditText && <EditText { ...this.props } />}

            {attrs.isOpenEditImage && attrs.frameImage && <EditImage { ...this.props } /> }
        </div>
      </div>
    );
  };
};


function mapStateToProps(state, props) {
  return {
    attrs: state.admin.design
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(DesignSP);
