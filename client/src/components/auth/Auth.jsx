import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import jwtDecode from 'jwt-decode';

import { Avatar, Button, Paper, Grid, Typography, Container } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import Input from './Input';

import ProgressFilter from "../modules/ProgressFilter";
import Messages from "../common/Messages";

import { Actions } from "../../actions/auth/auth";

const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(2),
      },
      root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
        },
      },
      avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
      },
      form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
      },
      submit: {
        margin: theme.spacing(3, 0, 2),
      }
});

const isAuthTokenValid = access_token => {
  if (!access_token) {
    return false;
  }
  const decoded = jwtDecode(access_token);
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    localStorage.removeItem("profile");
    console.log('access token expired');
    return false;
  } else {
    return true;
  }
};

class Auth extends React.Component {

    handleShowPassword() {
        this.props.actions.setState({ showPassword: !this.props.attrs.showPassword })
    }

    handleChange(e) {
        this.props.actions.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit(e) {
        e.preventDefault();
        const formData = { username: this.props.attrs.username, password: this.props.attrs.password };
        this.props.actions.login(formData);
    }

    componentWillUnmount() {
        this.props.actions.initState();
    }


    render() {
        const user = JSON.parse(localStorage.getItem('profile'));

        const { classes, attrs } = this.props;

        if (user && isAuthTokenValid(user.token)) {
          return <React.Fragment>{this.props.children}</React.Fragment>;
        } else {
          return (
            <Container component="main" maxWidth="xs">
                <Paper className={classes.paper} elevation={6}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">Sign in</Typography>
                <form className={classes.form} onSubmit={(e) => this.handleSubmit(e)}>
                    <Grid container spacing={2}>
                    <Input name="username" label="Username" value={attrs.username} handleChange={(e) => this.handleChange(e)} type="text" />
                    <Input name="password" label="Password" value={attrs.username} handleChange={(e) => this.handleChange(e)} type={attrs.showPassword ? 'text' : 'password'} handleShowPassword={() => this.handleShowPassword()} />
                    </Grid>
                    <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>Sign In</Button>
                </form>
                </Paper>

                <ProgressFilter isShow={attrs && attrs.progress} />
                <Messages anchorOrigin={{ vertical: "top", horizontal: "center" }} />
            </Container>
          );
        }
    }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.auth.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Auth);
