import React from 'react';
import {connect} from "react-redux";
import {compose} from "recompose";
import {withStyles} from "@material-ui/core/styles";
import {bindActionCreators} from "redux";
import Header from "../common/Header";
import DeleteIcon from '@material-ui/icons/Delete';

import {
  Button,
  Grid,
  Paper,
  Checkbox,
  Typography,
  FormControl,
  InputLabel,
  OutlinedInput,
  Select,
  MenuItem,
  IconButton,
} from '@material-ui/core';

import {Actions} from "../../actions/user/order";

import * as Storage from "../../storage/index";

const styles = theme => ({
  oderPage: {
    maxWidth: '1200px',
    margin: '20px auto 0',
    padding: '20px'
  },
  photobookOrderItem: {
    display: 'flex',
    boxShadow: '0 2px 5px 0 rgb(0 0 0 / 30%)',
    marginBottom: '15px',
    padding: '20px',
    borderRadius: '10px',
    marginTop: '15px'
  },
  photobookOrderItemContent: {
    display: 'flex',
    width: '100%',
    fontSize: "14px"
  },
  photobookOrderItemContentImage: {
    padding: '10px',
    border: '1px solid #ccc',
    width: '90px',
    height: '90px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '20px'
  },
  photobookOrderItemImage: {
    maxWidth: '70px'
  },
  photobookOrderItemBody: {
    width: '100%'
  },
  photobookOrderItemRow: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    margin: "5px 0"
  },
  photobookOrderItemName: {
    fontSize: '15px',
    lineHeight: 1.3
  },
  photobookOrderItemQuantity: {
    display: 'flex'
  },
  photobookOrderItemQuantityInput: {
    width: '20px',
    height: '20px',
    display: 'inline-block',
    textAlign: 'center',
    border: 0
  },
  photobookOrderItemPlus: {
    height: '20px',
    width: '20px',
    borderRadius: '50%',
    display: 'inline-block',
    background: 'rgb(221, 221, 221);',
    fontSize: '20px',
    textAlign: 'center',
    lineHeight: '15px',
    marginLeft: '5px',
    cursor: 'pointer'
  },
  photobookOrderItemMinus: {
    height: '20px',
    width: '20px',
    borderRadius: '50%',
    display: 'inline-block',
    background: 'rgb(221, 221, 221);',
    fontSize: '20px',
    textAlign: 'center',
    lineHeight: '15px',
    marginRight: '5px',
    cursor: 'pointer'
  },
  photobookOrderItemPrice: {
    fontSize: '14px',
    textAlign: "right",
    minWidth: "82px"
  },
  orderInforContent: {
    padding: '20px',
    fontSize: "14px"
  },
  orderInfoItem: {
    borderBottom: '2px solid #ccc',
    paddingBottom: '10px'
  },
  orderInfoItemRow: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  orderInfoItemPhotobookName: {
    padding: '5px 5px 5px 0',
    margin: 0,
    display: 'flex',
    alignItems: 'center',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
  photobookOrderItemDecs: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    color: '#9d9d9d'
  },
    oderInfo: {
      boxShadow: '0 2px 5px 0 rgb(0 0 0 / 30%)'
    },
  orderInfoItemPhotobookPrice: {
    padding: '5px 0',
    minWidth: "95px",
    margin: 0,
    textAlign: "right",
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
  orderInfoItemPhotobookTitle: {
    fontSize: '15px',
    fontWeight: 'bold',
  },
  orderInfoItemPhotobookPromotion: {
    width: '100px',
    height: '30px',
    textAlign: 'center',
    border: '1px solid rgb(204, 204, 204)',
    marginLeft: '5px'
  },
  orderInforCustomerItem: {
    width: '100%',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
  orderInforCustomerForm: {
    width: '100%',
    marginTop: '20px'
  },
  orderAction: {
    marginTop: '20px',
    display: 'flex',
    justifyContent: 'end'
  },
  photobookOrderInfoTitle: {
    marginBottom: '15px',
    fontWeight: '600'
  },
  photobookOrderTitle: {
    fontWeight: '600'
  },
  orderInfoItemRowContent: {
    marginTop: '15px',
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between'
  },
  orderInfoItemPhotobookTotalPayment: {
    fontWeight: 600,
    color: theme.palette.primary.main
  },
  orderInfoItemRowContentPromotion: {
    display: 'flex',
    flexWrap: 'wrap',
    width: "100%",
  },
  promotionValue: {
    color: theme.palette.primary.main
  },
  orderInfoItemPhotobookHelpTest: {
    width: '100%',
    margin: '0 0 15px 0',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    color: '#ff0000',
    fontStyle: 'italic',
    fontSize: '13px',
  },
  orderInfoItemPromotionInfor: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: "center"
  }
});

class Order extends React.Component {

    handleSubmit(e, history) {
    e.preventDefault();
    this.props.actions.handleOrder(history);
  }

  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    const idPhotobook = Storage.getCurrentIdPhotobookLocalStorage();
    const photobooks = Storage.getListPhotobookLocalStorage();
    if (!idPhotobook || idPhotobook == null || idPhotobook == ""
        || !photobooks || photobooks == {}) {
        this.props.history.push("/");
    }

    if (Storage.getAttrPhotobook("step") && Storage.getAttrPhotobook("step") != "order") {
        this.props.history.push(`/${Storage.getAttrPhotobook("step")}`);
    }

    this.props.actions.initPage();
  }

  render() {
    const {classes, actions, attrs, history} = this.props;
    return (
      <div>
        <Header history={history} title={"Đặt Hàng"} />
        <div className={classes.oderPage}>
          <Grid container spacing={3}>
            <Grid item md={7} xs={12} sm={12}>
              <Typography className={classes.photobookOrderTitle} variant="h6" component="div" sx={{flexGrow: 1}}>
                Sản phẩm
              </Typography>
              <div className={classes.photobookOrder}>
                <div className={classes.photobookOrderContent}>
                  {attrs.photobooks && attrs.photobooks.map((item, index) => (
                    <div key={index} className={classes.photobookOrderItem}>
                      <div className={classes.photobookOrderCheck}>
                        <Checkbox
                          checked={item.isPrint}
                          value={item.isPrint}
                          onChange={() => actions.handleOnPrint(item.idphotobook)}
                          color="primary"
                          inputProps={{'aria-label': 'secondary checkbox'}}
                        />
                      </div>
                      <div className={classes.photobookOrderItemContent}>
                        <div className={classes.photobookOrderItemContentImage}>
                          <img className={classes.photobookOrderItemImage}
                               src={item.photobookType.image}
                          />
                        </div>
                        <div className={classes.photobookOrderItemBody}>
                          <div className={classes.photobookOrderItemRow}>
                            <Typography className={classes.photobookOrderItemName} variant="h6" component="div"
                                        sx={{flexGrow: 1}}>
                              {item.photobookType.name}
                            </Typography>
                            <Typography className={classes.photobookOrderItemPrice} variant="h6" component="div"
                                        sx={{flexGrow: 1}}>
                              {actions.moneyFormat(item.price)}
                            </Typography>
                          </div>
                          <div className={classes.photobookOrderItemRow}>
                            <div className={classes.photobookOrderItemDecs}>
                              {item.pageSizeName} - {item.pageTotal} Trang
                            </div>
                          </div>
                          <div className={classes.photobookOrderItemRow} style={{ alignItems: "center" }}>
                            <div className={classes.photobookOrderItemQuantity}>
                              <span className={classes.photobookOrderItemMinus} onClick={() => actions.handleQuantity("minus", item.idphotobook)}>-</span>
                              <input className={classes.photobookOrderItemQuantityInput} type="text"
                                     value={item.quantity} onChange={()=>{}}/>
                              <span className={classes.photobookOrderItemPlus} onClick={() => actions.handleQuantity("plus", item.idphotobook)}>+</span>
                            </div>
                            <div className={classes.photobookOrderItemAction}>
                              <IconButton aria-label="delete" className={classes.margin} onClick={() => actions.handleDeletePhotobook(item.idphotobook)}>
                                <DeleteIcon fontSize="small" />
                              </IconButton>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </Grid>
            <Grid item md={5} sm={12} xs={12}>
              <Typography className={classes.photobookOrderInfoTitle} variant="h6" component="div" sx={{flexGrow: 1}}>
                Đơn hàng
              </Typography>
              <Paper className={classes.oderInfo}>
                <div className={classes.orderInforContent}>
                  <div className={classes.orderInfoItem}>
                    {attrs.photobooks && attrs.photobooks.map((item, index) => {
                      if (item.isPrint) {
                        return (<div key={index} className={classes.orderInfoItemRow}>
                          <p className={classes.orderInfoItemPhotobookName}>{item.quantity} x {item.photobookType.name} {item.pageSizeName}</p>
                          <p className={classes.orderInfoItemPhotobookPrice}>{actions.moneyFormat(item.price*item.quantity)}</p>
                        </div>)
                      }
                      return null;
                    })
                    }
                  </div>
                  <div className={classes.orderInfoItem}>
                    <div className={classes.orderInfoItemRow}>
                      <div className={classes.orderInfoItemPhotobookName}>
                        <Typography className={classes.orderInfoItemPhotobookTitle} variant="h6" component="div"
                                    sx={{flexGrow: 1}}>
                          Tổng Tiền
                        </Typography>
                      </div>
                      <p
                        className={classes.orderInfoItemPhotobookPrice}>{actions.moneyFormat(attrs.totalPrice || 0)}</p>
                    </div>
                    <div className={classes.orderInfoItemRow}>
                      <div className={classes.orderInfoItemRowContentPromotion}>
                        <div className={classes.orderInfoItemPromotionInfor}>
                          <div className={classes.orderInfoItemPhotobookName}>
                            <Typography className={classes.orderInfoItemPhotobookTitle}
                                        variant="h6"
                                        component="div"
                                        sx={{flexGrow: 1}}>Mã Giảm Giá
                            </Typography>
                            <input type="text" onBlur={(e) => actions.handlePromotion(e.target.value)} onKeyDown={ (e) => {
                              if (e.key == 'Enter' || e.keyCode == 13) actions.handlePromotion(e.target.value)
                            }} className={classes.orderInfoItemPhotobookPromotion}/>
                          </div>
                          {attrs.promotion && 
                          <p className={`${classes.orderInfoItemPhotobookPrice} ${classes.promotionValue}`}> - { (attrs.promotion.type == 1) ? (attrs.promotion.value.$numberDecimal + "%") : actions.moneyFormat(attrs.promotion.value.$numberDecimal)}</p>
                          }
                        </div>
                      </div>
                    </div>
                    {attrs.promotionError &&
                      <p className={classes.orderInfoItemPhotobookHelpTest}>Mã Giảm Giá Không Hợp Lệ</p>
                    }
                  </div>
                  <div className={classes.orderInfoItem}>
                    <div className={classes.orderInfoItemRow}>
                      <div className={classes.orderInfoItemRowContent}>
                        <div className={classes.orderInfoItemPhotobookName}>
                          <Typography className={classes.orderInfoItemPhotobookTitle} variant="h6" component="div"
                                      sx={{flexGrow: 1}}>
                            Thanh Toán
                          </Typography>
                        </div>
                        <p className={classes.orderInfoItemPhotobookTotalPayment}> {actions.moneyFormat(attrs.paymentPrice || 0)}</p>
                      </div>
                    </div>
                  </div>
                  <div className={classes.customerInfo}>
                    <div className={classes.orderInforCustomer}>
                    <form onSubmit={(e) => this.handleSubmit(e, history)}>
                      <FormControl className={classes.orderInforCustomerForm} variant="outlined">
                        <InputLabel htmlFor="component-full_name">Tên</InputLabel>
                        <OutlinedInput
                          id="component-full_name"
                          name="full_name"
                          label="Tên"
                          required
                          value={attrs.full_name}
                          onChange={actions.handleInputChange}
                          className={classes.orderInforCustomerItem} 
                          />
                      </FormControl>
                      <FormControl className={classes.orderInforCustomerForm} variant="outlined">
                        <InputLabel htmlFor="component-phone">Số điện thoại</InputLabel>
                        <OutlinedInput
                          type='number'
                          id="component-phone"
                          name="phone"
                          label="Số điện thoại"
                          required
                          value={attrs.phone}
                          onChange={actions.handleInputChange}
                          className={classes.orderInforCustomerItem} 
                          />
                      </FormControl>
                      <FormControl className={classes.orderInforCustomerForm} variant="outlined">
                        <InputLabel htmlFor="component-address">Địa chỉ</InputLabel>
                        <OutlinedInput
                          id="component-address"
                          name="address"
                          label="Địa chỉ"
                          required
                          value={attrs.address}
                          onChange={actions.handleInputChange}
                          multiline
                          rows={2}
                          className={classes.orderInforCustomerItem} 
                          />
                      </FormControl>
                      <FormControl className={classes.orderInforCustomerForm} variant="outlined">
                        <InputLabel htmlFor="component-email">Ghi Chú</InputLabel>
                        <OutlinedInput
                          id="component-note"
                          name="note"
                          label="Ghi Chú"
                          value={attrs.note}
                          multiline
                          rows={2}
                          onChange={actions.handleInputChange}
                          className={classes.orderInforCustomerItem} 
                          />
                      </FormControl>
                      <div className={classes.orderAction}>
                        <Button variant="contained" color="primary" type="submit" disabled={!(attrs.totalPrice && attrs.totalPrice > 0)}>Đặt Hàng</Button>
                      </div>
                      </form>
                    </div>

                  </div>
                </div>
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  };
};


function mapStateToProps(state, props) {
  return {
    attrs: state.user.order
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default compose(
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps)
)(Order);
