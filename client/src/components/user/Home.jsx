import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";
import Header from "../common/Header";

import {
  Grid,
  Typography,
  Box,
  Modal,
  Fade,
  Backdrop,
  Button,
  IconButton
} from "@material-ui/core";

import CloseIcon from "@material-ui/icons/Close";

import { Actions } from "../../actions/user/home";

const styles = (theme) => ({
  contentVideo: {
    margin: "0 auto",
    borderRadius: "20px",
    width: '90vw',
    height: '50vw',
    maxWidth: "800px",
    maxHeight: "450px",
  },
  frameVideo: {
    border: "none",
    borderRadius: "20px",
    width: '100%',
    height: '100%',
  },
  photobookType: {
    maxWidth: "1200px",
    margin: "0 auto",
  },
  photobookItem: {
    padding: "10px",
  },
  photobookItemContent: {
    background: "#ffffff",
    padding: "10px",
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.primary.main,
      "& img": {
        transform: "scale(1.2)"
      }
    },
    "@media (max-width: 599px)": {
      maxWidth: "450px"
    },
  },
  photobookImageWrap: {
    height: '200px',
    overflow: 'hidden',
    display: 'flex',
    justifyContent: "center",
    alignItems: 'center'
  },
  photobookImage: {
    width: "100%",
    height: "auto",
    transition: "transform 0.5s ease"
  },
  photobookTitle: {
    textAlign: "center",
    fontSize: "15px",
    fontWeight: "bold",
    marginTop: theme.spacing(2),
    transition: 'all 0.5s ease'
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: "40%",
    border: "unset",
  },
  paperModal: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(1, 2, 3),
    fontSize: "15px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px",
    }
  },
  btnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    spacing: 2,
    paddingTop: "30px",
  },
});

class Home extends React.Component {
  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    this.props.actions.fetchDatas();
  }

  render() {
    const { classes, actions, attrs, history } = this.props;
    return (
      <div>
        <Header history={history} title={"Trang Chủ"} />
        <Box p={[1, 2, 3]}>
          <Grid
            container
            className={classes.video}
            wrap="nowrap"
            direction="column"
          >
            <div className={classes.contentVideo}>
              <iframe
                className={classes.frameVideo}
                src="https://fast.wistia.com/embed/medias/mg31enqrln"
              ></iframe>
            </div>
          </Grid>
        </Box>
        <Box p={[2, 2, 3]} className={classes.photobookType}>
          <Grid container spacing={1} justifyContent="center">
            {attrs.listPhotobookType.map((item, index) => (
              <Grid item key={index} md={4} sm={6} xs={12}>
                <div className={classes.photobookItem}>
                  <div
                    className={classes.photobookItemContent}
                    onClick={() => {
                      actions.handleGoToSettingPage(item, history);
                    }}
                  >
                    <div className={classes.photobookImageWrap}>
                      <img
                        className={classes.photobookImage}
                        src={item.image}
                        alt={item.descriptions}
                      />
                    </div>
                    <Typography
                      className={classes.photobookTitle}
                      variant="h6"
                      component="div"
                      sx={{ flexGrow: 1 }}
                    >
                      {item.name}
                    </Typography>
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>
        </Box>

        <div>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={attrs.isModalOpen}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={attrs.isModalOpen}>
              <div className={classes.paperModal}>
                <h2 id="transition-modal-title" style={{ textAlign: "right", margin: 0 }}><IconButton onClick={actions.closeModalConfirm}><CloseIcon /></IconButton></h2>
                <Typography id="transition-modal-description" variant="inherit">
                  Bạn có thiết kế đang làm dở, bạn có muốn tiếp tục bản thiết kế
                  đó không? Nếu tạo thiết kế mới, thiết kế cũ sẽ bị xoá.
                </Typography>
                <div className={classes.btnGroup}>
                  <Box px={1}>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => actions.handleCreateNewPhotobook(history)}
                    >
                      Không, tạo mới
                    </Button>
                  </Box>
                  <Box px={1}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() =>
                        actions.handleContinueSettingPhotobook(history)
                      }
                    >
                      Có, làm tiếp
                    </Button>
                  </Box>
                </div>
              </div>
            </Fade>
          </Modal>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.user.home,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Home);
