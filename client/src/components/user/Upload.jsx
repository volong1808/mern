import React from 'react';
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import { Actions } from "../../actions/user/upload";

import * as Storage from "../../storage/index";

import { Container } from '@material-ui/core';
import { Button } from '@material-ui/core';
import CloseIcon from "@material-ui/icons/Close";
import { Card } from '@material-ui/core';

import { DialogTitle, DialogContent, IconButton } from '@material-ui/core';

import ResponsiveDialog from '../modules/ResponsiveDialog';
import Confirm from '../modules/Confirm';
import ImageComponent from '../modules/User/Upload/ImageComponent';
import Header from "../common/Header";

const styles = theme => ({
    root: {
        width: "100%",
        height: "100vh",
        display: "flex",
    },
    flexContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },

    cardNote: {
        width: "90%",
        maxWidth: "1200px",
        display: "block",
        margin: "auto",
        padding: "10px",
    },

    menuAdd: {
        display: "flex",
        justifyContent: "center",
        padding: "10px",
    },

    textGreen: {
        color: "green",
    },

    textRed: {
        color: "red",
    },

    btn: {
        display: "inline-block !important",
        marginRight: "10px",
        "&:hover": {
            backgroundColor: "#0da0c5",
            color: "white",
        },
        transition: "all 0.2s ease",
    },

    btnAddArea: {
        textAlign: "center",
    },

    imgInput: {
        display: "none",
    },

    headModal: {
        display: "flex",
        justifyContent: "space-between",
        jutifyItem: "center",
        borderBottom: "1px solid #aaaaff"
    },

    titleModal: {
        display: "inline-block",
        margin: "auto",
        fontWeight: "bold",
        color: "#0da0c5",
    },

    frameUploaded: {
        marginTop: "10px",
        display: "flex",
        justifyContent: "center",
    },

    uploadItemLeft: {
        marginTop: "10px",
        width: "40%",
        textOverflow: "ellipsis",
    },

    uploadItemRight: {
        marginTop: "10px",
        width: "40%",
        border: "1px solid #0da0c5",
        borderRadius: "4px",
        textAlign: "center",
    },

    uploadItemRightCompleted: {
        marginTop: "10px",
        width: "40%",
        border: "1px solid #0da0c5",
        borderRadius: "4px",
        backgroundColor: "#0da0c5",
        textAlign: "center",
        color: "white",
    },

    uploadItemHeadLeft: {
        width: "40%",
        fontWeight: "bold",
    },

    uploadItemHeadRight: {
        width: "40%",
        fontWeight: "bold",

    },

    imgPreview: {
        height: "auto",
        width: "100%",
        objectFit: 'cover',
    },

    uploadNote: {
        textAlign: "center",
    },

    imageContainer: {
        display: "flex",
        justifyContent: "center",

    },

    contentImage: {
        margin: "20px 0px",
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "space-between",
        width: "95%",
        maxWidth: "1200px"
    }

});

class Upload extends React.Component {

    componentWillUnmount() {
        this.props.actions.initState();
    }

    componentDidMount() {
        const idPhotobook = Storage.getCurrentIdPhotobookLocalStorage();
        const photobooks = Storage.getListPhotobookLocalStorage();
        if (!idPhotobook || idPhotobook == null || idPhotobook == ""
            || !photobooks || photobooks == {}) {
            this.props.history.push("/");
        }

        if (Storage.getAttrPhotobook("step") && Storage.getAttrPhotobook("step") != "upload") {
            this.props.history.push(`/${Storage.getAttrPhotobook("step")}`);
        }

        this.props.actions.fetchDatas();
    }

    render() {
        const { classes, actions, attrs, history } = this.props;

        const design = Storage.getAttrPhotobook("design");
        let imagesFrames = [];
        if (design.pagesImage && design.pagesImage.length) {
            design.pagesImage.forEach(page => {
                if (page.layoutItem?.m_frames && Array.isArray(page.layoutItem.m_frames) && page.layoutItem.m_frames.length) {
                    page.layoutItem.m_frames.forEach(frame => {
                        if (frame?.content) {
                            imagesFrames.push(frame.content);
                        }
                    });
                }
            });
        }

        return (
            < Container maxWidth="xl" disableGutters={true} >
                <Header history={history} title={"TẢI ẢNH"} />
                <div className={classes.uploadNote}>
                    <h3>Chọn ảnh</h3>
                    <Card className={classes.cardNote}>
                        {(attrs.listCurrentImage && attrs.listCurrentImage.length > 0) && <p className={classes.textGreen}>Bạn đã tải {attrs.listCurrentImage.length}/{attrs.max} ảnh, phù hợp với {Math.trunc(attrs.listCurrentImage.length / attrs.perPage)} trang</p>}
                        {(!attrs.listCurrentImage || attrs.listCurrentImage.length == 0 || attrs.listCurrentImage.length < attrs.max) && <p className={classes.textRed}>Bạn chưa tải đủ ảnh, cần tối thiểu {attrs.min} ảnh.</p>}
                    </Card>
                </div>
                <div className={classes.menuAdd}>
                    <Button variant="outlined" color="primary" onClick={(e) => actions.handleOnclickBtn(e, {})} data-action="addImage" size="large" className={classes.btn}>
                        Thêm ảnh
                    </Button>
                    <Button variant="outlined" color="primary" onClick={(e) => actions.handleOnclickBtn(e, this.props)} data-action="nextStep" size="large" className={classes.btn} disabled={(attrs.listCurrentImage && attrs.listCurrentImage.length < attrs.max)}>
                        Tạo thiết kế
                    </Button>
                    <form action="" id="form-image">
                        <input
                            type="file"
                            accept="image/png, image/jpeg"
                            className={classes.imgInput}
                            name="images"
                            id="upload_image"
                            multiple onChange={(e) => {
                                actions.handleOnChange(e, {
                                    id: "form-image",
                                    currentId: attrs.currentPhotobookId,
                                })
                            }}>
                        </input>
                    </form>
                </div >
                {
                    attrs.listCurrentImage &&
                    <div className={classes.imageContainer}>
                        <div className={classes.contentImage}>
                            {attrs.listCurrentImage.map(item => (
                                < ImageComponent style={{ opacity: imagesFrames.length && imagesFrames.includes(item.path) ? 0.6 : 1 }} imgInfo={item} key={item.key} handleFunction={actions.handleImage} ></ImageComponent>
                            ))}
                        </div>
                    </div >
                }
                {
                    attrs.isOpenModalImage ?
                        <ResponsiveDialog open scroll="paper" maxWidth="sm" fullWidth>
                            <DialogTitle>
                                <div className={classes.headModal}>
                                    <div className={classes.titleModal}>Ảnh phóng to</div>
                                    <IconButton onClick={actions.closeModalImage}><CloseIcon /></IconButton>
                                </div>
                            </DialogTitle>
                            <DialogContent style={{ padding: "10px 20px 20px 20px" }}>
                                <img src={attrs.imagePreview} alt="load fails" className={classes.imgPreview} />
                            </DialogContent>
                        </ResponsiveDialog>
                        : null
                }
                {attrs.isOpenModalConfirm ?
                    <Confirm open={attrs.isOpenModalConfirm}
                        message="Bạn muốn xóa ảnh này?"
                        onCancel={actions.closeModalConfirm}
                        onDone={() => actions.handleAccept(attrs.imageInfor)}
                    /> : null}
            </Container >
        );
    };
};


function mapStateToProps(state, props) {
    return {
        attrs: state.user.upload
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, mapDispatchToProps)
)(Upload);
