import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import {
  Button,
  Grid,
  Typography,
  Card,
  CardContent,
  FormControl,
  Select,
  MenuItem,
  InputLabel,
  Box,
  Paper,
} from "@material-ui/core";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import { Actions } from "../../actions/user/option";
import Header from "../common/Header";
import * as Storage from "../../storage/index";
import ProgressScreen from "../modules/ProgressFilter";

const styles = (theme) => ({
  formControl: {
    margin: "10px 0",
  },
  container: {
    backgroundColor: "#f2f2f2",
    width: "100%",
    minHeight: "100vh",
    margin: 0,
    padding: 0,
  },
  optionContainer: {
    maxWidth: "1200px",
    margin: "0 auto",
  },
  paperImage: {
    display: "flex",
  },
  image: {
    width: "auto",
    maxWidth: "100%",
    height: "auto",
    margin: "auto",
  },
  card: {
    "@media (max-width: 599px)": {
      backgroundColor: "unset",
      border: 0,
      borderRadius: "unset",
      boxShadow: "unset",
    },
  },
  buttonSubmit: {
    display: "flex",
    margin: "10px auto",
  },
  imageWrap: {
    order: 1,
    [theme.breakpoints.down("sm")]: {
      order: 2,
      paddingTop: "0 !important"
    }
  },
  optionWrap: {
    order: 2,
    [theme.breakpoints.down("sm")]: {
      order: 1,
      paddingBottom: "0 !important"
    }
  }
});

class Option extends React.Component {
  componentWillUnmount() {
    this.props.actions.initState();
  }

  componentDidMount() {
    const idPhotobook = Storage.getCurrentIdPhotobookLocalStorage();
    const photobooks = Storage.getListPhotobookLocalStorage();

    if (!idPhotobook || idPhotobook == null || idPhotobook == ""
     || !photobooks || photobooks == {}) {
      this.props.history.push("/");
    }

    if (Storage.getAttrPhotobook("step") && Storage.getAttrPhotobook("step") != "option") {
      this.props.history.push(`/${Storage.getAttrPhotobook("step")}`);
    }
    
    this.props.actions.fetchStorageData();
    this.props.actions.fetchDatas();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const currentAttrs = this.props.attrs;
    const prevAttrs = prevProps.attrs;

    const { pageSize, pageTotal, pageCover } = currentAttrs;

    if (
      pageSize !== prevAttrs.pageSize ||
      pageTotal !== prevAttrs.pageTotal ||
      pageCover !== prevAttrs.pageCover
    ) {
      if (pageSize && pageTotal && pageCover) {
        this.props.actions.handleGetPrice(currentAttrs);
      } else {
        this.props.actions.handleResetPrice();
      }
    }
  }

  render() {
    const { classes, actions, attrs, history } = this.props;

    if (!attrs.listPageSize && !attrs.listPageCover && !attrs.photobookType) {
      return <ProgressScreen />
    } else {
      return (
        <div className={classes.container}>
          <Header history={history} title={"Tùy Chọn"} />
          <div className={classes.optionContainer}>
            <Box p={[0, 2, 4]}>
              <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="stretch"
                spacing={3}
                style={{ margin: 0, width: "100%" }}
              >
                <Grid item md={8} sm={7} xs={12} className={classes.imageWrap}>
                  <Paper style={{ padding: 15 }}>
                    {attrs.pageCoverDesciption && <Typography align="center" style={{ marginBottom: 10, fontWeight: "500" }}>{attrs.pageCoverDesciption}</Typography>}
                    <div className={classes.paperImage}>
                      <img
                        src={attrs.pageCoverImage}
                        alt="image"
                        className={classes.image}
                      />
                    </div>
                  </Paper>
                </Grid>
                <Grid item md={4} sm={5} xs={12} className={classes.optionWrap}>
                  <Card className={classes.card}>
                    <form
                      onSubmit={(e) => {
                        actions.handleSubmit(e, this.props.attrs);
                        this.props.history.push("/upload");
                      }}
                    >
                      <CardContent style={{padding: 15}}>
                        <Typography gutterBottom variant="h6" component="h3">
                          {attrs.photobookType?.name}
                        </Typography>
                        <Typography gutterBottom
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {attrs.photobookType?.descriptions}
                        </Typography>
                        <FormControl
                          fullWidth
                          variant="outlined"
                          className={classes.formControl}
                        >
                          <InputLabel id="size-label">Kích thước</InputLabel>
                          <Select
                            name="pageSize"
                            label="Kích thước"
                            labelId="size-label"
                            value={attrs.pageSize}
                            onChange={actions.handleInputChange}
                          >
                            <MenuItem value=""></MenuItem>
                            {attrs.listPageSize.map((item) => (
                              <MenuItem key={item._id} value={item._id}>
                                {`${item.width}x${item.height} ${item.unit}`}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
  
                        <FormControl
                          fullWidth
                          variant="outlined"
                          className={classes.formControl}
                        >
                          <InputLabel id="total-label">Số trang</InputLabel>
                          <Select
                            name="pageTotal"
                            label="Số trang"
                            labelId="total-label"
                            value={attrs.pageTotal}
                            onChange={actions.handleInputChange}
                          >
                            <MenuItem value=""></MenuItem>
                            {attrs.listPageTotal.map((item) => (
                              <MenuItem key={item} value={item}>
                                {item}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
  
                        <FormControl
                          fullWidth
                          variant="outlined"
                          className={classes.formControl}
                        >
                          <InputLabel id="cover-label">Trang Bìa</InputLabel>
                          <Select
                            name="pageCover"
                            label="Trang bìa"
                            labelId="cover-label"
                            value={attrs.pageCover}
                            onChange={actions.handleInputChange}
                          >
                            <MenuItem value=""></MenuItem>
                            {attrs.listPageCover.map((item) => (
                              <MenuItem key={item._id} value={item._id}>
                                {item.name}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                        <Typography variant="h6">{`Giá: ${attrs.price.toLocaleString()}đ`}</Typography>
                        <Button
                          size="medium"
                          variant="contained"
                          color="primary"
                          type="submit"
                          className={classes.buttonSubmit}
                          disabled={
                            !(attrs.pageCover && attrs.pageSize && attrs.pageTotal)
                          }
                        >
                          Tiếp theo
                          <ArrowForwardIcon />
                        </Button>
                      </CardContent>
                    </form>
                  </Card>
                </Grid>
              </Grid>
            </Box>
          </div>
        </div>
      );
    }
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.user.option,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Option);
