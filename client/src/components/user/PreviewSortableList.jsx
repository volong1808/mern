import { Grid } from "@material-ui/core";
import React from "react";
import { SortableContainer } from "react-sortable-hoc";
import PreviewSortableItem from "./PreviewSortableItem";

const PreviewSortableList = SortableContainer(({ items, classes, handleClick }) => {
  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
      spacing={3}
    >
      {items.map((value, index) => (
        <PreviewSortableItem
          key={`item-${index}`}
          index={index}
          value={value}
          classes={classes}
          incrementPageIndex={index}
          // Disable first page
          collection={index === 0 ? 1 : 0}
          handleClick={handleClick}
        />
      ))}
    </Grid>
  );
});

export default PreviewSortableList;
