import React from 'react';
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import ResponsiveComponent from '../common/ResponsiveComponent';
import DesignPC from './DesignPC';
import DesignSP from './DesignSP';

import { Actions } from "../../actions/user/design";
import { Actions as headerAction } from "../../actions/common/header";

import * as Storage from "../../storage/index";

const styles = theme => ({
});

class Design extends ResponsiveComponent {

  componentWillUnmount() {
    // this.props.actions.initState();
  }

  componentDidMount() {
    const idPhotobook = Storage.getCurrentIdPhotobookLocalStorage();
    const photobooks = Storage.getListPhotobookLocalStorage();
    if (!idPhotobook || idPhotobook == null || idPhotobook == ""
     || !photobooks || photobooks == {}
     || !Storage.getAttrPhotobook('upload')
     || Storage.getAttrPhotobook('upload').length == 0) {
      this.props.history.push("/");
    }

    if (Storage.getAttrPhotobook("step") && Storage.getAttrPhotobook("step") != "design") {
      this.props.history.push(`/${Storage.getAttrPhotobook("step")}`);
    }

    this.props.actions.fetchDatas();
    const fontText = document.getElementById("renderLayout")?.offsetWidth*16/1096;
    this.props.actions.setState({ fontText });
  }

  renderPC() {
    return <DesignPC {...this.props} />;
  }

  renderSP() {
    return <DesignSP {...this.props} />;
  }

};


function mapStateToProps(state, props) {
  return {
    attrs: state.user.design
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
    headerAction: bindActionCreators(headerAction, dispatch)
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Design);
