import React from 'react';
import {connect} from "react-redux";
import {compose} from "recompose";
import {withStyles} from "@material-ui/core/styles";
import {bindActionCreators} from "redux";
import moment from  "moment";
import Header from "../common/Header";
import {Actions} from "../../actions/user/success";
import {Alert} from '@material-ui/lab';
import {
  Grid, Typography
} from '@material-ui/core';


import dotenv from 'dotenv';
dotenv.config();
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";
const styles = theme => ({
  pageSuccess: {
    padding: '0 25px',
    maxWidth: '960px',
    margin: 'auto'
  },

  alertSuccess: {
    marginTop: '20px',
  },

  alertSuccessOrderCode: {
    fontWeight: 500,
    color: '#0da0c5'
  },

  invoice: {
    padding: '20px',
    background: '#ffffff',
    border: '1px solid #e5e5e5',
    marginTop: '20px'
  },

  invoiceHeader: {
    borderBottom: '1px solid #ccc',
    paddingBottom: '20px',
    marginBottom: '20px'
  },

  invoiceHeaderItem: {
    display: 'flex',
  },

  invoiceHeaderItemInfoCompany: {
    paddingLeft: '10px'
  },

  invoiceHeaderItemInfoCompanyTitle: {
    fontSize: '18px',
    fontWeight: 600
  },

  invoiceHeaderItemInfoCompanyText: {
    fontSize: '14px',
    lineHeight: 1.3,
    fontStyle: 'italic',
    color: '#544f4f'
  },

  bold: {
    fontWeight: 600,
    display: 'inline-block',
    width: '20px'
  },

  invoiceOrder: {
    textAlign: 'right'
  },

    invoiceHeaderItemTitle: {
    fontSize: '25px'
    },

  invoiceOrderItem: {
    display: 'flex',
    justifyContent: 'space-between',
  },

  invoiceOrderCode: {
    fontSize: '20px',
    lineHeight: '25px',
    fontWeight: 600,
    color: '#0da0c5'
  },

  invoiceOrderText: {
    fontSize: '16px',
    lineHeight: '25px',
    color: '#544f4f',
    fontStyle: 'italic',
  },

  orderInfoTitle: {
    fontSize: '18px',
    fontWeight: 600,
    marginBottom: '10px'
  },

  orderInfoLabel: {
    width: '60px',
    display: 'inline-block',
    fontWeight: 600
  },

  orderInfoItem: {
    fontSize: '14px',
  },

  orderNote: {
    padding: '10px',
    border: '1px solid #ccc',
    background: '#e5e5e5',
    marginTop: '10px',
    fontSize: '16px',
    color: '#000000',
    fontWeight: 200,
    fontStyle: 'italic',
  },

  orderInfoNote: {
    fontWeight: 600
  },

  orderInfo: {
    borderBottom: '1px solid #ccc',
    paddingBottom: '20px',
    marginBottom: '20px'
  },

  orderContent: {
    marginTop: '20px'
  },

  orderItemImage: {
    width: '100%'
  },

  orderContentRow: {
    padding: '10px',
    border: '1px dashed #cccccc'
  },

  photobookOrderItemName: {
    fontSize: '16px',
    lineHeight: '20px'
  },

  photobookOrderItemPrice: {
    fontWeight: 400
  },

  photobookOrderItemLabel: {
    display: 'inline-block',
    paddingRight: '15px'
  },

  photobookOrderItemTextTotal: {
    fontWeight: 600
  },

  orderContentFooter: {
    marginTop: '10px'
  },

  orderTotal: {
    background: '#e5e5e5',
    textAlign: 'left',
  },

  orderTotalItem: {
    padding: '0 10px',
    fontSize: '16px',
    border: '1px dashed #cccccc'
  },

  orderTotalItemLabel: {
    display: 'inline-block',
    width: '100px',
    textAlign: 'left',
    fontWeight: 400
  },

  orderTotalItemBold: {
    fontWeight: 600,
    color: '#ff0000',
    padding: '0 10px',
    fontSize: '16px',
    border: '1px dashed #cccccc'
  },

  orderTotalItemLabelBold: {
    display: 'inline-block',
    fontWeight: 600,
    width: '100px',
    textAlign: 'left'
  }
})

class Success extends React.Component {
  getOrderId() {
    return this.props.match.params.idOrder;
  }
  componentWillUnmount() {
    this.props.actions.initState(this.getOrderId());
  }
  componentDidMount() {
    this.props.actions.initPage(this.getOrderId());
  }
  render() {
    const {classes, actions, attrs, history} = this.props;
    return (
      <div>
        <Header history={history} title={"Đặt Hàng Thành Công"} />
        <div className={classes.pageSuccess}>
          <div className={classes.alertSuccess}>
            <Alert variant="outlined" severity="success">
              Đơn hàng <span className={classes.alertSuccessOrderCode}>{attrs.order?.code}</span> đã đặt hàng thành công!
            </Alert>
          </div>
          <div className={classes.invoice}>
            <div className={classes.invoiceHeader}>
              <Grid container spacing={2}>
                <Grid item xs={7}>

                  <div className={classes.invoiceHeaderItem}>
                    <div className={classes.invoiceHeaderItemLogo}>
                      <img src={"/images/logo.png"} alt="image logo" height={"80px"} />
                    </div>
                    <div className={classes.invoiceHeaderItemInfoCompany}>
                      <Typography className={classes.invoiceHeaderItemInfoCompanyTitle} variant="h3" component="div"
                                  sx={{flexGrow: 1}}>
                        {attrs.company?.value.company_name}
                      </Typography>
                      <Typography className={classes.invoiceHeaderItemInfoCompanyText} component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.bold}>[A]</span>{attrs.company?.value.address}
                      </Typography>
                      <Typography className={classes.invoiceHeaderItemInfoCompanyText} component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.bold}>[E]</span>{attrs.company?.value.email}
                      </Typography>
                      <Typography className={classes.invoiceHeaderItemInfoCompanyText} component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.bold}>[P]</span>{attrs.company?.value.phone}
                      </Typography>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={5}>
                  <div className={classes.invoiceOrder}>
                    <Typography className={classes.invoiceHeaderItemTitle} variant="h3" component="div"
                                sx={{flexGrow: 1}}>
                      ĐƠN HÀNG
                    </Typography>
                    <div className={classes.invoiceOrderItem}>
                      <Typography className={classes.invoiceOrderText} component="div"
                                  sx={{flexGrow: 1}}>
                        (Ngày: {attrs.order !== undefined ? moment(attrs.order.createdAt).format('DD/MM/yyyy') : ''})
                      </Typography>
                      <Typography className={classes.invoiceOrderCode} component="div"
                                  sx={{flexGrow: 1}}>
                        {attrs.order?.code}
                      </Typography>

                    </div>
                  </div>
                </Grid>
              </Grid>


            </div>
            <div className={classes.orderInfo}>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <Typography className={classes.orderInfoTitle} variant="h3" component="div"
                              sx={{flexGrow: 1}}>
                    Khách Hàng
                  </Typography>
                  <Typography className={classes.orderInfoItem} component="div"
                              sx={{flexGrow: 1}}>
                    <span className={classes.orderInfoLabel}>Tên:</span>{attrs.order?.customer.full_name}
                  </Typography>
                  <Typography className={classes.orderInfoItem} component="div"
                              sx={{flexGrow: 1}}>
                    <span className={classes.orderInfoLabel}>Số ĐT:</span>{attrs.order?.customer.phone}
                  </Typography>
                  <Typography className={classes.orderInfoItem} component="div"
                              sx={{flexGrow: 1}}>
                    <span className={classes.orderInfoLabel}>Địa Chỉ:</span>{attrs.order?.address_ship}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.orderInfoTitle} variant="h3" component="div"
                              sx={{flexGrow: 1}}>
                    Thanh Toán
                  </Typography>
                </Grid>
              </Grid>
            </div>
            <div className={classes.orderContent}>
              <div className={classes.orderContentTable}>
                {attrs.photobook_setting && attrs.photobook_setting.map((item, index) => (
                  <div className={classes.orderContentRow}>
                    <Grid container spacing={2}>
                      <Grid item xs={2}>
                        <div className={classes.orderItemCol}>
                          <img className={classes.orderItemImage}
                               src={API_URL + '/' + item.m_photobook_type?.image}
                          />
                        </div>
                      </Grid>
                      <Grid item xs={10}>
                        <div className={classes.orderItemCol}>
                          <Typography className={classes.photobookOrderItemName} variant="h6" component="div"
                                      sx={{flexGrow: 1}}>
                            {item.m_photobook_type?.name}
                          </Typography>
                          <Typography className={classes.photobookOrderItemText} variant="div" component="div"
                                      sx={{flexGrow: 1}}>
                            <span className={classes.photobookOrderItemLabel}>Quy Cách:</span>{item.m_size?.width}X{item.m_size?.height} (cm) - {item?.total_page} Trang
                          </Typography>
                          <Typography className={classes.photobookOrderItemPrice} variant="div" component="div"
                                      sx={{flexGrow: 1}}>
                            <span className={classes.photobookOrderItemLabel}>Đơn Giá:</span>{actions.moneyFormat(item.price)} | <span className={classes.photobookOrderItemLabel}>Số Lượng:</span>{item.quantity} (Cuốn)
                          </Typography>
                          <Typography className={classes.photobookOrderItemTextTotal} variant="div" component="div"
                                      sx={{flexGrow: 1}}>
                            <span className={classes.photobookOrderItemLabel}>Thành Tiền:</span>{actions.moneyFormat(item.price*item.quantity)}
                          </Typography>
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                ))}
              </div>
              <div className={classes.orderContentFooter}>
                <Grid container spacing={2}>
                  <Grid item xs={8}>
                  </Grid>
                  <Grid item xs={4}>
                    <div className={classes.orderTotal}>
                      <Typography className={classes.orderTotalItem} variant="h6" component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.orderTotalItemLabel}>Tổng:</span>{attrs.order !== undefined ? actions.moneyFormat(attrs.order.total_price) : ''}
                      </Typography>
                      <Typography className={classes.orderTotalItem} variant="h6" component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.orderTotalItemLabel}>Giảm Giá:</span>{attrs.order !== undefined ? actions.moneyFormat(attrs.order.discount_price) : ''}
                      </Typography>
                      <Typography className={classes.orderTotalItemBold} variant="h6" component="div"
                                  sx={{flexGrow: 1}}>
                        <span className={classes.orderTotalItemLabelBold}>Thành tiền:</span>{attrs.order !== undefined ? actions.moneyFormat(attrs.order.payment_price) : ''}
                      </Typography>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div className={classes.orderNote}>
              <Typography className={classes.orderNoteContent} variant="div" component="div"
                          sx={{flexGrow: 1}}>
                <span className={classes.orderInfoNote}>Ghi Chú:</span> {attrs.order?.note}
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.user.success
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default compose(
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps)
)(Success);
