import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import { bindActionCreators } from "redux";

import {
  Container,
  Button,
  Typography,
  Box,
  IconButton,
} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { Actions } from "../../actions/user/preview";
import Header from "../common/Header";
import HTMLFlipBook from "react-pageflip";

import * as Storage from "../../storage/index";

import ProgressScreen from "../modules/ProgressFilter";
import ResponsiveComponent from "../common/ResponsiveComponent";

const styles = (theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    width: "100%",
    height: "100%"
  },
  container: {
    backgroundColor: "#f2f2f2",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  boxTitle: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    paddingBottom: "30px",
  },
  textTitle: {
    textAlign: "center",
    fontSize: "22px",
    fontWeight: "bold"
  },
  textDescription: {
    textAlign: "center",
    padding: "10px 0",
    fontSize: "15px"
  },
  btnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    padding: "5px 0",
    alignItems: "center",
  },
  iconChangePage: {
    margin: "10px",
    padding: 0,
    backgroundColor: "#ffffff",
    color: "#000000",
    borderRadius: "50%",
    cursor: "pointer",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 30%)",
    "&:hover": {
      backgroundColor: "#ffffff",
    },
  },
  paginateTitle: {
    fontSize: "16px",
    fontWeight: 500,
  },
  flipBookWrap: {
    maxWidth: "1000px",
    aspectRatio: "2/1",
    overflow: "hidden",
    marginTop: "15px",
    position: "relative",
    "&::after": {
      content: '""',
      width: "1px",
      height: "100%",
      display: "block",
      position: "absolute",
      zIndex: 1,
      backgroundColor: "#eeeeee",
      left: "50%",
      top: 0
    }
  },
  flipBook: {
    width: "100% !important",
    height: "100% !important",
  },
  page: {
    maxWidth: "100%",
    color: "rgb(236, 26, 26)",
    backgroundColor: "white",
    "& img": {
      width: "100%",
      height: "auto",
      maxWidth: "100%"
    }
  },
});

const PageCover = React.forwardRef((props, ref) => {
  return (
    <div ref={ref} data-density="hard">
      <div>{props.children}</div>
    </div>
  );
});

const Page = React.forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <div className={props.className}>{props.children}</div>
    </div>
  );
});

class Preview extends ResponsiveComponent {
  componentWillUnmount() {
    this.props.actions.initState();
  }

  nextButtonClick = () => {
    this.flipBook.getPageFlip().flipNext();
  };

  prevButtonClick = () => {
    this.flipBook.getPageFlip().flipPrev();
  };

  getPaginateTitle = () => {
    let currentPage = "";
    if (this.props.attrs.page == 0) {
      currentPage = "bìa";
    } else {
      currentPage = this.props.attrs.page;
    }
    return `Trang ${currentPage} / ${this.props.attrs.totalPage}`;
  };

  componentDidMount() {
    const idPhotobook = Storage.getCurrentIdPhotobookLocalStorage();
    const photobooks = Storage.getListPhotobookLocalStorage();
    if (!idPhotobook || idPhotobook == null || idPhotobook == ""
     || !photobooks || photobooks == {}) {
      this.props.history.push("/");
    }

    if (Storage.getAttrPhotobook("step") && Storage.getAttrPhotobook("step") != "preview") {
      this.props.history.push(`/${Storage.getAttrPhotobook("step")}`);
    }

    this.props.actions.fetchDatas();
  }

  render() {
    const { classes, actions, attrs, history } = this.props;

    if (!attrs.pagesImage && !attrs.pagesImage.length) {
      return <ProgressScreen />
    } else {
      return (
        <div className={classes.root}>
          <Header history={history} title={"Bản xem trước"} />
          <Container maxWidth="lg" className={classes.container}>
            <Box p={[1, 2, 3]}>
              <Box className={classes.boxTitle}>
                <Typography variant="h5" className={classes.textTitle}>
                  Thiết kế của bạn đã hoàn thành
                </Typography>
                <Typography variant="subtitle1" className={classes.textDescription}>
                  Vui lòng kiểm tra kỹ ảnh trước khi đặt hàng
                </Typography>
                <div className={classes.btnGroup}>
                  <Box px={1}>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => { Storage.setAttrPhotobook("step", "design"); history.push("/design"); }}
                    >
                      Sửa thiết kế
                    </Button>
                  </Box>
                  <Box px={1}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => actions.handleContinueOrder(history)}
                    >
                      Đặt hàng
                    </Button>
                  </Box>
                </div>
                <div className={classes.btnGroup}>
                  <IconButton
                    className={classes.iconChangePage}
                    variant="container"
                    onClick={() => this.flipBook.getPageFlip().flipPrev()}
                  >
                    <ChevronLeftIcon style={{ fontSize: "25px" }} />
                  </IconButton>
                  <Typography variant="h6" className={classes.paginateTitle}>
                    {this.getPaginateTitle()}
                  </Typography>
                  <IconButton
                    className={classes.iconChangePage}
                    variant="container"
                    onClick={() => this.flipBook.getPageFlip().flipNext()}
                  >
                    <ChevronRightIcon style={{ fontSize: "25px" }} />
                  </IconButton>
                </div>
                {attrs.pagesImage && attrs.pagesImage.length ? (
                  <div className={classes.flipBookWrap}>
                    <HTMLFlipBook
                      className={classes.flipBook}
                      width={this.isMobile().any() ? 360 : 1000}
                      height={this.isMobile().any() ? 180 : 500}
                      showCover={false}
                      mobileScrollSupport={false}
                      onFlip={actions.onPage}
                      flippingTime={800}
                      useMouseEvents={false}
                      ref={(el) => (this.flipBook = el)}
                    >
                      {attrs.pagesImage.map((page, index) => (
                        <div className={classes.page} key={index}>
                          <img src={page.image} alt="preview" />
                        </div>
                      ))}
                    </HTMLFlipBook>
                  </div>
                  
                ) : null}
              </Box>
            </Box>
          </Container>
        </div>
      );
    }
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.user.preview,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Preview);
