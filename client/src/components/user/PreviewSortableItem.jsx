import { Box, Grid, Paper, Typography } from "@material-ui/core";
import React from "react";
import { SortableElement } from "react-sortable-hoc";

const PreviewSortableItem = SortableElement(
  ({ value, classes, incrementPageIndex, handleClick }) => {
    return (
      <Grid item onClick={handleClick}>
        <Box direction="column">
          <Paper className={classes.paper}>
            <Grid container spacing={1} className={classes.gridItem}>
              <img
                src={"/images/book_cover.jpg"}
                alt="img"
                className={classes.image}
              />
              <img
                src={"/images/book_cover.jpg"}
                alt="img"
                className={classes.image}
              />
            </Grid>
          </Paper>
          <Typography className={classes.pageTitle}>
            {`Trang ${incrementPageIndex * 2 + 1} & ${
              incrementPageIndex * 2 + 2
            }`}
          </Typography>
        </Box>
      </Grid>
    );
  }
);

export default PreviewSortableItem;
