import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import ErrorIcon from "@material-ui/icons/Error";
import CloseIcon from "@material-ui/icons/Close";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import WarningIcon from "@material-ui/icons/Warning";
import { Snackbar as MuiSnackbar } from "@material-ui/core";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";
import red from "@material-ui/core/colors/red";
import { Actions } from "../../actions/common/messages";
import ResponsiveComponent from "./ResponsiveComponent";

const styles = theme => ({
  root: {
    position: "fixed",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: "fit-content",
    zIndex: 1600,
    top: 0,
    left: "50%",
    transform: "translateX(-50%)"
  },
  body: {
    alignSelf: "stretch",
    display: "flex",
    flexWrap: "nowrap",
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: red[600],
  },
  warning: {
    backgroundColor: amber[700],
  },
  message: {
    display: "flex",
    alignItems: "center",
  },
  snackbar: {
    position: "relative",
    left: null,
    marginTop: theme.spacing(0.5),
    [theme.breakpoints.down('sm')]: {
      left: 0
    },
  }
});

class Messages extends ResponsiveComponent {
  render() {
    const { classes, messages, actions, anchorOrigin } = this.props;

    if (!(messages && messages.length)) {
      return null;
    }
    return (
      <div className={classes.root}>
        {messages.map((row, i) => {
          const autoHideDuration = row.autoHideDuration ? row.autoHideDuration : (() => {
            switch(row.type) {
              case "success": return 3000;
              default: return 10000;
            }
          })();

          return (
            <MuiSnackbar key={row.uuid} open={true} className={classes.snackbar}
              variant={row.type} anchorOrigin={anchorOrigin}
              autoHideDuration={autoHideDuration}
              onClose={(event, reasion) => reasion !== "clickaway" && actions.closeMessage(row.uuid)}
            >
              <SnackbarContent
                className={classes.body + " " + classes[row.type]}
                aria-describedby="client-snackbar"
                message={
                  <span id="client-snackbar" className={classes.message}>
                    {(() => {
                      if ("info" === row.type) {
                        return <InfoIcon />;
                      } else if ("error" === row.type) {
                        return <ErrorIcon />;
                      } else if ("success" === row.type) {
                        return <CheckCircleIcon />;
                      } else if ("warning" === row.type) {
                        return <WarningIcon />;
                      }
                    })()}
                    <span style={{marginLeft: "6px"}}>
                      {(() => {
                        if (row.message) {
                          if (row.message instanceof Array) {
                            return row.message.join("");
                          } else {
                            return row.message;
                          }
                        } else if (row.type === "error") {
                          let result = "";
                          if (row.targets && row.targets.length) {
                            result += row.targets.join(" " + "or" + " ");
                          } else if (row.target && Array.isArray(row.target)) {
                            result += row.target.join(".");
                          } else {
                            result += row.target;
                          }
                          return result + row.check;
                        }
                      })()}
                    </span>
                  </span>
                }
                action={
                  <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    style={{padding: 2}}
                    onClick={() => actions.closeMessage(row.uuid)}
                  >
                    <CloseIcon />
                  </IconButton>
                }
              />
            </MuiSnackbar>
          );
        })}
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    messages: state.common.messages
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
) (Messages);