import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { compose } from "recompose";
import { withRouter } from "react-router-dom";

import MenuBookIcon from '@material-ui/icons/MenuBook';
import PictureInPictureIcon from '@material-ui/icons/PictureInPicture';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import LoyaltyIcon from '@material-ui/icons/Loyalty';

import { Actions } from "../../actions/common/appMenu";
import AppMenuPC from "./AppMenuPC";
import AppMenuSP from "./AppMenuSP";

import ResponsiveComponent from "./ResponsiveComponent";

class AppMenu extends ResponsiveComponent {
  items = [
    {
      uri: "/admin/order",
      icon: <ShoppingCartIcon />,
      text: "Đơn Hàng"
    },
    {
      uri: "/admin/photobook-type",
      icon: <MenuBookIcon />,
      text: "Loại Photobook"
    },
    {
      uri: "/admin/covers",
      icon: <PictureInPictureIcon />,
      text: "Loại Bìa"
    },
    {
      uri: "/admin/price-basic",
      icon: <MonetizationOnIcon />,
      text: "Giá Cơ Bản"
    },
    {
      uri: "/admin/promotion",
      icon: <LoyaltyIcon />,
      text: "Mã giảm giá"
    }
  ]

  renderPC() {
    return (<AppMenuPC {...this.props} items={this.items} >{this.props.children}</AppMenuPC>);
  }

  renderSP() {
    return (<AppMenuSP {...this.props} items={this.items} >{this.props.children}</AppMenuSP>);
  }
  
}

function mapStateToProps(state, props) {
  return {
    attrs: state.common.appMenu
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default withRouter(compose(
  connect(mapStateToProps, mapDispatchToProps)
)(AppMenu));