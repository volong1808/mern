import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { compose } from "recompose";
import { AppBar, Toolbar, Typography, IconButton, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import classNames from "classnames";
import { Link, withRouter } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import ExitIcon from "@material-ui/icons/ExitToApp";
import { withStyles } from "@material-ui/core/styles";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Actions } from "../../actions/common/appMenu";
import ProgressFilter from "../../components/modules/ProgressFilter";
import Messages from "../../components/common/Messages";
import ProgressScreen from "../../components/modules/ProgressScreen";

const styles = theme => ({
  root: {
    height: "100%",
    width: "100%",
    position: "relative",
    paddingTop: theme.mixins.toolbar.minHeight,
    ["@media (min-width:600px)"]: { // eslint-disable-line no-useless-computed-key
      paddingTop: theme.mixins.toolbar["@media (min-width:600px)"].minHeight
    },
    background: "#eef5f9"
  },
  body: {
    display: "flex",
    position: "relative",
    height: "100%",
    width: "100%",
  },
  content: {
    position: "relative",
    flexGrow: 1,
    overflowY: "auto",
    padding: "10px"
  },
  navAction: {
    display: "flex",
    justifyContent: "flex-end"
  },
  sidenav: {
    textDecoration: "none",
    userSelect: "none",
  },
  titleChild: {
    marginLeft: 20,
    alignItems: "center",
    display: "inline-flex",
  },

  drawerUserInfo: {
    justifyContent: "flex-end",
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.common.white
  },
  drawerPaper: {
    top: theme.mixins.toolbar.minHeight
  },
  toolBar: {
    ...theme.mixins.toolbar,
    display: "flex",
    padding: "0 16px",
    userSelect: "none",
    justifyContent: "flex-end"
  },
  toolBarLogo: {
    display: "flex",
    flex: "1 0 auto",
    justifyContent: "center"
  },
  toolBarAction: {
    display: "flex",
    flex: "0 0 auto",
  },
  menuItem: {
    "&.actived": {
      background: theme.palette.primary.main,
      border: "1px solid #ffffff",
      "& $menuItemIcon": {
        color: '#ffffff'
      },
      "& $menuItemText": {
        color: '#ffffff'
      }
    }
  },
  menuItemIcon: {
    color: theme.palette.primary.main,
    minWidth: 'unset',
    marginRight: "20px"
  },
  menuItemText: {
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize: "14px"
  },
});

class AppMenuSP extends React.Component {

  componentDidMount() {
    this.props.actions.toggleMenu();
  }

  render() {
    const { classes, attrs, actions, items, history } = this.props;
    const loginUser = JSON.parse(localStorage.getItem('profile'))?.result;
    if(!loginUser){
      return <ProgressScreen />;
    }else{
      return (
        <div className={classes.root}>

          <div className={classes.body}>
            <AppBar position="fixed" style={attrs.isOpen ? { zIndex: 1400 } : { zIndex: 1100, transitionDelay: "200ms" }}>
              <Toolbar className={classes.toolBar}>
                <div className={classes.toolBarAction}>
                  <IconButton onClick={() => actions.toggleMenu(attrs.isOpen)} color="inherit">
                    {attrs.isOpen ? <CloseIcon /> : <MenuIcon />}
                  </IconButton>
                </div>
              </Toolbar>
            </AppBar>

            <Drawer anchor="right"
              classes={{
                paper: classNames(classes.drawerPaper),
              }}
              open={attrs.isOpen} onClose={actions.closeMenu}
            >
              <List style={{ padding: 0 }}>
                <ListItem className={classes.drawerUserInfo}>
                  <div style={{ display: "flex", width: "100%", alignItems: "center" }}>
                    <ListItemIcon style={{ color: "inherit" }} className={classes.menuItemIcon} ><AccountCircleIcon /></ListItemIcon>
                    <Typography variant="subheading" color="inherit" noWrap>
                      {loginUser.full_name}
                    </Typography>
                  </div>
                </ListItem>
                <Divider />

                {items.map(i => {
                  return (
                    <React.Fragment key={i.uri}>
                    <Link key={i.uri} to={i.uri} className={classes.sidenav} tabIndex={-1} onClick={() => actions.closeMenu(attrs.isOpen)}>
                      <ListItem className={`${classes.menuItem} ${(history.location.pathname.includes(i.uri)) ? "actived" : ""}`} button>
                        <ListItemIcon className={classes.menuItemIcon}>{i.icon}</ListItemIcon>
                        <ListItemText className={classes.menuItemText} primary={i.text} />
                      </ListItem>
                    </Link>
                    <Divider />
                    </React.Fragment>
                  );
                })}

                <ListItem button onClick={() => actions.handleSignOut(history)}>
                  <ListItemIcon className={classes.menuItemIcon}><ExitIcon /></ListItemIcon>
                  <ListItemText className={classes.menuItemText}>Đăng Xuất</ListItemText>
                </ListItem>
              </List>
            </Drawer>

            <main className={classes.content}>
              {this.props.children}
            </main>
          </div>
  
          <ProgressFilter isShow={attrs && attrs.progress} />
          <Messages anchorOrigin={{ vertical: "top", horizontal: "center" }} />
        </div>
      );
    }
  }
}


function mapStateToProps(state, props) {
  return {
    attrs: state.common.appMenu
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default withRouter(compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(AppMenuSP));