import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import {AppBar, Toolbar, Typography, IconButton, Button} from "@material-ui/core";
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import AutorenewIcon from '@material-ui/icons/Autorenew';

import { Actions } from "../../actions/common/header";
import ResponsiveComponent from "./ResponsiveComponent";
import ProgressFilter from "../modules/ProgressFilter";
import Messages from "./Messages";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  header: {
    boxShadow: "none"
  },
  headerToolbar: {
    justifyContent: "space-between",
    maxWidth: "1200px",
    width: "100%",
    margin: "0 auto",
    minHeight: "40px"
  },
  menuButton: {
    paddingTop: 0,
    paddingBottom: 0
  },
  imageLogo: {
    height: '30px'

  },
  title: {
    flexGrow: 1,
    display: 'none',
    textAlign: "center",
    textTransform: "uppercase",
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    marginLeft: 0,
    width: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    fontSize: "12px",
    [theme.breakpoints.up('sm')]: {
      fontSize: "16px",
    },
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    borderRadius: "5px",
    width: '0',
    '&:focus': {
      width: '110px',
      backgroundColor: "rgba(255, 255, 255, 0.15)",
      '&:hover': {
        backgroundColor: "rgba(255, 255, 255, 0.25)",
      },
    },
  },
});

class Header extends ResponsiveComponent {
  render() {
    const { classes, history, title, attrs } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.header}>
          <Toolbar className={classes.headerToolbar}>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
              onClick={() => history.push("/")}
            >
              <img
                src={"/images/logo.png"}
                alt="image"
                className={classes.imageLogo}
              />
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>{title}</Typography>
            <div style={{ display: "flex", alignItems: "center" }}>
              <div>
                {this.props.autoDesign
                ? <Button style={{ border: "1px solid #ffffff", padding: "2px 8px", marginRight: "5px" }}
                    onClick={() => this.props.autoDesign()}
                    variant="contained" color="secondary" endIcon={<AutorenewIcon size="small" />}>Thiết kế tự động</Button>
                : null
                }
              </div>
              {this.props.saveDesign
              ? (
                  <div>
                    <Button style={{ border: "1px solid #ffffff", padding: "2px 8px" }}
                      onClick={() => this.props.saveDesign(history)}
                      variant="contained" color="secondary" endIcon={<ArrowForwardIcon size="small" />}>Next</Button>
                  </div>
                )
              : (
                null
                  // <div className={classes.search}>
                  //   <div className={classes.searchIcon}>
                  //     <SearchIcon />
                  //   </div>
                  //   <InputBase
                  //     placeholder="Nhập SĐT..."
                  //     classes={{
                  //       root: classes.inputRoot,
                  //       input: classes.inputInput,
                  //     }}
                  //     inputProps={{ 'aria-label': 'search' }}
                  //   />
                  // </div>
                )
              }
            </div>
          </Toolbar>
        </AppBar>

        <ProgressFilter isShow={attrs && attrs.progress} />
        <Messages anchorOrigin={{ vertical: "top", horizontal: "center" }} />
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    attrs: state.common.header
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
) (Header);
