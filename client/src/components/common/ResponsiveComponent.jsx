import React from "react";

export default class ResponsiveComponent extends React.Component {
  isMobile() {
    return {
      Android: () => navigator.userAgent.match(/Android/i),
      iOS: () => navigator.userAgent.match(/iPhone|iPad|iPod/i),
      BlackBerry: () => navigator.userAgent.match(/BlackBerry/i),
      Opera: () => navigator.userAgent.match(/Opera Mini/i),
      Windows: () => navigator.userAgent.match(/IEMobile/i),
      any: () => this.isMobile().Android() || this.isMobile().BlackBerry() || this.isMobile().iOS() || this.isMobile().Opera() || this.isMobile().Windows()
    };
  }

  renderPC() {}
  renderSP() {}
  
  render() {
    return (
      <div style={{ height: "100%", width: "100%" }}>
        {this.isMobile().any() ? this.renderSP() : this.renderPC()}
      </div>
    );
  }
}
