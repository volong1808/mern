import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { compose } from "recompose";
import { AppBar, Toolbar, Typography, IconButton, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText, MenuList, MenuItem } from "@material-ui/core";
import classNames from "classnames";
import { Link, withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import MenuIcon from '@material-ui/icons/Menu';

import DropDownMenu from "../modules/DropDownMenu";
import { Actions } from "../../actions/common/appMenu";
import ProgressFilter from "../../components/modules/ProgressFilter";
import Messages from "../../components/common/Messages";

import ProgressScreen from "../../components/modules/ProgressScreen";

const styles = theme => ({
  root: {
    height: "100%",
    width: "100%",
    position: "relative",
    paddingTop: theme.mixins.toolbar.minHeight,
    ["@media (min-width:600px)"]: { // eslint-disable-line no-useless-computed-key
      paddingTop: theme.mixins.toolbar["@media (min-width:600px)"].minHeight
    },
    fontSize: "14px",
    background: "#eef5f9"
  },
  body: {
    display: "flex",
    position: "relative",
    height: "100%",
    width: "100%",
  },
  content: {
    position: "relative",
    flexGrow: 1,
    overflowY: "auto",
    padding: theme.spacing(2)
  },
  navAction: {
    display: "flex",
    justifyContent: "flex-end"
  },
  sidenav: {
    textDecoration: "none",
    userSelect: "none",
  },

  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: 220,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),

    width: theme.spacing(1) * 7,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(1) * 8,
    },
  },
  toolBar: {
    ...theme.mixins.toolbar,
    display: "flex",
    // justifyContent: "space-between",
    padding: "0 20px",
    userSelect: "none",
  },
  title: {
    display: "flex",
    marginRight: "auto",
    alignItems: "center",
    // paddingLeft: 10,
  },
  titleChild: {
    alignItems: "center",
    display: "inline-flex",
    fontWeight: "bold",
    width: "160px",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    "&.hidden": {
      width: 0
    }

  },
  logo: {
    width: "auto",
    height: "auto",
    maxHeight: 48,
    textAlign: "center",
    display:"table-cell",
    marginRight: "24px",
    cursor: "pointer"
  },
  menuItem: {
    paddingLeft: 20,
    paddingRight: 20,
    "&.actived": {
      background: theme.palette.primary.main,
      border: "1px solid #ffffff",
      "& $menuItemIcon": {
        color: '#ffffff'
      },
      "& $menuItemText": {
        color: '#ffffff'
      }
    }
  },
  menuItemIcon: {
    color: theme.palette.primary.main,
    minWidth: 'unset',
    marginRight: "20px"
  },
  menuItemText: {
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize: "14px"
  },
});

class AppMenuPC extends React.Component {

  render() {
    const { classes, attrs, actions, items, history } = this.props;
    const loginUser = JSON.parse(localStorage.getItem('profile'))?.result;
    if(!loginUser){
      return <ProgressScreen />;
    }else{
      return (
        <div className={classes.root}>
          <div className={classes.body}>
            <AppBar position="fixed" style={{ boxShadow: "none" }}>
              <Toolbar className={classes.toolBar}>
                <div className={classes.title}>
                  <Typography variant="subtitle1" color="inherit" noWrap className={classes.logo} onClick={() => history.push("/admin/order")} >
                    <img src={"/images/logo.png"} alt="image logo" height={"30px"} />
                  </Typography>
                  <Typography className={`${classes.titleChild} ${!attrs.isOpen ? "hidden" : ""}`} variant="subtitle1" color="inherit" noWrap>PHOTOBOOK</Typography>
                  <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={actions.toggleMenu}
                    edge="start"
                  >
                    <MenuIcon />
                  </IconButton>
                </div>
                <div>
                  {loginUser && 
                    <DropDownMenu id="header_user_menu" key="header_user_menu" style={{ zIndex: 1100 }}
                      color="inherit" icon={<AccountCircleIcon />} placement="bottom-end"
                      label={
                        <Typography variant="subtitle1" color="inherit" noWrap>{loginUser.full_name}</Typography>
                      }
                    >
                      <MenuList>
                        <MenuItem onClick={() => actions.handleSignOut(history)}>
                          <ListItemText className={classes.menuItemText}>Đăng Xuất</ListItemText>
                        </MenuItem>
                      </MenuList>
                    </DropDownMenu>}
                </div>
              </Toolbar>
            </AppBar>
  
            <Drawer
              variant="permanent"
              classes={{
                paper: classNames(classes.drawerPaper, !attrs.isOpen && classes.drawerPaperClose),
              }}
              open={attrs.isOpen}
            >
              <List style={{ padding: 0 }}>
                {items.map(i => {
                  return (
                    <React.Fragment key={i.uri}>
                    <Link key={i.uri} to={i.uri} className={classes.sidenav} tabIndex={-1}>
                      <ListItem className={`${classes.menuItem} ${(history.location.pathname.includes(i.uri)) ? "actived" : ""}`} button>
                        <ListItemIcon className={classes.menuItemIcon}>{i.icon}</ListItemIcon>
                        <ListItemText className={classes.menuItemText} primary={i.text} />
                      </ListItem>
                    </Link>
                    <Divider />
                    </React.Fragment>
                    )
                })}
              </List>
            </Drawer>
  
            <main className={classes.content}>
              {this.props.children}
            </main>

          </div>
  
          <ProgressFilter isShow={attrs && attrs.progress} />
          <Messages anchorOrigin={{ vertical: "top", horizontal: "center" }} />
        </div>
      );
    }
  }
}


function mapStateToProps(state, props) {
  return {
    attrs: state.common.appMenu
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default withRouter(compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(AppMenuPC));