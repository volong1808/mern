import React from "react";
import { Dialog, DialogContent, DialogContentText, DialogActions, Button } from "@material-ui/core";

export default class Confirm extends React.Component {

  render() {
    return (
      <Dialog open={this.props.open ? true : false} onClose={this.props.onCancel} maxWidth="sm">
        <DialogContent>
          {(() => {
            if (this.props.message) {
              if (typeof this.props.message === "string") {
                return <DialogContentText>{this.props.message}</DialogContentText>;
              } else {
                return this.props.message.map(mes => <DialogContentText>{mes}</DialogContentText>);
              }
            }
            return this.props.children;
          })()}
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={this.props.onCancel}>Hủy bỏ</Button>
          <Button color="primary" onClick={() => {
            this.props.onDone();
            this.props.onCancel();
          }}>OK</Button>
        </DialogActions>
      </Dialog>
    );
  }
}
