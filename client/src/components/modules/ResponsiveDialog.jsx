import React from "react";
import { Dialog, Slide } from "@material-ui/core";
import ResponsiveComponent from "../common/ResponsiveComponent";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} timeout={500} />;
});

export default class ResponsiveDialog extends ResponsiveComponent {

  render() {

    if (this.props.transition) {
      this.props = { ...this.props, TransitionComponent: Transition };
    }
  
    return (
      <Dialog fullScreen={this.isMobile().any()} {...this.props}>
        {this.props.children}
      </Dialog>
    );
  }
}
