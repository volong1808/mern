import React from 'react';
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";

import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';

const styles = theme => ({
    imageCover: {
        position: "relative",
        width: "24%",
        height: "200px",
        backgroundColor: theme.palette.primary.main,
        margin: "15px 0.5% 0",
        "&:hover": {
            transform: "scale(1.05)",
        },
        transition: "all 0.2s ease",

        "@media (max-width: 1000px)": {
            width: "32%",
        },

        "@media (max-width: 576px)": {
            width: "48%",
            height: "120px",
            margin: "15px 1% 0",
        },
        "&:last-child": {
            marginRight: "auto"
        }
    },

    image: {
        width: "100%",
        height: "100%",
        objectFit: "cover",
    },

    handleContainer: {
        position: "absolute",
        left: "0px",
        bottom: "0px",
        backgroundColor: "gray",
        color: "white",
        width: "100%",
        opacity: "0.4",
        padding: "4px 0px",
        "&:hover": {
            color: "white",
            opacity: ".9",

        },
        transition: "all 0.2s ease",
    },

    handleList: {
        display: "flex",
        justifyContent: "center",
        margin: "0px 5px",
        padding: "0px",

    },

    handleItem: {
        display: "inline-block",
        listStyleType: "none",
        padding: "5px 8px",
        backgroundColor: "#8899ee",
        margin: "0px 5px",
        borderRadius: "5px",
        cursor: "pointer",
        "&:hover": {
            backgroundColor: "#99aaff",
            opacity: "1",

        },
    },

});

class ImageComponent extends React.Component {

    constructor(props = {}) {
        super(props);

        this.handleDeleteImage = this.handleDeleteImage.bind(this);
        this.handleZoomImage = this.handleZoomImage.bind(this);

        const { imgInfo } = props;

        this.state = {
            path: imgInfo.path,
            name: imgInfo.name,
            key: imgInfo.key,
        }
    }

    handleDeleteImage = (e) => {
        const dataEvent = { id: e.currentTarget.dataset.key, type: "deleteImage" };
        this.props.handleFunction(dataEvent);

    }

    handleZoomImage = (e) => {
        const dataEvent = { id: e.currentTarget.dataset.key, type: "zoomImage", path: e.currentTarget.dataset.path };
        this.props.handleFunction(dataEvent);
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.imageCover} style={this.props.style}>
                <img src={this.state.path} alt={this.state.name} className={classes.image} />
                <div className={classes.handleContainer}>
                    <ul className={classes.handleList}>
                        <li className={classes.handleItem} onClick={this.handleDeleteImage} data-key={this.state.key}><DeleteIcon fontSize='small' /></li>
                        <li className={classes.handleItem} onClick={this.handleZoomImage} data-key={this.state.key} data-path={this.state.path}><VisibilityIcon fontSize='small' /></li>
                    </ul>
                </div>
            </div>
        );
    };
};

export default compose(
    withStyles(styles, { withTheme: true }),
)(ImageComponent);
