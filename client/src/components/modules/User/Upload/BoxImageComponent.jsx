import React from 'react';
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import ImageComponent from './ImageComponent';

const styles = theme => ({
    imageContainer: {
        minHeight: "90vh",
        backgroundColor: "#eeeeee",
        display: "flex",
        justifyContent: "center",

    },

    contentImage: {
        margin: "20px 0px 80px 0px",
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "space-between",
        width: "80%",
    }
});

class BoxImageComponent extends React.Component {

    constructor(props = {}) {
        super(props);

        const { listImage } = props;

        this.handleOnclick = this.handleOnclick.bind(this);

        this.state = {
            listImage: Array.isArray(listImage) ? listImage : [],
        }
    }

    handleOnclick = (value) => {
        this.props.handleProcessImage(value);
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.imageContainer}>
                <div className={classes.contentImage}>
                    {this.state.listImage.map(item => (
                        < ImageComponent imgInfo={item} key={item.id} handleFunction={this.handleOnclick} ></ImageComponent>
                    ))}
                </div>
            </div >
        );
    };
};

export default compose(
    withStyles(styles, { withTheme: true }),
)(BoxImageComponent);
