import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button, IconButton, ClickAwayListener, Grow, Paper, Popper, withStyles } from "@material-ui/core";

import MoreVertIcon from "@material-ui/icons/MoreVert";
import * as Actions from "../../actions/modules/dropdownMenu";
import { compose } from "recompose";

class DropDownMenu extends React.Component {

  render() {
    const { actions, children, id } = this.props;
    const anchorEl = this.props.attrs ? this.props.attrs.anchorEl : null;
    const isOpen = this.props.attrs ? this.props.attrs.isOpen : false;

    let trigger;
    if (this.props.triggerComponent) {
      trigger = React.cloneElement(
        this.props.triggerComponent,
        {
          onClick: (e) => {
            e.stopPropagation();
            !isOpen && actions.openMenu(id, e);
          },
          onFocus: (e) => {
            !isOpen && actions.openMenu(id, e);
          },
        }
      );
    } else {
      if (this.props.label) {
        trigger = (
          <Button aria-owns="menu-list-grow"
            aria-haspopup="true"
            onClick={(e) => {
              e.stopPropagation();
              !isOpen && actions.openMenu(id, e);
            }}
            // onFocus= {(e) => {
            //   !isOpen && actions.openMenu(id, e);
            // }}
            color={this.props.color}
            {...this.props}
          >
            {this.props.icon &&  this.props.icon}
            <div style={{ paddingLeft: 5 }}>{this.props.label}</div>
          </Button>
        );
      } else {
        trigger = (
          <IconButton aria-owns={isOpen ? "menu-list-grow" : null}
            aria-haspopup="true"
            onClick={(e) => {
              e.stopPropagation();
              !isOpen && actions.openMenu(id, e);
            }}
            // onFocus= {(e) => {
            //   !isOpen && actions.openMenu(id, e);
            // }}
            {...this.props}
          >
            {this.props.icon ? this.props.icon : <MoreVertIcon/>}
          </IconButton>
        );
      }
    }

    return (
      <div key={id} >
        {trigger}
        {isOpen ? <Popper open={isOpen} anchorEl={anchorEl}
          placement={this.props.placement ? this.props.placement : "bottom-start"}
          style={this.props.zIndex ? {zIndex: this.props.zIndex} : {zIndex: this.props.theme.zIndex.modal}}
          transition >
          {({ TransitionProps }) => (
            <Grow {...TransitionProps} >
              <Paper >
                <ClickAwayListener onClickAway={() => actions.closeMenu(id)}>
                  {children}
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
          : null}
        
      </div>
    );
  }
}


function mapStateToProps(state, props) {
  return {
    attrs: state.modules.dropdownMenu[props.id]
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
}

export default compose(
  withStyles({}, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(DropDownMenu);
