import React from "react";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = theme => ({
  root: {
    position: "fixed",
    top: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,0.2)",
    height: "100%",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 2000
  },
});

class ProgressFilter extends React.Component {
  render() {
    const { classes, isShow } = this.props;
    if (isShow) {
      return (
        <div className={classes.root}>
          <CircularProgress size={100} thickness={2} />
        </div>
      );
    } else {
      return null;
    }
  }
}

export default withStyles(styles, { withTheme: true })(ProgressFilter);