import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  imageBoxWrap: {
    width: "20%",
    height: "100%",
    padding: "5px",
    display: "flex",
    justifyContent: "space-between",
    flexFlow: "column nowrap",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    }
  },
  imageBox: {
    width: "100%",
    height: "calc(100% - 60px)",
    display: "flex",
    flexFlow: "row wrap",
    alignContent: "flex-start",
    overflowY: "auto"
  },
  imageItem: {
    width: "32%",
    position: "relative",
    margin: "auto 0.5%",
    aspectRatio: "1/1",
    cursor: "pointer",
    "& img": {
      width: "100%",
      height: "100%",
      objectFit: "cover"
    },
  },
  imgAction: {
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: "50%",
    opacity: 0.75,
    position: "absolute",
    top: "85%",
    left: "85%",
    transform: "translate(-85%, -85%)",
  },
  imageBoxAction: {
    height: "60px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
});

class ReplaceImageBox extends React.Component {
  render() {
    const { classes, attrs, actions, history } = this.props;

    let imagesFrames = [];
    if (attrs.pagesImage && attrs.pagesImage.length) {
      attrs.pagesImage.forEach(page => {
        if (page.layoutItem?.m_frames && Array.isArray(page.layoutItem.m_frames) && page.layoutItem.m_frames.length) {
          page.layoutItem.m_frames.forEach(frame => {
            if (frame?.content) {
              imagesFrames.push(frame.content);
            }
          });
        }
      });
    }

    return (
      <div className={classes.imageBoxWrap}>
        <div className={classes.imageBox}>
          {attrs.images &&  attrs.images.map((image, index) => {
            return <div key={index} className={classes.imageItem} onClick={() => actions.handleSelectImages(image)}>
              <img src={image.path} style={{ opacity: imagesFrames.length && imagesFrames.includes(image.path) ? 0.6 : 1 }} />
            </div>
          })}
        </div>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ReplaceImageBox);