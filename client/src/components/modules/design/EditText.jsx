import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { DialogActions, DialogTitle, DialogContent, Button, IconButton, FormControl, InputLabel, TextField, MenuItem } from '@material-ui/core';

import CloseIcon from "@material-ui/icons/Close";
import SaveIcon from '@material-ui/icons/Save';

import ResponsiveComponent from "../../common/ResponsiveComponent";
import ResponsiveDialog from "../ResponsiveDialog";

const styles = theme => ({
  flexContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  formControl: {
    marginTop: "20px"
  },
  previewLabelWrap: {
    marginTop: "20px",
  },
  previewLabel: {
    marginTop: "5px",
    border: "1px solid #ccc",
    background: "#dddddd",
    padding: "15px",
    whiteSpace: "pre-wrap",
    wordBreak: "break-all",
    lineHeight: 1.5
  }
});

class EditText extends ResponsiveComponent {
  
  inputChanged = (key, value) => {
    const frameTextEdit = this.props.attrs.frameTextEdit;
    frameTextEdit[key] = value;
    this.props.actions.setState({ frameTextEdit });
  }
  
  render() {
    const { classes, attrs, actions } = this.props;

    return (
      <ResponsiveDialog open onClose={actions.closeEditText} scroll="paper" maxWidth="sm" fullWidth>
        <DialogTitle style={{ paddingBottom: 0, textAlign: "right" }}>
          <IconButton onClick={actions.closeEditText} ><CloseIcon /></IconButton>
        </DialogTitle>
        <DialogContent style={{ padding: "0 20px 20px"}}>
          <FormControl className={classes.formControl} fullWidth>
            <TextField
              label="Nội dung"
              value={attrs.frameTextEdit.content}
              onChange={(e) => this.inputChanged("content", e.target.value)}
              multiline
              fullWidth
              rows={5}
              variant="outlined"
              inputProps={{ style: { textAlign: "center" } }}
            />
          </FormControl>
          <div className={classes.flexContainer} style={{ justifyContent: "space-between"}}>
            <FormControl className={classes.formControl}>
              <TextField
                style={{ minWidth: "150px" }}
                select
                label="Font"
                value={attrs.frameTextEdit.fonts}
                onChange={(e) => this.inputChanged("fonts", e.target.value)}
                variant="outlined"
              >
                {attrs.editText.fonts.map((font, index) => {
                  return <MenuItem style={{ fontFamily: font }} key={index} value={font}>{font}</MenuItem>
                })}
              </TextField>
            </FormControl>
            <FormControl className={classes.formControl}>
              <TextField
                style={{ minWidth: "145px" }}
                select
                label="Style"
                value={attrs.frameTextEdit.style}
                onChange={(e) => this.inputChanged("style", e.target.value)}
                variant="outlined"
              >
                {Object.keys(attrs.editText.styles).map((style, index) => {
                  return <MenuItem style={(style == "bold") ? { fontWeight: style } : { fontStyle: style }} key={index} value={style}>{attrs.editText.styles[style]}</MenuItem>
                })}
              </TextField>
            </FormControl>
            <FormControl className={classes.formControl}>
              <TextField
                style={{width: "60px"}}
                label="Màu"
                type={"color"}
                defaultValue={attrs.frameTextEdit.color}
                onBlur={(e) => this.inputChanged("color", e.target.value)}
                variant="outlined"
              />
            </FormControl>
          </div>
          <div className={classes.previewLabelWrap}>
            <InputLabel>Preview</InputLabel>
            <div style={(attrs.frameTextEdit.style == "bold") 
              ? { fontWeight: attrs.frameTextEdit.style, fontFamily: attrs.frameTextEdit.fonts, color: `${attrs.frameTextEdit.color}`, textAlign: "center" }
              : { fontStyle: attrs.frameTextEdit.style, fontFamily: attrs.frameTextEdit.fonts, color: `${attrs.frameTextEdit.color}`, textAlign: "center"}}
              className={classes.previewLabel} >{attrs.frameTextEdit.content}</div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button 
            onClick={() => actions.updateFrameText(attrs.frameTextEdit)}
            variant="contained" color="primary" endIcon={<SaveIcon />} >Lưu</Button>
        </DialogActions>
      </ResponsiveDialog>
    )
  }
}

export default withStyles(styles, { withTheme: true })(EditText);