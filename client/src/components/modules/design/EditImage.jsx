import React from "react";
import { withStyles } from "@material-ui/core/styles";

import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import { DialogTitle, DialogContent, Button, IconButton, DialogActions } from '@material-ui/core';

import CloseIcon from "@material-ui/icons/Close";
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import RotateRightIcon from '@material-ui/icons/RotateRight';
import SaveIcon from '@material-ui/icons/Save';
import ImageIcon from '@material-ui/icons/Image';

import ResponsiveComponent from "../../common/ResponsiveComponent";
import ResponsiveDialog from "../ResponsiveDialog";

const styles = theme => ({
  flexContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  imageBoxWrap: {
    width: "100%",
    height: "100%",
    padding: "5px",
    display: "flex",
    justifyContent: "space-between",
    flexFlow: "column nowrap",
  },
  imageBox: {
    width: "100%",
    height: "calc(100% - 60px)",
    display: "flex",
    flexFlow: "row wrap",
    alignContent: "flex-start",
    overflowY: "auto"
  },
  imageItem: {
    width: "32%",
    position: "relative",
    margin: "auto 0.5%",
    aspectRatio: "1/1",
    cursor: "pointer",
    "& img": {
      width: "100%",
      height: "100%",
      objectFit: "cover"
    },
  },
});

class EditImage extends ResponsiveComponent {
  constructor(props) {
    super(props);
    this.cropper = React.createRef();
  }

  onRotate = (direction) => {
    const rotate = (direction == "left") ? -90 : 90;
    this.cropper.current.cropper.rotate(rotate);
  }

  onCropImage = () => {
    const infoCrop = {
      imageData: this.cropper.current.cropper.imageData,
      canvasData: this.cropper.current.cropper.canvasData,
      cropBoxData: this.cropper.current.cropper.cropBoxData,
    }
    this.props.actions.setState({ cropImage: this.cropper.current.cropper.getCroppedCanvas().toDataURL(), infoCrop });
  }

  render() {
    const { classes, attrs, actions } = this.props;

    let imagesFrames = [];
    if (attrs.pagesImage && attrs.pagesImage.length) {
      attrs.pagesImage.forEach(page => {
        if (page.layoutItem?.m_frames && Array.isArray(page.layoutItem.m_frames) && page.layoutItem.m_frames.length) {
          page.layoutItem.m_frames.forEach(frame => {
            if (frame?.content) {
              imagesFrames.push(frame.content);
            }
          });
        }
      });
    }

    return (
      <>
        <ResponsiveDialog open onClose={actions.closeEditImage} scroll="paper" maxWidth="sm" fullWidth>
        <DialogTitle>
          <div className={classes.flexContainer}>
            <div>
              <Button onClick={() => this.onRotate("left")} variant="outlined" color="primary" endIcon={<RotateLeftIcon />}>Xoay Trái</Button>
              <Button onClick={() => this.onRotate("right")} variant="outlined" color="primary" endIcon={<RotateRightIcon />}>Xoay Phải</Button>
            </div>
            <div>
              <IconButton onClick={actions.closeEditImage} ><CloseIcon /></IconButton>
            </div>
          </div>
        </DialogTitle>
        <DialogContent style={{ padding: "20px"}}>
          <div className={classes.flexContainer} style={{ justifyContent: "center" }}>
            <Cropper
              ref={this.cropper}
              src={attrs.frameImage.content}
              crop={this.onCropImage}
              aspectRatio={attrs.frameImage.width / attrs.frameImage.height}
              autoCropArea={1}
              cropBoxMovable={false}
              cropBoxResizable={false}
              dragMode={"move"}
              ready={() => this.props.headerAction ? this.props.headerAction.hideProgress() : this.props.appMenuAction.hideProgress()}
              checkOrientation
              checkCrossOrigin
            />
          </div>
        </DialogContent>
        <DialogActions>
          <div className={classes.flexContainer} style={{ padding: "0 16px" }}>
            <div>
              <Button 
                onClick={() => actions.openAddImage()}
                variant="contained" color="primary" endIcon={<ImageIcon />}>Thay Ảnh</Button>
            </div>
            <div>
              <Button
                onClick={() => actions.handleCropImage(attrs.frameImage, attrs.cropImage, attrs.infoCrop )}
                variant="contained" color="primary" endIcon={<SaveIcon />}>Lưu</Button>
            </div>
          </div>
        </DialogActions>
      </ResponsiveDialog>

        {attrs.isOpenAddImage &&
          <ResponsiveDialog open onClose={actions.closeAddImage} scroll="paper" maxWidth="sm" fullWidth transition={this.isMobile().any() ? true : false}>
            <DialogActions>
              <Button onClick={actions.closeAddImage} endIcon={<CloseIcon />} ></Button>
            </DialogActions>
            <DialogContent style={{padding: 0}}>
              <div className={classes.imageBoxWrap}>
                <div className={classes.imageBox}>
                  {attrs.images &&  attrs.images.map((image, index) => {
                    return <div key={index} className={classes.imageItem} onClick={() => actions.handleReplaceImageLayout(image, attrs.frameImage)}>
                      <img src={image.path} style={{ opacity: imagesFrames.length && imagesFrames.includes(image.path) ? 0.6 : 1 }} />
                    </div>
                  })}
                </div>
              </div>
            </DialogContent>
          </ResponsiveDialog>}
      </>
    )
  }
}

export default withStyles(styles, { withTheme: true })(EditImage);