import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { Button } from '@material-ui/core';

import AddCircleIcon from '@material-ui/icons/AddCircle';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';

const styles = theme => ({
  imageBoxWrap: {
    width: "20%",
    height: "100%",
    padding: "5px",
    display: "flex",
    justifyContent: "space-between",
    flexFlow: "column nowrap",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    }
  },
  imageBox: {
    width: "100%",
    height: "calc(100% - 60px)",
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "space-between",
    overflowY: "auto",
  },
  imageItem: {
    width: "32%",
    position: "relative",
    margin: "auto 0.5%",
    aspectRatio: "1/1",
    cursor: "pointer",
    "& img": {
      width: "100%",
      height: "100%",
      objectFit: "cover"
    },
    "&:last-child": {
      marginRight: "auto"
    }
  },
  imgAction: {
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: "50%",
    opacity: 0.75,
    position: "absolute",
    top: "85%",
    left: "85%",
    transform: "translate(-85%, -85%)",
  },
  imageBoxAction: {
    height: "60px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
});

class ImageBox extends React.Component {
  render() {
    const { classes, attrs, actions, history } = this.props;

    let imagesFrames = [];
    if (attrs.pagesImage && attrs.pagesImage.length) {
      attrs.pagesImage.forEach(page => {
        if (page.layoutItem?.m_frames && Array.isArray(page.layoutItem.m_frames) && page.layoutItem.m_frames.length) {
          page.layoutItem.m_frames.forEach(frame => {
            if (frame?.content) {
              imagesFrames.push(frame.content);
            }
          });
        }
      });
    }
    
    return (
      <div className={classes.imageBoxWrap}>
        <div className={classes.imageBox}>
          {attrs.images &&  attrs.images.map((image, index) => {
            return <div key={index} className={classes.imageItem} onClick={() => actions.handleSelectImages(image)}>
              <img src={image.path} style={{ opacity: imagesFrames.length && imagesFrames.includes(image.path) ? 0.6 : 1 }} />
              { attrs.selectImages && attrs.selectImages.length && attrs.selectImages.map(item => item.key ).includes(image.key)
                ? <CheckCircleIcon fontSize="small" color="primary" className={classes.imgAction} />
                : null
                }
            </div>
          })}
        </div>
        <div className={classes.imageBoxAction}>
          <Button style={{margin: "0 8px", lineHeight: 1.3}} variant="contained" color="primary" onClick={() => document.getElementById("upload_image").click()}
          size="small" endIcon={<AddPhotoAlternateIcon size="small" />} >Tải thêm ảnh</Button>
          <Button style={{margin: "0 8px", lineHeight: 1.3}} variant="contained" disabled={ attrs.selectImages && attrs.selectImages.length ? false : true } color="primary"
            onClick={actions.handleAddImageLayout}
            size="small" endIcon={<AddCircleIcon size="small" />}>Thêm vào trang</Button>
        </div>
        <form action="" id="form-image" style={{ display: "none" }}>
          <input
              type="file"
              accept="image/png, image/jpeg"
              name="images"
              id="upload_image"
              multiple onChange={(e) => {
                actions.handleUploadImage(e, {
                      id: "form-image",
                      currentId: localStorage.getItem("photobook_current")
                  })
              }}>
          </input>
      </form>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ImageBox);