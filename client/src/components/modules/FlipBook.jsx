import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { IconButton, Typography } from '@material-ui/core';

import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import HTMLFlipBook from "react-pageflip";

import ResponsiveComponent from "../common/ResponsiveComponent";

const styles = theme => ({
  
  btnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    padding: "5px 0",
    alignItems: "center",
  },
  iconChangePage: {
    margin: "10px",
    padding: 0,
    backgroundColor: "#ffffff",
    color: "#000000",
    borderRadius: "50%",
    cursor: "pointer",
    boxShadow: "0 2px 5px 0 rgb(0 0 0 / 30%)",
    "&:hover": {
      backgroundColor: "#ffffff",
    },
  },
  paginateTitle: {
    fontSize: "16px",
    fontWeight: 500,
  },
  flipBookWrap: {
    maxWidth: "1000px",
    aspectRatio: "2/1",
    overflow: "hidden",
    marginTop: "15px",
    position: "relative",
    border: "1px solid #ccc",
    "&::after": {
      content: '""',
      width: "1px",
      height: "100%",
      display: "block",
      position: "absolute",
      zIndex: 1,
      backgroundColor: "#eeeeee",
      left: "50%",
      top: 0
    }
  },
  flipBook: {
    width: "100% !important",
    height: "100% !important",
  },
  page: {
    maxWidth: "100%",
    color: "rgb(236, 26, 26)",
    backgroundColor: "white",
    "& img": {
      width: "100%",
      height: "auto",
      maxWidth: "100%"
    }
  },
});

class FlipBook extends ResponsiveComponent {

  render() {
    const { classes, actions, photobook, indexPhotobook } = this.props;
    return (
      <>
        <div className={classes.flipBookWrap}>
          <HTMLFlipBook
            className={classes.flipBook}
            width={this.isMobile().any() ? 360 : 1000}
            height={this.isMobile().any() ? 180 : 500}
            showCover={false}
            mobileScrollSupport={false}
            onFlip={(e) => actions.handleOnFlip(e.data, indexPhotobook)}
            flippingTime={800}
            useMouseEvents={false}
            ref={(el) => (this.flipBook = el)}
          >
            {photobook.photobook_layout.map((page, i) => (
              <div className={classes.page} key={i}>
                <img src={page.image} alt="preview" />
              </div>
            ))}
          </HTMLFlipBook>
        </div>
        <div className={classes.btnGroup}>
          <IconButton
            className={classes.iconChangePage}
            variant="container"
            onClick={() => this.flipBook.getPageFlip().flipPrev()}
          >
            <ChevronLeftIcon style={{ fontSize: "25px" }} />
          </IconButton>
          <Typography variant="h6" className={classes.paginateTitle}>
            {`Trang ${(photobook.page == 0) ? "bìa" : photobook.page} / ${photobook.photobook_layout.length - 1}`}
          </Typography>
          <IconButton
            className={classes.iconChangePage}
            variant="container"
            onClick={() => this.flipBook.getPageFlip().flipNext()}
          >
            <ChevronRightIcon style={{ fontSize: "25px" }} />
          </IconButton>
        </div>
      </>
    )
  }
}

export default withStyles(styles, { withTheme: true })(FlipBook);